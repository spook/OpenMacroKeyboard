![Logo](./Documentation/Images/omk-logo.png)
# Open Macro Keyboard

A unified software for all DIY macro keyboards.

## 1 TL;DR

You design, assemble and program your own DIY macro keyboard. ![Icon](./Documentation/Images/omk-icon.png) Open Macro Keyboard provides you with a satellite PC application, which connects to your keyboard, handles the communication, provides a set of ready-to-use actions and presents everything in easy to use desktop application.

Long story short, with ![Icon](./Documentation/Images/omk-icon.png) Open Macro Keyboard you can turn your DIY macro keyboard into a powerful device, which can compete with the commercial products.

In the best case scenario, you only need to properly implement software for your keyboard. In the worst case scenario you need to write a small .NET assembly, which serves as a bridge between your keyboard and ![Icon](./Documentation/Images/omk-icon.png) Open Macro Keyboard application.

## 2 What is a macro keyboard?

A macro keyboard is a custom, small keyboard, which allows performing various complex tasks simply with a press of a key. Among most popular tasks you may find pressing a key combination or sequence of key combinations, starting an application, opening a specific location on your disk or navigating to a website. More complex macro keyboards can perform even more complicated tasks, sometimes crafted for usage with a specific application (like applying filters to sound or changing lighting parameters of a photo).

Long story short, it is all about **enabling the user to perform his tasks easier and faster.**

There is a number of commercial, multi-functional and popular macro keyboards,  such as [Elgato Stream Deck](https://elgato.com) or [LoupeDeck](https://loupedeck.com/) but prices of those start at $150 and only go higher. As an alternative, you can buy cheap Chinese keyboards, which allows performing a number of various tasks, but don't present the same level of customization and functionality. Finally, you can build a DIY macro keyboard of your own. 

Open Macro Keyboard is **designed mostly for the DIY macro keyboards** (although it is possible to make it work with commercially available ones as well).

## 3 The problem

Since availability of Cherry MX keys and various programmable microcontrollers on the market is very high, building your own macro keyboard is not much of a complicated task. Also developing embedded software for said keyboard is also quite simple.

At some point however you reach a point, when after pressing a key, **you want an action to be executed on your computer**. And that's where things starts to get complicated.

### 3.1 Executing actions on the PC

In general, **operating systems do not allow external devices to freely execute operations**, and for a good reason. The only excetption are input devies, such as keyboard, mouse, joystick and MIDI devices (electronical usical instruments). 

Most of advanced microcontrollers (such as Teensy, Arduino Leonardo/Micro or Raspberry Pi Pico) can present themselves to the operating system as an input device. If you design your macro keyboard this way, after plugging it to the PC, operating system will treat it as a regular input device.

This level of automation allows you to compress a series of keystrokes (or mouse/joystick actions) to a single key press. However performing any more complex tasks either requires workarounds (you can launch an application by automating pressing Win+R followed by application's executable path) or are outright impossible (for instance bringing specific application to the front).

A different approach involves treating the keyboard as a simple terminal, which is restricted only to sending key press informations to the PC. There, specially designed application translates those keypresses to actual actions.

This solution is for more flexible than the previous one, but **requires a lot more development effort**. This is by the way the approach taken by most of designers of the commercial macro keyboards such as Stream Deck or Loupedeck.

### 3.2 Action count restriction

The base number of actions, which can be performed with your keyboard is equal to number of its keys. 

There is a number of ways of **increasing this count**, but most of them requires some way of providing feedback about currently available actions (e.g. screen with displayed options or LEDs denoting the current mode). This complicates both the design and implementation of keyboard's software.

### 3.3 The solution

![Icon](./Documentation/Images/omk-icon.png) Open Macro Keyboard is an application, which aims in **solving all of the beforementioned problems**.

You don't have to worry about your keyboard not being powerful enough, because ![Icon](./Documentation/Images/omk-icon.png) Open Macro Keyboard **serves as a satellite application** (similarly to the commercial solutions) and **provides you with a set of predefined, ready-to use macros**. And if it is not enough, you can **very easily write your own macros**.

You also don't have to worry about not having enough keys on your keyboard, because ![Icon](./Documentation/Images/omk-icon.png) Open Macro Keyboard provides you with a functionality of **hierarchy of screens**. You can pick a macro folder, which will give you **another full screen** of macros to be used (and you can create virtually as many folders as you want).

If your keyboard doesn't implement any way of providing feedback to the user (such as screens or LEDs), ![Icon](./Documentation/Images/omk-icon.png) Open Macro Keyboard have you covered, because it briefly shows a **hint screen** if you hold any key on your keyboard for more than 300ms. It also keeps it alive when you navigate through folders.

You don't need to put effort into inventing a way of configuring macros on your keyboard, because ![Icon](./Documentation/Images/omk-icon.png) Open Macro Keyboard contains a **very easy to use, drag&drop based screen editor**. Connect your keyboard and start configuring macros right away.

If (despite my best efforts) there is a need for you to write a plugin for the ![Icon](./Documentation/Images/omk-icon.png) Open Macro Keyboard, worry not. One of the core design decisions behind ![Icon](./Documentation/Images/omk-icon.png) Open Macro Keyboard is to make sure that in such case you need to **write as little code as it is humanly possible**. There are also **a couple of levels** of customization. You can choose the one that fits your needs the most.

Ah, and did I mentioned that ![Icon](./Documentation/Images/omk-icon.png) Open Macro Keyboard is completely **free** and **Open Source**?

Let's delve into internals of ![Icon](./Documentation/Images/omk-icon.png) Open Macro Keyboard a little bit more.

## 4 General architecture

The following image explains the overall architecture of application.

![Architecture](./Documentation/Images/Architecture.png)

The <span style="color: #ff0000">red</span> parts are the ones which you need to take care by yourself. The <span style="color: #00a000">green</span> parts are those, which Open Macro Keyboard implements for you. Finally, the <span style="color: #0000ff">blue</span> part represents the underlying operating system on which macros are being executed.

Following sections will discuss in more detail every layer in the beforementioned architecture diagram.

### 4.1 Macro keyboard

![Icon](./Documentation/Images/omk-icon.png) Open Macro Keyboard is a **purely software solution**. However, in the project folders you will find resources, which will simplify building your own keyboard, such as printed circuit board projects, guides and enclosure CAD projects. Nevertheless - you need to build a macro keyboard on your own.

### 4.2 The driver

On the high level, the driver is a **piece of software, which serves as a intermediary** between ![Icon](./Documentation/Images/omk-icon.png) Open Macro Keyboard and your device. Its responsibility is to receive information from the keyboard about events (such as key presses) and translate them into a form understood by ![Icon](./Documentation/Images/omk-icon.png) Open Macro Keyboard.

On the low level, the driver is a **small .NET assembly**, which standarizes communication between your keyboard and Open Macro Keyboard application. Note that ![Icon](./Documentation/Images/omk-icon.png) Open Macro Keyboard's driver has nothing to do with system-level drivers.

Basically, you need to provide a driver for your keyboard. However the effort needed for that depends on how much compatible with ![Icon](./Documentation/Images/omk-icon.png) Open Macro Keyboard your keyboard can become.

#### 4.2.1 Generic driver

If your keyboard:
* Has standard dimensions (in terms of number of buttons), and 
* You can ensure communication protocol compatible with Open Macro Keyboard, which consists of the following requirements:
    * Your keyboard **must send information through the USB COM port** (UART) with speed of 115200 (default settings in general)
    * The messages sent from your keyboard to the PC **must exactly match** the following pattern (including letter casing):

| Command | Example | Comments |
|---------|---------|----------|
| `bxxd;` | `b03d;` | Sent when button xx has been pushed down by the user. |
| `bxxu;` | `b03u;` | Sent when button xx has been released by the user. |
| `exx+;` | `e01+;` | Sent when encoder xx was rotated clockwise. |
| `exx-;` | `e01-;` | Sent when encoder xx was rotated counter-clockwise. |
| `exxd;` | `e01d;` | Sent when encoder xx was pushed down by the user (if it supports pressing). |
| `exxu;` | `e01u;` | Sent when encoder xx was released by the user (if it supports pressing). |

...then **you can simply use one of generic drivers provided with ![Icon](./Documentation/Images/omk-icon.png) Open Macro Keyboard**. You can do that by simply starting up the application and picking one of the generic keyboards from the list:

![Generic keyboards](./Documentation/Images/GenericKeyboard.png)

#### 4.2.2 Custom driver based on generic one

If your keyboard **meets the requirements for the generic driver**, but you **cannot find matching layout**, then you can implement your own driver, basing it on the generic driver's mechanisms. To do that:

* Open the `OpenMacroKeyboard.Drivers.Generic` project
* Add a new GUID to the `Constants` class
* Implement a new class:

```
    public class DriverFactory8x5 : BaseGenericDriverFactory
    {
        public DriverFactory8x5(ILog log)
            : base(log, 8, 5, Constants.GUID_8X5)
        {

        }
    }
```

Afterwards recompile the whole solution and run the application - you should now see your new keyboard on the list.

#### 4.2.3 Custom based on the generic protocol

If your keyboard **meets the requirements for the generic driver**, but **you have encoders** or **you want to take advantage of advanced features** (e.g. sending colors from the application to the keyboard), you can implement your own driver by using the generic infrastructure, so that you don't need to worry about the communication.

It's also quite easy, don't be scared. Follow the steps:

* Create a new class library project in the solution
* Add references to `OpenMacroKeyboard.API` and `OpenMacroKeyboard.Drivers.Common`
* Implement a class deriving from `BaseDriverFactory` (example follows):

```
    public class DriverFactory : BaseDriverFactory
    {               
        private readonly DriverMetadata metadata;

        public DriverFactory(ILog log)
            : base(log)
        {
            metadata = new DriverMetadata(Resources.Strings.KeyboardName, Constants.GUID);

            for (int i = 0; i < 15; i++)
                metadata.ButtonDefinitions.Add(new ButtonInfo(i < 5, true, true, (i / 5), (i % 5)));

            for (int i = 0; i < 2; i++)
                metadata.EncoderDefinitions.Add(new EncoderInfo(true, true, false, true, (i * 2), 6));

            metadata.Settings.Add(new IntSettingInfo(Constants.COM_SETTING_KEY, Resources.Strings.Label_ComPortNumber, 1, 1, 255));
        }

        public override BaseDriver CreateDriver(Dictionary<string, object> settings, IDriverHandler handler)
        {
            return new Driver(settings, handler, log);
        }

        public override DriverMetadata Metadata => metadata;
    }
```

In the constructor you want to generate `DriverMetadata`. Basically, it contains information about your keyboard (mostly its layout and capabilities), so that Open Macro Keyboard can properly display it on the screen and provide appropriate functionalities.

You **must** provide an **unique GUID** for your keyboard. If it clashes with any existing keyboard driver, only one of them will be loaded.

Last two paramters in the `ButtonInfo` and `EncoderInfo` define positioning of the keyboard elements. You can take one of two approaches:

* Specify row and column if the layout is grid-based, or
* You can use an overload, which precisely specifies width, height, x and y of an element (button/encoder).
 
In the latter case you have to express sizes and positions in multiples of 1.0f, which translates into default width/height of one button. So if you want a simple grid layout, set widths and heights to 1.0f and then positions to (0.0f, 0.0f), (1.0f, 0.0f), (2.0f, 0.0f), ..., (0.0f, 1.0f), (1.0f, 1.0f) and so on. Experiment with different sizes and positions to achieve desired effect.

The default mechanism (the one with rows and columns) sets button sizes to (0.9f, 0.9f) and positions them apart with 0.1f margin.

* Finally, implement the actual driver as a class deriving from `BaseDriver`. Use `BaseSerialPortDriver` as a base class - all the incoming communication from the keyboard will be handled for you. 

```
    public class Driver : BaseSerialPortDriver
    {
        private int comPort;

        public Driver(Dictionary<string, object> settings, IDriverHandler handler, ILog log)
            : base(handler, log, (int)settings[Constants.COM_SETTING_KEY])
        {
            comPort = (int)settings[Constants.COM_SETTING_KEY];
        }

        public override void UpdateVisuals(List<KeyboardElementVisuals> buttons, List<KeyboardElementVisuals> encoders)
        {
            List<ColorChangeRequest> request = [];

            for (int i = 0; i < Math.Min(5, buttons.Count); i++)
                request.Add(new ColorChangeRequest(i, buttons[i].Color ?? KeyboardElementColor.Zero));

            for (int i = 0; i < Math.Min(2, encoders.Count); i++)
                request.Add(new ColorChangeRequest(i + 5, encoders[i].Color ?? KeyboardElementColor.Zero));

            ChangeColors(request);
        }

        public override string Display => String.Format(Resources.Strings.DriverDisplay, comPort);
    }
```

In addition you can override the `UpdateVisuals` method, which is called when screen changes, and gives you a chance to pass information about the current screen to your keyboard if it supports providing the feedback (displays, LEDs etc.)

The current implementation of `BaseSerialPortDriver` is capable only of sending button/encoder colors to the keyboard (descriptions and images are not being sent). Commands are being sent through the COM port and need to be intercepted and processed on your macro keyboard.

The message structure follows:

| Command | Example | Comments |
|---------|---------|----------|
| lxxrrrgggbbb; | l01255000255 | Prepares to set color of xx LED to rrr/ggg/bbb. By design the lowercase-l command should prepare the color to be set, but don't set it right away. |
| Lxxrrrgggbbb; | L03064128064 | Immediately set color of xx LED to rrr/ggg/bbb. |
| s; | s; | Apply all colors prepared by the l command to the LEDs |

It is perfectly fine to skip the preparation part. In such case set colors immediately after receiving `l`/`L` commands and completely ignore the `s` commands.

The project files contains example implementation for Raspberry Pi Pico and WS2811-compatible LEDs. Look there for guidance/useful code parts.

Finally, you need to copy your compiled driver into `Drivers` subfolder in the Open Macro Keyboard binary folder. The projects with generic keyboards do that automatically through a build event (specifically, the post-build). Also, to be sure that all required libraries are copied to the folder of your binary, make sure to add the following lines to your driver's project's file:

```
<Project Sdk="Microsoft.NET.Sdk">

  <PropertyGroup>
    <TargetFramework>net8.0-windows</TargetFramework>
    <ImplicitUsings>enable</ImplicitUsings>
    <Nullable>enable</Nullable>
    <Platforms>x64</Platforms>

    <!-- Two lines below -->
    <CopyLocalLockFileAssemblies>true</CopyLocalLockFileAssemblies>
    <RestoreProjectStyle>PackageReference</RestoreProjectStyle>
  </PropertyGroup>
```

Finally, rebuild the solution (and possibly place binaries of your driver in the proper folder manually if you did not prepare your own post-build scripts), start the application and you should see your keyboard on the list.

#### 4.2.4 Custom driver

Finally, if you are unable to match requirements of the communication protocol, you can write your own driver. This requires the most effort, but allows you to turn everything into a macro keyboard (e.g. you can send events from your Bluetooth-enabled smartphone).

If you want to implement your own driver from scratch, follow the steps:

* Create a new class library project
* Reference `OpenMacroKeyboard.API`
* Implement driver factory (derive from `BaseDriverFactory`) and driver (derive from `BaseDriver`). Their public interfaces should be self-explanatory.
* Make sure to put binaries (and their dependencies!) of your driver in the `Drivers` subfolder of the ![Icon](./Documentation/Images/omk-icon.png) Open Macro Keyboard binary folder
* Start the application - your keyboard should be available on the keyboard list.

In this case, you're on your own. Look at the implementations of generic/demo keyboards for some guidance.

## 5. Open Macro Keyboard application

The application serves as a hub receiving commands from your keyboard(s), processing them and executing appropriate commands.

The application starts in system tray by default (once set up you won't look at it too much anyway). Click the icon or use the context menu to bring up the main window.

![Main screen](./Documentation/Images/MainScreen.png)

To use your keyboard, you need to add it. Use the `Add macro keyboard` button, pick a keyboard definition from the list and configure it properly (e.g. COM port).

![Add keyboard](./Documentation/Images/AddKeyboard.png)

The next step is configuring keyboard behavior. Choose `Edit macro layout` to bring up an editor.

![Macro editor](./Documentation/Images/MacroEditor.png)

Some highlights of the editor:

* Click a button/encoder placeholder on the preview to configure it
* Drag a macro from the macro dictionary to the proper placeholder in button/encoder configuration
* Drag buttons and encoders to change their order (you can only switch button with another button and encoder with another encoder)
* Use folders to organize your macros hierarchically
    * To enter a folder, double-click its placeholder in the keyboard preview area
    * To return to an upper folder, locate the `Back` command and double-click it. Note that **`Back` command cannot be replaced or deleted.**
* Use the hexadecimal `#rrggbb` notation to set colors for the buttons, which supports that (note to self: add color picker)
* Adjust description and image to your liking

![Icon](./Documentation/Images/omk-icon.png) Open Macro Keyboard allows you to define complex macro hierarchies. If you assign a folder to a keyboard key, pressing that key will enter its folder. Pressing button with the `Back` action will be equivalent to exiting the folder. This is, by the way, a behavior consistent with the one present in the Stream Deck's configuration application.

Blind navigation through folders is obviously problematic, so ![Icon](./Documentation/Images/omk-icon.png) Open Macro Keyboard implements a feature, which aids in that. If you hold any button on your keyboard longer than 300ms (~1/3 of a second), upon its releasing, ![Icon](./Documentation/Images/omk-icon.png) Open Macro Keyboard will show you a hint window above the system tray area of your primary screen (action of the button *will not* get executed in that case)

![Hint window](./Documentation/Images/HintWindow.png)

The window disappears after 5 seconds, but navigating through the hierarchy of folders when the preview is visible will keep it visible 5 seconds after each change.

The folder navigation feature along with a set of ready-to use macros allows you to turn your keyboard into a powerful device with similar capabilities of commercial products.

## 6. Actions

![Icon](./Documentation/Images/omk-icon.png) Open Macro Keyboard comes with a small set of default actions. This set will be expanded in the future (pull requests are welcome!), but you are also free to implement your own actions if you need some specific behavior. To do that:

* Create a new class library
* Reference the `OpenMacroKeyboard.API` project
* Create a class deriving from `BaseActionFactory` and another one deriving from `BaseAction` (their public interfaces should be self-explanatory)
* Implement, build and place your assembly (and its dependencies) inside `Actions` subfolder in the ![Icon](./Documentation/Images/omk-icon.png) Open Macro Keyboard binary folder.

This is the sample implementation of a "Execute" action from ![Icon](./Documentation/Images/omk-icon.png) Open Macro Keyboard's source code.

The action factory:

```
    public class ExecuteActionFactory : BaseActionFactory
    {
        private readonly ActionMetadata metadata;

        public ExecuteActionFactory(ILog log) 
            : base(log)
        {
            var settings = new List<BaseSettingInfo>
            {
                new StringSettingInfo(Constants.Execute.KEY_PATH, Resources.Strings.Macro_Execute_Label_Path, string.Empty),
                new StringSettingInfo(Constants.Execute.KEY_PARAMETERS, Resources.Strings.Macro_Execute_Label_Parameters, string.Empty)
            };

            metadata = new ActionMetadata(Resources.Strings.Macro_Execute_Name,
                Resources.Strings.Macro_Execute_Group,
                Constants.Execute.Guid,
                settings);
        }

        public override ActionMetadata Metadata => metadata;

        public override BaseAction CreateAction(Dictionary<string, object> settings)
        {
            string? path = settings.ContainsKey(Constants.Execute.KEY_PATH) ? settings[Constants.Execute.KEY_PATH] as string : null;            
            string? parameters = settings.ContainsKey(Constants.Execute.KEY_PARAMETERS) ? settings[Constants.Execute.KEY_PARAMETERS] as string : null;

            return new ExecuteAction(log, path, parameters);
        }
    }
```

The action itself:

```
    public class ExecuteAction : BaseAction
    {        
        private string? path;
        private string? parameters;

        public ExecuteAction(ILog log, string? path, string? parameters)
            : base(log)
        {            
            this.path = path;
            this.parameters = parameters;
        }

        public override void Execute()
        {
            if (path != null) 
            {
                ProcessStartInfo startInfo = new();
                startInfo.UseShellExecute = true;
                startInfo.FileName = path;

                if (!string.IsNullOrEmpty(parameters))
                    startInfo.Arguments = parameters;

                try
                {
                    Process.Start(startInfo);
                }
                catch (Exception e)
                {
                    log.Error("Execute", $"Failed to start process {path} with parameters {parameters}. Error: {e.Message}");
                }
            }            
        }
    }
```

## 7. Frequently asked questions

.NET? What about Linux and MacOS?

<div style="margin-left: 3em; margin-bottom: 1em;background: #f0f0f0; padding: 1em">
Although I respect Linux and its users, I'm not using one daily and also I don't have too much experience in writing GUI applications for that system.

However, since I wrote everything in WPF, you can try to port the application to Linux by replacing WPF with Avalonia. It shouldn't be *that* hard, but surely nontrivial.

Also, I don't own any Mac device and don't plan owning one in the foreseeable future.
</div>

Which version of .NET do I need?

<div style="margin-left: 3em; margin-bottom: 1em; background: #f0f0f0; padding: 1em">
.NET 6 or later.
</div>

How big my keyboard can be?

<div style="margin-left: 3em; margin-bottom: 1em;background: #f0f0f0; padding: 1em">

Your keyboard can have up to 100 keys and 100 encoders. This is by-design restriction and can be lifted if someone presents me with a sane real-life case.

But remember that you can connect multiple keyboards at once and ![Icon](./Documentation/Images/omk-icon.png) Open Macro Keyboard should handle that properly.
</div>

Why do I need to recompile the project or compile its parts?

<div style="margin-left: 3em; margin-bottom: 1em;background: #f0f0f0; padding: 1em">

Because you're doing DIY anyway, don't you? You can't create your own macro keyboard without at least *some* development knowledge. And Visual Studio Community is free to download and use for personal projects.
</div>

Is your software safe?

<div style="margin-left: 3em; margin-bottom: 1em;background: #f0f0f0; padding: 1em">

I provide it as-is and don't take responsibility for any damages caused. But also I have put effort into making sure that it works correctly and also I don't have any agenda on e.g. stealing your data or whatnot. You have full sources, inspect them if you want.

By the way I am accepting pull-requests with new actions, but before merging _I will inspect the whole source myself_.
</div>

