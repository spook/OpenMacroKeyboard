#include <stdio.h>
#include <cmath>
#include <functional>
#include <memory>
#include "pico/stdlib.h"
#include "WS2812.hpp"

// Defines --------------------------------------------------------------------

#define NOISE_SKIP_TIME_MS 50
#define ENCODER_NOISE_SKIP_TIME_MS 50

#define LED_PIN 1
#define LED_LENGTH 15
#define COMMAND_BUFFER_SIZE 256
#define BUTTON_PIN_COUNT 15
#define ENCODER_COUNT 2

// Constants ------------------------------------------------------------------

const int BUTTON_PINS[BUTTON_PIN_COUNT] = { 6, 5, 4, 3, 2, 11, 10, 9, 8, 7, 16, 15, 14, 13, 12 };
const int ENCODERS_OUTA[ENCODER_COUNT] = { 27, 18 };
const int ENCODERS_OUTB[ENCODER_COUNT] = { 26, 17 };
const int ENCODERS_SW[ENCODER_COUNT] = { 28, 19 };

// Types ----------------------------------------------------------------------

enum LogLevel : int {

    None = 0,
    Error = 1,
    Warning = 2,
    Info = 3,
    Verbose = 4
};

// Types ----------------------------------------------------------------------

class Button {

private:
    int index;
    int pin;
    bool state;

public:
    Button(int index, int pin) {

        this->index = index;
        this->pin = pin;
        state = true;
    }

    int getIndex() {

        return this->index;
    }

    int getPin() {

        return this->pin;
    }

    bool getState() {

        return this->state;
    }

    void setState(bool newState) {

        this->state = newState;
    }
};

class Encoder {

private:
    int index;
    int pinA;
    int pinB;
    int pinSW;

    bool valueA, valueB;
    bool buttonState;

public:
    Encoder(int index, int pinA, int pinB, int pinSW) {

        this->index = index;
        this->pinA = pinA;
        this->pinB = pinB;
        this->pinSW = pinSW;
        this->buttonState = true;
        this->valueA = false;
        this->valueB = false;
    }

    int getIndex() {

        return this->index;
    }

    int getPinA() {

        return this->pinA;
    }

    int getPinB() {

        return this->pinB;
    }

    int getPinSW() {

        return this->pinSW;
    }

    bool getButtonState() {

        return this->buttonState;
    }

    void setButtonState(bool newButtonState) {

        this->buttonState = newButtonState;
    }

    bool getValueA() {

        return valueA;
    }

    void setValueA(bool newValueA) {

        valueA = newValueA;
    }

    bool getValueB() {

        return valueB;
    }

    void setValueB(bool newValueB) {

        valueB = newValueB;
    }
};

// Global variables -----------------------------------------------------------

Button buttons[BUTTON_PIN_COUNT] = {
    Button(0, BUTTON_PINS[0]),
    Button(1, BUTTON_PINS[1]),
    Button(2, BUTTON_PINS[2]),
    Button(3, BUTTON_PINS[3]),
    Button(4, BUTTON_PINS[4]),
    Button(5, BUTTON_PINS[5]),
    Button(6, BUTTON_PINS[6]),
    Button(7, BUTTON_PINS[7]),
    Button(8, BUTTON_PINS[8]),
    Button(9, BUTTON_PINS[9]),
    Button(10, BUTTON_PINS[10]),
    Button(11, BUTTON_PINS[11]),
    Button(12, BUTTON_PINS[12]),
    Button(13, BUTTON_PINS[13]),
    Button(14, BUTTON_PINS[14])
};

Encoder encoders[ENCODER_COUNT] = {
    Encoder(0, ENCODERS_OUTA[0], ENCODERS_OUTB[0], ENCODERS_SW[0]),
    Encoder(1, ENCODERS_OUTA[1], ENCODERS_OUTB[1], ENCODERS_SW[1])
};

WS2812 * ledStrip;

char commandBuffer[COMMAND_BUFFER_SIZE + 1];
int commandBufferCount = 0;

LogLevel logLevel;

// Global functions -----------------------------------------------------------

void log(const char * message, LogLevel level) {

    if (level > logLevel) {
        return;
    }

    printf("m%s;\r\n", message);
}

void log(std::function<void(void)> logFn, LogLevel level) {
    
    if (level > logLevel) {
        return;
    }

    printf("m");
    logFn();
    printf(";\r\n");
}

void processButton(Button & button) {

    bool state = gpio_get(button.getPin());

    if (state != button.getState()) {
                
        log([&]() { printf("Detected button %d (pin %d) state change to %d", button.getIndex(), button.getPin(), (int)state); }, LogLevel::Info);

        // Send state change
        if (state == false) {
            printf("b%02dd;\r\n", button.getIndex());
        } else {
            printf("b%02du;\r\n", button.getIndex());
        }

        // Filter out noise
        sleep_ms(NOISE_SKIP_TIME_MS);

        button.setState(state);
    }
}

void processEncoder(Encoder & encoder) {

    bool newValueA = gpio_get(encoder.getPinA());
    bool newValueB = gpio_get(encoder.getPinB());
    bool valueA = encoder.getValueA();
    bool valueB = encoder.getValueB();

    if (valueA != newValueA || valueB != newValueB) {
        printf("%d %d %d\r\n", encoder.getIndex(), newValueA, newValueB);
    }

    int result = 0;

    if (newValueA == valueA && newValueB == valueB) {

        // 0 0 0 0 = transition from 00 to 00 = no change in reading;        
        // 0 1 0 1 = transition from 01 to 01 = no change in reading;        
        // 1 0 1 0 = transition from 10 to 10 = no change in reading;        
        // 1 1 1 1 = transition from 11 to 11 = no change in reading;        
    }
    else {    
        if ((!valueA && !valueB && !newValueA && valueB) ||
            (!valueA && valueB && newValueA && newValueB) ||
            (valueA && !valueB && !newValueA && !newValueB) ||
            (valueA && valueB && newValueA && !newValueB)) {

            // 0 0 0 1 = transition from 00 to 01 = clockwise rotation;          
            // 0 1 1 1 = transition from 01 to 11 = clockwise rotation;          
            // 1 0 0 0 = transition from 10 to 00 = clockwise rotation;          
            // 1 1 1 0 = transition from 11 to 10 = clockwise rotation;          

            log([&](){ printf("Encoder %d, A: %d->%d, B: %d->%d", encoder.getIndex(), valueA, newValueA, valueB, newValueB); }, LogLevel::Verbose);
            log([&](){ printf("Detected encoder %d down", encoder.getIndex()); }, LogLevel::Info);

            printf("e%02d-;\r\n", encoder.getIndex());
        }

        // 0 0 1 0 = transition from 00 to 10 = counter clockwise rotation;  
        // 0 1 0 0 = transition from 01 to 00 = counter clockwise rotation;  
        // 1 0 1 1 = transition from 10 to 11 = counter clockwise rotation;  
        // 1 1 0 1 = transition from 11 to 01 = counter clockwise rotation;  

        else if ((!valueA && !valueB && newValueA && !newValueB) ||
            (!valueA && valueB && !newValueA && !newValueB) ||
            (valueA && !valueB && newValueA && newValueB) ||
            (valueA && valueB && !newValueA && newValueB)) {

            log([&](){ printf("Encoder %d, A: %d->%d, B: %d->%d", encoder.getIndex(), valueA, newValueA, valueB, newValueB); }, LogLevel::Verbose);
            log([&]() { printf("Detected encoder %d up", encoder.getIndex()); }, LogLevel::Info);

            printf("e%02d+;\r\n", encoder.getIndex());
        }
        else {

            // 0 0 1 1 = transition from 00 to 11 = error;                       
            // 0 1 1 0 = transition from 01 to 10 = error;                       
            // 1 0 0 1 = transition from 10 to 01 = error;                       
            // 1 1 0 0 = transition from 11 to 00 = error;                       
        }

        sleep_ms(ENCODER_NOISE_SKIP_TIME_MS);

        newValueA = gpio_get(encoder.getPinA());
        newValueB = gpio_get(encoder.getPinB());

        encoder.setValueA(newValueA);
        encoder.setValueB(newValueB);
    }

    // Button

    bool state = encoder.getButtonState();
    bool newState = gpio_get(encoder.getPinSW());

    if (newState != state) {

        if (newState == true) {

            log([&]() { printf("Detected encoder %d button up", encoder.getIndex()); }, LogLevel::Info);

            // Send encoder button up
            printf("e%02du;\r\n", encoder.getIndex());
        } else {

            log([&]() { printf("Detected encoder %d button down", encoder.getIndex()); }, LogLevel::Info);

            // Send encoder button up
            printf("e%02dd;\r\n", encoder.getIndex());
        }

        sleep_ms(NOISE_SKIP_TIME_MS);

        encoder.setButtonState(newState);
    }
}

void processCommand() {

    log([&]() { printf("Processing command: ");

        for (int i = 0; i < commandBufferCount-1; i++) {
            printf("%c", commandBuffer[i]);
        }
    }, LogLevel::Info);

    if (commandBufferCount == 0) {

        log("Command buffer is empty!", LogLevel::Warning);
        return;
    }

    if (commandBuffer[0] == 'l' || commandBuffer[0] == 'L') {

        log("Detected LED command", LogLevel::Info);

        // Command to change LED color
        // Expected format: lxxrrrgggbbb;
        // Where:
        //   l   - Command
        //         l - set the color, but not update yet
        //             (use s; command to show all LEDs)
        //         L - set the color and immediately update
        //   xx  - index of LED to change
        //   rrr - red (valid range: 000 - 255)
        //   ggg - green (valid range: 000 - 255)
        //   bbb - blue (valid range: 000 - 255)

        // Checking for correctness

        if (commandBufferCount != 13) {

            log([&](){ printf("Wrong length of the command, should be 13, is %d", commandBufferCount); }, LogLevel::Error);
            return;
        }

        for (int j = 1; j < 11; j++) {

            if (commandBuffer[j] < '0' || commandBuffer[j] > '9') {

                log([&]() { printf("%d-th character is not a digit", j); }, LogLevel::Error);
                return;
            }
        }

        if (commandBuffer[12] != ';') {

            log("Command does not end with a semicolon", LogLevel::Error);
            return;
        }

        // Decoding parameters

        int index = (commandBuffer[1] - '0') * 10 + 
            (commandBuffer[2] - '0');
        int red = (commandBuffer[3] - '0') * 100 +
            (commandBuffer[4] - '0') * 10 +
            (commandBuffer[5] - '0');
        int green = (commandBuffer[6] - '0') * 100 +
            (commandBuffer[7] - '0') * 10 +
            (commandBuffer[8] - '0');
        int blue = (commandBuffer[9] - '0') * 100 +
            (commandBuffer[10] - '0') * 10 +
            (commandBuffer[11] - '0');

        if (red > 255 || green > 255 || blue > 255) {

            log([&]() { printf("One of RGB values is incorrect: %d, %d, %d.", red, green, blue);}, LogLevel::Error);
            return;
        }

        log([&]() { printf("Setting LED %d color to %d, %d, %d", index, red, green, blue);}, LogLevel::Info);

        // Restricting the maximum brightness not to exceed max USB current
        red /= 2;
        green /= 2;
        blue /= 2;

        ledStrip->setPixelColor(index, WS2812::RGB(red, green, blue));

        if (commandBuffer[0] == 'L') {

            log("Showing LED colors", LogLevel::Info);
            ledStrip->show();
        }
    } else if (commandBuffer[0] == 's') {

        log("Detected LED show command", LogLevel::Info);

        // Command to show stored LED colors
        // Expected format: s;

        // Checking for correctness

        if (commandBufferCount != 2) {

            log([&](){ printf("Wrong length of the command, should be 2, is %d", commandBufferCount); }, LogLevel::Error);
            return;
        }

        if (commandBuffer[1] != ';') {

            log("Command does not end with a semicolon", LogLevel::Error);
            return;
        }

        ledStrip->show();
    } else if (commandBuffer[0] == 'm') {

        log("Detected log level switch command", LogLevel::Info);

        // Command to toggle logging
        // Expected format: mx;
        // Where
        // m - Command (expected "m")
        // x - Mode (0 - None, 1 - Error, 2 - Warning, 3 - Info, 4 - Verbose)

        if (commandBufferCount != 3) {

            log([&](){ printf("Wrong length of the command, should be 3, is %d", commandBufferCount); }, LogLevel::Error);                
            return;
        }

        if (commandBuffer[2] != ';') {

            log("Command does not end with a semicolon", LogLevel::Error);
            return;
        }

        if (commandBuffer[1] < '0' || commandBuffer[1] > '4') {

            log("Command parameter is not a digit or is outside valid range", LogLevel::Error);
            return;
        }

        int newLogLevel = commandBuffer[1] - '0';

        log([&]() { printf("Switching log level to %d", newLogLevel);}, LogLevel::Info);
        logLevel = (LogLevel)(newLogLevel);        

    } else if (commandBuffer[0] == 'z') {

        log("Detected sleep command", LogLevel::Info);

        // Command to put the keyboard into sleep mode
        // Expected format: z;

        // Checking for correctness

        if (commandBufferCount != 2) {

            log([&](){ printf("Wrong length of the command, should be 2, is %d", commandBufferCount); }, LogLevel::Error);
            return;
        }

        if (commandBuffer[1] != ';') {

            log("Command does not end with a semicolon", LogLevel::Error);
            return;
        }

        ledStrip->fill(WS2812::RGB(0, 0, 0));
        ledStrip->show();
    } else {

        log("Unknown command, ignoring", LogLevel::Error);
    }
}

void processSerial() {

    int res;

    do {

        res = getchar_timeout_us(0);
        
        // Ignore CR/LF characters (simplifies debugging
        // with serial port monitor)

        if (res == 10 || res == 13) {

            log("Ignoring CR/LF character", LogLevel::Verbose);
            continue;
        }

        if (res != PICO_ERROR_TIMEOUT) {

            // Store character in command buffer

            auto ch = (char)res;

            if (ch != ';') {

                log([&](){ printf("Received character %c, storing on position %d", ch, commandBufferCount); }, LogLevel::Verbose);
            } else {

                log([&](){ printf("Received semicolon, storing on position %d", ch, commandBufferCount); }, LogLevel::Verbose);
            }

            commandBuffer[commandBufferCount] = ch;

            // Command buffer has one extra item at end,
            // which makes space for additional \0 char

            commandBuffer[commandBufferCount + 1] = 0;

            commandBufferCount++;
            if (commandBufferCount > COMMAND_BUFFER_SIZE - 1) {

                commandBufferCount = COMMAND_BUFFER_SIZE - 1;
            }

            // If sent character is a semicolon, process the command

            if (ch == ';') {

                log("Received semicolon, processing command", LogLevel::Verbose);

                // Process command
                
                processCommand();

                // Clear buffer

                commandBufferCount = 0;
            }
        }
    } while (res != PICO_ERROR_TIMEOUT);
}

// Main -----------------------------------------------------------------------

int main()
{
    logLevel = LogLevel::None;

    stdio_init_all();

    // Initialize button pins

    for (uint i = 0; i < BUTTON_PIN_COUNT; i++) {

        gpio_init(buttons[i].getPin());
        gpio_set_dir(buttons[i].getPin(), GPIO_IN);
        gpio_pull_up(buttons[i].getPin());
    }

    // Initialize encoder pins

    for (int i = 0; i < ENCODER_COUNT; i++) {

        gpio_init(encoders[i].getPinA());
        gpio_set_dir(encoders[i].getPinA(), GPIO_IN);
        gpio_pull_up(encoders[i].getPinA());

        gpio_init(encoders[i].getPinB());
        gpio_set_dir(encoders[i].getPinB(), GPIO_IN);
        gpio_pull_up(encoders[i].getPinB());

        gpio_init(encoders[i].getPinSW());
        gpio_set_dir(encoders[i].getPinSW(), GPIO_IN);
        gpio_pull_up(encoders[i].getPinSW());
    }

    // Initialize led strip
    ledStrip = new WS2812(LED_PIN, LED_LENGTH, pio0, 0, WS2812::FORMAT_GRB);
    ledStrip->fill(WS2812::RGB(0, 0, 0));
    ledStrip->show();

    // Render animation

    for (int i = 0; i < LED_LENGTH; i++) {

        ledStrip->setPixelColor(i, WS2812::RGB(128, 128, 128));
        ledStrip->show();

        sleep_ms(150);
    }

    sleep_ms(1000);
    ledStrip->fill(WS2812::RGB(0, 0, 0));
    ledStrip->show();

    // Initialize USB 
    while (!stdio_usb_connected()) {
        
        ledStrip->fill( WS2812::RGB(128, 0, 0) );
        ledStrip->show();

        sleep_ms(500);

        ledStrip->fill(WS2812::RGB(0, 0, 0));
        ledStrip->show();

        sleep_ms(500);
    }

    // Clear LED strip

    ledStrip->fill(WS2812::RGB(0, 0, 0));
    ledStrip->show();

    // Clear command buffer
    for (int i = 0; i < COMMAND_BUFFER_SIZE + 1; i++) {
        commandBuffer[i] = 0;
    }

    // Main loop

    while (true) {

        // Poll buttons

        for (int i = 0; i < BUTTON_PIN_COUNT; i++) {
            processButton(buttons[i]);
        }

        // Poll encoders

        for (int i = 0; i < ENCODER_COUNT; i++) {
            processEncoder(encoders[i]);
        }

        // Poll serial for commands

        processSerial();
    }

    /*
    ledStrip.fill( WS2812::RGB(255, 0, 0) );
    ledStrip.setPixelColor(ledIndex, color);
    ledStrip.show();
    */

    return 0;
}
