﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.API.V1.Common.Models
{
    public class SelectSettingEntryInfo
    {
        public SelectSettingEntryInfo(string description, int value)
        {
            Description = description;
            Value = value;
        }

        public string Description { get; }
        public int Value { get; }
    }
}
