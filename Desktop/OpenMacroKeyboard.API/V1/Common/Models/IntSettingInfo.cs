﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.API.V1.Common.Models
{
    public class IntSettingInfo : BaseSettingInfo
    {
        public IntSettingInfo(string key, string description, int defaultValue, int? minValue = null, int? maxValue = null) 
            : base(key, description)
        {
            DefaultValue = defaultValue;
            MinValue = minValue;
            MaxValue = maxValue;
        }

        public int DefaultValue { get; }
        public int? MinValue { get; }
        public int? MaxValue { get; }
    }
}
