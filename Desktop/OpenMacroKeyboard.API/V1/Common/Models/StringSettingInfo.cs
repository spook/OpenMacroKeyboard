﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.API.V1.Common.Models
{
    public class StringSettingInfo : BaseSettingInfo
    {
        public StringSettingInfo(string key, string description, string defaultValue, uint? maxLength = null, Regex? validationRegex = null) 
            : base(key, description)
        {
            DefaultValue = defaultValue;
            MaxLength = maxLength;
            ValidationRegex = validationRegex;
        }

        public string DefaultValue { get; }
        public uint? MaxLength { get; }
        public Regex? ValidationRegex { get; }
    }
}
