﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.API.V1.Common.Models
{
    public class SelectSettingInfo : BaseSettingInfo
    {
        public SelectSettingInfo(string key, string description, int defaultValue) 
            : base(key, description)
        {
            DefaultValue = defaultValue;
            AvailableEntries = new();
        }

        public SelectSettingInfo(string key, string description, int defaultValue, List<SelectSettingEntryInfo> availableEntries)
            : base(key, description)
        {
            DefaultValue = defaultValue;
            AvailableEntries = availableEntries;
        }

        public int DefaultValue { get; } 
        public List<SelectSettingEntryInfo> AvailableEntries { get; }
    }
}
