﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.API.V1.Common.Models
{
    public class BoolSettingInfo : BaseSettingInfo
    {
        public BoolSettingInfo(string key, string description, bool defaultValue) 
            : base(key, description)
        {
            DefaultValue = defaultValue;
        }

        public bool DefaultValue { get; }
    }
}
