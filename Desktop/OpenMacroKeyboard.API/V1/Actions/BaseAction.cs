﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.API.V1.Actions
{
    public abstract class BaseAction : IDisposable
    {
        protected readonly ILog log;

        protected BaseAction(ILog log)
        {
            this.log = log;
        }

        public virtual void Dispose()
        {
            
        }

        public abstract void Execute();
    }
}
