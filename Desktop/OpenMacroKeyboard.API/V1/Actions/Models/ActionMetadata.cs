﻿using OpenMacroKeyboard.API.V1.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.API.V1.Actions.Models
{
    public class ActionMetadata
    {
        public ActionMetadata(string actionName, string group, Guid guid)
        {
            ActionName = actionName;
            Group = group;
            Guid = guid;
            Settings = new();
        }

        public ActionMetadata(string actionName, string group, Guid guid, List<BaseSettingInfo> settings)
        {
            ActionName = actionName;
            Group = group;
            Guid = guid;
            Settings = settings;
        }

        public string ActionName { get; }
        public string Group { get; }
        public Guid Guid { get; }
        public List<BaseSettingInfo> Settings { get; }
    }
}
