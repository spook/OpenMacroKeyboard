﻿using OpenMacroKeyboard.API.V1.Actions.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.API.V1.Actions
{
    public abstract class BaseActionFactory
    {
        protected readonly ILog log;

        public abstract BaseAction CreateAction(Dictionary<string, object> settings);

        public BaseActionFactory(ILog log)
        {
            this.log = log;
        }

        public virtual void Dispose()
        {

        }

        public abstract ActionMetadata Metadata { get; }
    }
}
