﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenMacroKeyboard.API.V1.Common.Models;

namespace OpenMacroKeyboard.API.V1.Drivers.Models
{
    public class DriverMetadata
    {
        public DriverMetadata(string driverName, Guid guid)
        {
            DriverName = driverName;
            Guid = guid;
        }

        public string DriverName { get; }
        public Guid Guid { get; }
        public List<ButtonInfo> ButtonDefinitions { get; } = new();
        public List<EncoderInfo> EncoderDefinitions { get; } = new();
        public List<BaseSettingInfo> Settings { get; } = new();
    }
}
