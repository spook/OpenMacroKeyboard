﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.API.V1.Drivers.Models
{
    public class RawImage
    {
        public RawImage(byte[] argb, int width, int height)
        {
            Argb = argb;
            Width = width;
            Height = height;
        }

        public byte[] Argb { get; }
        public int Width { get; }
        public int Height { get; }
    }
}
