﻿namespace OpenMacroKeyboard.API.V1.Drivers.Models
{
    public class EncoderInfo : BaseKeyboardElementInfo
    {
        public EncoderInfo(bool hasButton, bool canSetColor, bool canSetImage, bool canSetDescription, float width, float height, float x, float y)
            : base(canSetColor, canSetDescription, canSetImage, height, width, x, y)
        {
            HasButton = hasButton;
        }

        public EncoderInfo(bool hasButton, bool canSetColor, bool canSetImage, bool canSetDescription, int row, int col)
            : this(hasButton, canSetColor, canSetImage, canSetDescription, DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_MARGIN + col, DEFAULT_MARGIN + row)
        {

        }

        public bool HasButton { get; }
    }
}
