﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.API.V1.Drivers.Models
{
    public class KeyboardElementColor
    {
        public static readonly KeyboardElementColor Zero = new(0, 0, 0);

        public KeyboardElementColor(byte r, byte g, byte b)
        {
            R = r;
            G = g;
            B = b;
        }

        public byte R { get; } 
        public byte G { get; } 
        public byte B { get; } 
    }
}
