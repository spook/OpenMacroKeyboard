﻿namespace OpenMacroKeyboard.API.V1.Drivers.Models
{
    public abstract class BaseKeyboardElementInfo
    {
        protected const float DEFAULT_MARGIN = 0.05f;
        protected const float DEFAULT_WIDTH = 0.9f;
        protected const float DEFAULT_HEIGHT = 0.9f;

        protected BaseKeyboardElementInfo(bool canSetColor, bool canSetDescription, bool canSetImage, float height, float width, float x, float y)
        {
            CanSetColor = canSetColor;
            CanSetDescription = canSetDescription;
            CanSetImage = canSetImage;
            Height = height;
            Width = width;
            X = x;
            Y = y;
        }

        public bool CanSetColor { get; }
        public bool CanSetDescription { get; }
        public bool CanSetImage { get; }
        public float Height { get; }
        public float Width { get; }
        public float X { get; }
        public float Y { get; }
    }
}