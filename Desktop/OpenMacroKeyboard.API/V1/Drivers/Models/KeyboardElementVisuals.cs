﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.API.V1.Drivers.Models
{
    public class KeyboardElementVisuals
    {
        public KeyboardElementVisuals(string? description, KeyboardElementColor? color, RawImage? image, object? customCache)
        {
            Description = description;
            Color = color;
            Image = image;
            CustomCache = customCache;
        }

        public string? Description { get; }
        public KeyboardElementColor? Color { get; }
        public RawImage? Image { get; }
        public object? CustomCache { get; }
    }
}
