﻿namespace OpenMacroKeyboard.API.V1.Drivers.Models
{
    public class ButtonInfo : BaseKeyboardElementInfo
    {
        public ButtonInfo(bool canSetColor, bool canSetImage, bool canSetDescription, float width, float height, float x, float y)
            : base(canSetColor, canSetDescription, canSetImage, height, width, x, y)
        {

        }

        public ButtonInfo(bool canSetColor, bool canSetImage, bool canSetDescription, int row, int col)
            : this(canSetColor, canSetImage, canSetDescription, DEFAULT_WIDTH, DEFAULT_HEIGHT, DEFAULT_MARGIN + col, DEFAULT_MARGIN + row)
        {

        }
    }
}
