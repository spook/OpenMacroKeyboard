﻿using OpenMacroKeyboard.API.V1.Drivers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.API.V1.Drivers
{
    public abstract class BaseDriverFactory : IDisposable
    {
        protected readonly ILog log;

        public abstract BaseDriver CreateDriver(Dictionary<string, object> settings, IDriverHandler handler);

        public BaseDriverFactory(ILog log)
        {
            this.log = log;
        }

        public virtual void Dispose()
        {

        }

        public abstract DriverMetadata Metadata { get; }
    }
}
