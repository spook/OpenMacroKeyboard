﻿using OpenMacroKeyboard.API.V1.Drivers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.API.V1.Drivers
{
    public abstract class BaseDriver : IDisposable
    {
        protected readonly IDriverHandler handler;
        protected readonly ILog log;

        protected T TryGetSetting<T>(Dictionary<string, object> settings, string key, T defaultValue)
        {
            if (settings.ContainsKey(key) && settings[key] is T value)
                return value;
            else
                return defaultValue;
        }

        public BaseDriver(IDriverHandler handler, ILog log)
        {
            ArgumentNullException.ThrowIfNull(handler);

            this.handler = handler;
            this.log = log;
        }

        /// <summary>
        /// Releases all resources occupied by the keyboard
        /// </summary>
        public virtual void Dispose()
        {

        }

        /// <summary>
        /// Initializes the keyboard
        /// </summary>
        public virtual bool Init()
        {
            return true;
        }

        /// <summary>
        /// Updates visuals (description, color, image) on the
        /// keyboard (if supported).
        /// </summary>
        public virtual void UpdateVisuals(List<KeyboardElementVisuals> buttons, List<KeyboardElementVisuals> encoders)
        {

        }

        /// <summary>
        /// Puts keyboard in sleep mode (if supported)
        /// </summary>
        public virtual void Sleep()
        {
            
        }

        /// <summary>
        /// Allows generating custom cache for a button, which later can
        /// be retrieved from KeyboardElementVisuals instance passed to
        /// UpdateVisuals method. May be used to e.g. pre-render the image,
        /// so that it will be faster displayed on the keyboard.
        /// </summary>
        public virtual object? BuildButtonCustomCache(ButtonInfo definition, KeyboardElementVisuals visuals)
        {
            return null;
        }

        /// <summary>
        /// Allows generating custom cache for an encoder, which later can
        /// be retrieved from KeyboardElementVisuals instance passed to
        /// UpdateVisuals method. May be used to e.g. pre-render the image,
        /// so that it will be faster displayed on the keyboard.
        /// </summary>
        public virtual object? BuildEncoderCustomCache(EncoderInfo encoderDefinition, KeyboardElementVisuals visuals)
        {
            return null; 
        }

        public abstract string Display { get; }
    }
}
