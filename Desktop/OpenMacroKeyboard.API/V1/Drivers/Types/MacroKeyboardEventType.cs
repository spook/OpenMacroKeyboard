﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.API.V1.Drivers.Types
{
    public enum MacroKeyboardEventType
    {
        Down = 1,
        Up = 2,
        Increase = 3,
        Decrease = 4
    }
}
