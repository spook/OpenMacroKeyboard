﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.API.V1.Drivers.Types
{
    public enum MacroKeyboardEventSource
    {
        Button = 1,
        Encoder = 2
    }
}
