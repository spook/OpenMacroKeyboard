﻿using OpenMacroKeyboard.API.V1.Drivers.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.API.V1.Drivers
{
    public interface IDriverHandler
    {
        public void HandleEvent(MacroKeyboardEventSource source, int index, MacroKeyboardEventType type);
        public void NotifyAwake();
    }
}
