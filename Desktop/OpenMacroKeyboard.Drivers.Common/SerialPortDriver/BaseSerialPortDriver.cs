﻿using OpenMacroKeyboard.API.V1;
using OpenMacroKeyboard.API.V1.Drivers;
using OpenMacroKeyboard.API.V1.Drivers.Types;
using OpenMacroKeyboard.Drivers.Common.Models;
using OpenMacroKeyboard.Drivers.Common.SerialPortDriver.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Drivers.Common.SerialPortDriver
{
    public abstract class BaseSerialPortDriver : BaseDriver
    {
        private const byte ASCII_SEMICOLON = 59;
        private const byte ASCII_b = 98;
        private const byte ASCII_B = 66;
        private const byte ASCII_e = 101;
        private const byte ASCII_E = 69;
        private const byte ASCII_0 = 48;
        private const byte ASCII_9 = 57;
        private const byte ASCII_u = 117;
        private const byte ASCII_U = 85;
        private const byte ASCII_d = 100;
        private const byte ASCII_D = 68;
        private const byte ASCII_PLUS = 43;
        private const byte ASCII_MINUS = 45;
        private const byte ASCII_CR = 10;
        private const byte ASCII_LF = 13;

        private const int BUFFER_SIZE = 256;

        private readonly bool supportsSleep;
        private bool isAsleep = false;
        private SerialPort serialPort;
        private int comPort;

        private byte[] buffer;
        private byte[] commandBuffer;
        private int commandBufferLength;

        private void ProcessCommand()
        {
            if (commandBuffer[0] is ASCII_b or ASCII_B)
            {
                // Button change
                if (commandBuffer[1] < ASCII_0 || commandBuffer[1] > ASCII_9 || commandBuffer[2] < ASCII_0 || commandBuffer[2] > ASCII_9)
                    return;

                int button = (commandBuffer[1] - ASCII_0) * 10 + (commandBuffer[2] - ASCII_0);
                MacroKeyboardEventType eventType;

                if (commandBuffer[3] is ASCII_u or ASCII_U)
                    eventType = MacroKeyboardEventType.Up;
                else if (commandBuffer[3] is ASCII_d or ASCII_D)
                    eventType = MacroKeyboardEventType.Down;
                else
                    return;

                if (commandBuffer[4] is not ASCII_SEMICOLON)
                    return;

                // Button status change detected

                if (isAsleep && eventType == MacroKeyboardEventType.Up)
                {
                    isAsleep = false;
                    handler.NotifyAwake();
                }
                else
                {
                    handler.HandleEvent(MacroKeyboardEventSource.Button, button, eventType);
                }
            }
            else if (commandBuffer[0] is ASCII_e or ASCII_E)
            {
                // Encoder change
                if (commandBuffer[1] < ASCII_0 || commandBuffer[1] > ASCII_9 || commandBuffer[2] < ASCII_0 || commandBuffer[2] > ASCII_9)
                    return;

                int encoder = (commandBuffer[1] - ASCII_0) * 10 + (commandBuffer[2] - ASCII_0);
                MacroKeyboardEventType eventType;

                if (commandBuffer[3] is ASCII_u or ASCII_U)
                    eventType = MacroKeyboardEventType.Up;
                else if (commandBuffer[3] is ASCII_d or ASCII_D)
                    eventType = MacroKeyboardEventType.Down;
                else if (commandBuffer[3] is ASCII_PLUS)
                    eventType = MacroKeyboardEventType.Increase;
                else if (commandBuffer[3] is ASCII_MINUS)
                    eventType = MacroKeyboardEventType.Decrease;
                else
                    return;

                if (commandBuffer[4] is not ASCII_SEMICOLON)
                    return;

                // Encoder status change detected

                if (isAsleep && eventType is MacroKeyboardEventType.Up or MacroKeyboardEventType.Increase or MacroKeyboardEventType.Decrease)
                {
                    isAsleep = false;
                    handler.NotifyAwake();
                }
                else
                {
                    handler.HandleEvent(MacroKeyboardEventSource.Encoder, encoder, eventType);
                }
            }
        }

        private void HandleSerialPortDataReceived(object? sender, EventArgs e)
        {
            int bytesRead = serialPort.ReadBytes(buffer, BUFFER_SIZE);

            for (int i = 0; i < bytesRead; i++)
            {
                if (buffer[i] is ASCII_CR or ASCII_LF)
                    continue;

                commandBuffer[commandBufferLength++] = buffer[i];
                if (commandBufferLength > BUFFER_SIZE)
                    commandBufferLength = BUFFER_SIZE;

                if (buffer[i] == ASCII_SEMICOLON)
                {
                    ProcessCommand();
                    commandBufferLength = 0;
                }
            }
        }

        protected void SendChangeColors(IReadOnlyList<ColorChangeRequest> colorChanges)
        {
            StringBuilder colorCommand = new StringBuilder();
            for (int i = 0; i < colorChanges.Count; i++)
                colorCommand.Append($"l{colorChanges[i].Index:00}{colorChanges[i].Color.R:000}{colorChanges[i].Color.G:000}{colorChanges[i].Color.B:000};");
            colorCommand.Append("s;");

            byte[] buffer = Encoding.ASCII.GetBytes(colorCommand.ToString());
            serialPort.WriteBytes(buffer, buffer.Length);
        }

        protected void SendSleep()
        {
            string command = "z;";
            byte[] buffer = Encoding.ASCII.GetBytes(command);
            serialPort.WriteBytes(buffer, buffer.Length);
        }

        protected void SetPort(int port)
        {
            if (serialPort.Opened)
                throw new InvalidOperationException("Can not set port when serial port connection is opened! Set up port in Init before call to base.Init.");

            this.comPort = port;
        }

        public BaseSerialPortDriver(IDriverHandler handler, ILog log, bool supportsSleep)
            : base(handler, log)
        {
            // Initialize

            buffer = new byte[BUFFER_SIZE];
            commandBuffer = new byte[BUFFER_SIZE + 1];
            commandBufferLength = 0;
            comPort = 1;

            serialPort = new SerialPort(log);
            serialPort.DataReceived += HandleSerialPortDataReceived;

            // Read settings

            this.supportsSleep = supportsSleep;
        }

        public override void Dispose()
        {
            serialPort.ClosePort();
        }

        public override bool Init()
        {
            serialPort.OpenPort($"COM{comPort}");
            return serialPort.Opened;
        }

        public override void Sleep()
        {
            if (supportsSleep)
            {
                SendSleep();
                isAsleep = true;
            }
        }
    }
}
