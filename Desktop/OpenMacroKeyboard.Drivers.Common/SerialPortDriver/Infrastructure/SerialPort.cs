﻿using OpenMacroKeyboard.API.V1;
using RJCP.IO.Ports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Drivers.Common.SerialPortDriver.Infrastructure
{
    public class SerialPort
    {
        // Private constants --------------------------------------------------

        private const string SerialPortLogSource = "Serial port service";

        // Private fields -----------------------------------------------------

        private SerialPortStream serialPortStream;
        private bool opened;
        private string? port;
        private readonly ILog log;

        // Private methods ----------------------------------------------------

        private void HandleSerialPortDataReceived(object? sender, SerialDataReceivedEventArgs e)
        {
#if DEBUG
            log.Information(SerialPortLogSource, $"Data received");
#endif
            DataReceived?.Invoke(this, EventArgs.Empty);
        }

        private void HandleSerialPortErrorReceived(object? sender, SerialErrorReceivedEventArgs e)
        {
#if DEBUG
            log.Warning(SerialPortLogSource, $"Serial port received error!");
#endif
        }

        // Public methods -----------------------------------------------------

        public SerialPort(ILog log)
        {
            this.log = log;

            serialPortStream = new SerialPortStream();
            serialPortStream.BaudRate = 115200;
            serialPortStream.DataReceived += HandleSerialPortDataReceived;
            serialPortStream.ErrorReceived += HandleSerialPortErrorReceived;
            serialPortStream.ReceivedBytesThreshold = 1;
            serialPortStream.ReadTimeout = 250;

            string[] ports = SerialPortStream.GetPortNames();
        }

        public void OpenPort(string port)
        {
            log.Information(SerialPortLogSource, $"Trying to open port {port}...");

            ClosePort();

            serialPortStream.PortName = port;

            try
            {
                log.Information(SerialPortLogSource, $"Opening port {port}...");
                serialPortStream.Open();

                if (serialPortStream.IsOpen)
                    log.Information(SerialPortLogSource, $"Opened port {port}...");
                else
                    log.Warning(SerialPortLogSource, $"Open call exited, but port is not opened");

                opened = true;
                this.port = port;

                DataReceived?.Invoke(this, EventArgs.Empty);
            }
            catch
            {
                log.Error(SerialPortLogSource, $"Failed to open port {port}...");

                opened = false;
                this.port = null;
            }
        }

        public void ClosePort()
        {
            log.Information(SerialPortLogSource, $"Closing port {port}...");

            if (serialPortStream.IsOpen)
            {
                serialPortStream.Close();
                opened = false;
                port = null;
            }
        }

        public string[] GetAvailablePorts() => SerialPortStream.GetPortNames().Distinct().ToArray();

        public int ReadBytes(byte[] byteBuffer, int count)
        {
#if DEBUG
            log.Information(SerialPortLogSource, $"Attempting to read {count} bytes...");
#endif

            if (!opened)
            {
#if DEBUG
                log.Warning(SerialPortLogSource, $"Port is not open, nothing got read.");
#endif
                return -1;
            }

            try
            {
                int result = serialPortStream.Read(byteBuffer, 0, count);
#if DEBUG
                log.Information(SerialPortLogSource, $"Managed to read {result} bytes.");
#endif

                return result;
            }
#if DEBUG
            catch (Exception e)
            {
                log.Error(SerialPortLogSource, $"Failed to read bytes: {e.Message}");
#else
            catch
            {
#endif
                ClosePort();
                return -1;
            }
        }

        public int WriteBytes(byte[] byteBuffer, int count)
        {
#if DEBUG
            log.Information(SerialPortLogSource, $"Attempting to write {count} bytes...");
#endif

            if (!opened)
            {
#if DEBUG
                log.Warning(SerialPortLogSource, $"Port is not open, nothing got written.");
#endif
                return -1;
            }

            try
            {
                serialPortStream.Write(byteBuffer, 0, count);
#if DEBUG
                log.Information(SerialPortLogSource, $"Managed to write {count} bytes.");
#endif
                return count;
            }
#if DEBUG
            catch (Exception e)
            {
                log.Error(SerialPortLogSource, $"Failed to write bytes: {e.Message}");
#else
            catch
            { 
#endif
                ClosePort();
                return -1;
            }
        }

        // Public properties --------------------------------------------------

        public string? Port => port;

        public bool Opened => opened;

        public event EventHandler? DataReceived;
    }
}
