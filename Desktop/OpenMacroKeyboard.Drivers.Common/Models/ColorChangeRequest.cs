﻿using OpenMacroKeyboard.API.V1.Drivers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Drivers.Common.Models
{
    public class ColorChangeRequest
    {
        public ColorChangeRequest(int index, KeyboardElementColor color)
        {
            Index = index;
            Color = color;
        }

        public int Index { get; }
        public KeyboardElementColor Color { get; } 
    }
}
