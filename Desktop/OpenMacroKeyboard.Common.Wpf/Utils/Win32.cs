﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Common.Wpf.Utils
{
    public static class Win32
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern bool GetCursorPos(out POINT pt);

        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            public int X;
            public int Y;

            public POINT(int x, int y)
            {
                X = x;
                Y = y;
            }
        }

        public static Point AbsoluteMousePosition
        {
            get
            {
                GetCursorPos(out POINT p);
                return new Point(p.X, p.Y);
            }
        }
    }
}
