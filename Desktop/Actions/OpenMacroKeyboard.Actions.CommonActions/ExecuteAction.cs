﻿using OpenMacroKeyboard.API.V1;
using OpenMacroKeyboard.API.V1.Actions;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Actions.CommonActions
{
    public class ExecuteAction : BaseAction
    {        
        private readonly string? path;
        private readonly string? parameters;
        private readonly string? workingDirectory;

        public ExecuteAction(ILog log, Dictionary<string, object> settings)
            : base(log)
        {
            this.path = settings.ContainsKey(Constants.Execute.KEY_PATH) ? settings[Constants.Execute.KEY_PATH] as string : null;
            this.parameters = settings.ContainsKey(Constants.Execute.KEY_PARAMETERS) ? settings[Constants.Execute.KEY_PARAMETERS] as string : null;
            this.workingDirectory = settings.ContainsKey(Constants.Execute.KEY_WORKING_DIRECTORY) ? settings[Constants.Execute.KEY_WORKING_DIRECTORY] as string : null;
        }

        public override void Execute()
        {
            if (path != null) 
            {
                ProcessStartInfo startInfo = new();
                startInfo.UseShellExecute = true;
                startInfo.FileName = path;
                if (!string.IsNullOrEmpty(workingDirectory))
                    startInfo.WorkingDirectory = workingDirectory;

                if (!string.IsNullOrEmpty(parameters))
                    startInfo.Arguments = parameters;

                try
                {
                    Process.Start(startInfo);
                }
                catch (Exception e)
                {
                    log.Error("Execute", $"Failed to start process {path} with parameters {parameters}. Error: {e.Message}");
                }
            }            
        }
    }
}
