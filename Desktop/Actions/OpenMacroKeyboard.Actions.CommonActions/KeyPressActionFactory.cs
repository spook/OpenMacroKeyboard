﻿using OpenMacroKeyboard.Actions.CommonActions.Types;
using OpenMacroKeyboard.API.V1;
using OpenMacroKeyboard.API.V1.Actions;
using OpenMacroKeyboard.API.V1.Actions.Models;
using OpenMacroKeyboard.API.V1.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Actions.CommonActions
{
    public class KeyPressActionFactory : BaseActionFactory
    {
        private readonly ActionMetadata actionMetadata;

        public KeyPressActionFactory(ILog log)
            : base(log)
        {
            actionMetadata = new ActionMetadata(Resources.Strings.Macro_KeyPress_Name, Resources.Strings.Macro_KeyPress_Group, Constants.KeyPress.Guid);

            var availableKeys = ((VirtualKeycodes[])Enum.GetValues(typeof(VirtualKeycodes)))
                .OfType<VirtualKeycodes>()
                .Select(vk => new SelectSettingEntryInfo(vk.ToString(), (int)vk))
                .OrderBy(sei => sei.Description)
                .ToList();

            actionMetadata.Settings.Add(new SelectSettingInfo(Constants.KeyPress.KEY_KEY, Resources.Strings.Macro_KeyPress_Label_Key, availableKeys[0].Value, availableKeys));
            actionMetadata.Settings.Add(new BoolSettingInfo(Constants.KeyPress.KEY_LEFT_CTRL, Resources.Strings.Macro_KeyPress_Label_HoldLeftControl, false));
            actionMetadata.Settings.Add(new BoolSettingInfo(Constants.KeyPress.KEY_RIGHT_CTRL, Resources.Strings.Macro_KeyPress_Label_HoldRightControl, false));
            actionMetadata.Settings.Add(new BoolSettingInfo(Constants.KeyPress.KEY_LEFT_SHIFT, Resources.Strings.Macro_KeyPress_Label_HoldLeftShift, false));
            actionMetadata.Settings.Add(new BoolSettingInfo(Constants.KeyPress.KEY_RIGHT_SHIFT, Resources.Strings.Macro_KeyPress_Label_HoldRightShift, false));
            actionMetadata.Settings.Add(new BoolSettingInfo(Constants.KeyPress.KEY_LEFT_ALT, Resources.Strings.Macro_KeyPress_Label_HoldLeftAlt, false));
            actionMetadata.Settings.Add(new BoolSettingInfo(Constants.KeyPress.KEY_RIGHT_ALT, Resources.Strings.Macro_KeyPress_Label_HoldRightAlt, false));
        }

        public override ActionMetadata Metadata => actionMetadata;

        public override BaseAction CreateAction(Dictionary<string, object> settings)
        {
            return new KeyPressAction(log, settings);
        }
    }
}
