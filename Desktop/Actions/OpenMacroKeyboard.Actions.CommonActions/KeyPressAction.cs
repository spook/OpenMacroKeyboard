﻿using OpenMacroKeyboard.Actions.CommonActions.Infrastructure;
using OpenMacroKeyboard.Actions.CommonActions.Types;
using OpenMacroKeyboard.API.V1;
using OpenMacroKeyboard.API.V1.Actions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Actions.CommonActions
{
    public class KeyPressAction : BaseAction
    {
        private readonly VirtualKeycodes key;
        private readonly bool leftControl;
        private readonly bool rightControl;
        private readonly bool leftShift;
        private readonly bool rightShift;
        private readonly bool leftAlt;
        private readonly bool rightAlt;

        public KeyPressAction(ILog log, Dictionary<string, object> settings) : base(log)
        {
            key = (VirtualKeycodes)settings[Constants.KeyPress.KEY_KEY];
            leftControl = (bool)settings[Constants.KeyPress.KEY_LEFT_CTRL];
            rightControl = (bool)settings[Constants.KeyPress.KEY_RIGHT_CTRL];
            leftShift = (bool)settings[Constants.KeyPress.KEY_LEFT_SHIFT];
            rightShift = (bool)settings[Constants.KeyPress.KEY_RIGHT_SHIFT];
            leftAlt = (bool)settings[Constants.KeyPress.KEY_LEFT_ALT];
            rightAlt = (bool)settings[Constants.KeyPress.KEY_RIGHT_ALT];
        }

        public override void Execute()
        {
            InputHandler.PressKey(key, leftControl, rightControl, leftShift, rightShift, leftAlt, rightAlt);
        }
    }
}
