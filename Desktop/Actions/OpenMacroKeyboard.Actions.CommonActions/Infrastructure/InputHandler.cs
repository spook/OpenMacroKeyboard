﻿using OpenMacroKeyboard.Actions.CommonActions.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Actions.CommonActions.Infrastructure
{
    public static class InputHandler
    {
        [StructLayout(LayoutKind.Sequential)]
        private struct KeyboardInput
        {
            public ushort wVk;
            public ushort wScan;
            public uint dwFlags;
            public uint time;
            public IntPtr dwExtraInfo;
        }

        [StructLayout(LayoutKind.Sequential)]
        private struct MouseInput
        {
            public int dx;
            public int dy;
            public uint mouseData;
            public uint dwFlags;
            public uint time;
            public IntPtr dwExtraInfo;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct HardwareInput
        {
            public uint uMsg;
            public ushort wParamL;
            public ushort wParamH;
        }

        [StructLayout(LayoutKind.Explicit)]
        private struct InputUnion
        {
            [FieldOffset(0)] public MouseInput mi;
            [FieldOffset(0)] public KeyboardInput ki;
            [FieldOffset(0)] public HardwareInput hi;
        }

        private struct Input
        {
            public int type;
            public InputUnion u;
        }

        [Flags]
        private enum InputType
        {
            Mouse = 0,
            Keyboard = 1,
            Hardware = 2
        }

        [Flags]
        private enum KeyEventF
        {
            KeyDown = 0x0000,
            ExtendedKey = 0x0001,
            KeyUp = 0x0002,
            Unicode = 0x0004,
            Scancode = 0x0008
        }

        [Flags]
        private enum MouseEventF
        {
            Absolute = 0x8000,
            HWheel = 0x01000,
            Move = 0x0001,
            MoveNoCoalesce = 0x2000,
            LeftDown = 0x0002,
            LeftUp = 0x0004,
            RightDown = 0x0008,
            RightUp = 0x0010,
            MiddleDown = 0x0020,
            MiddleUp = 0x0040,
            VirtualDesk = 0x4000,
            Wheel = 0x0800,
            XDown = 0x0080,
            XUp = 0x0100
        }

        [DllImport("user32.dll", SetLastError = true)]
        private static extern uint SendInput(uint nInputs, Input[] pInputs, int cbSize);

        [DllImport("user32.dll")]
        private static extern IntPtr GetMessageExtraInfo();

        private static Input BuildKeyInfo(VirtualKeycodes keycode, bool press)
        {
            return new Input
            {
                type = (int)InputType.Keyboard,
                u = new InputUnion
                {
                    ki = new KeyboardInput
                    {
                        wVk = (ushort)keycode,
                        wScan = 0,
                        dwFlags = (uint)(press ? KeyEventF.KeyDown : KeyEventF.KeyUp),
                        dwExtraInfo = GetMessageExtraInfo()
                    }
                }
            };
        }

        public static void PressKey(VirtualKeycodes key, bool leftCtrl, bool rightCtrl, bool leftShift, bool rightShift, bool leftAlt, bool rightAlt)
        {
            Input[] inputs = new Input[7];
            uint inputCount = 0;

            if (leftCtrl)
                inputs[inputCount++] = BuildKeyInfo(VirtualKeycodes.LeftCtrl, true);
            if (leftShift)
                inputs[inputCount++] = BuildKeyInfo(VirtualKeycodes.LeftShift, true);
            if (leftAlt)
                inputs[inputCount++] = BuildKeyInfo(VirtualKeycodes.LeftAlt, true);
            if (rightCtrl)
                inputs[inputCount++] = BuildKeyInfo(VirtualKeycodes.RightCtrl, true);
            if (rightShift)
                inputs[inputCount++] = BuildKeyInfo(VirtualKeycodes.RightShift, true);
            if (rightAlt)
                inputs[inputCount++] = BuildKeyInfo(VirtualKeycodes.RightAlt, true);
            inputs[inputCount++] = BuildKeyInfo(key, true);

            var result = SendInput((uint)inputCount, inputs, Marshal.SizeOf(typeof(Input)));

            Thread.Sleep(50);

            inputCount = 0;

            if (leftCtrl)
                inputs[inputCount++] = BuildKeyInfo(VirtualKeycodes.LeftCtrl, false);
            if (leftShift)
                inputs[inputCount++] = BuildKeyInfo(VirtualKeycodes.LeftShift, false);
            if (leftAlt)
                inputs[inputCount++] = BuildKeyInfo(VirtualKeycodes.LeftAlt, false);
            if (rightCtrl)
                inputs[inputCount++] = BuildKeyInfo(VirtualKeycodes.RightCtrl, false);
            if (rightShift)
                inputs[inputCount++] = BuildKeyInfo(VirtualKeycodes.RightShift, false);
            if (rightAlt)
                inputs[inputCount++] = BuildKeyInfo(VirtualKeycodes.RightAlt, false);
            inputs[inputCount++] = BuildKeyInfo(key, false);

            result = SendInput((uint)inputCount, inputs, Marshal.SizeOf(typeof(Input)));
        }
    }
}
