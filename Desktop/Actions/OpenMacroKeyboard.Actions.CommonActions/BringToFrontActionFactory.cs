﻿using OpenMacroKeyboard.API.V1;
using OpenMacroKeyboard.API.V1.Actions;
using OpenMacroKeyboard.API.V1.Actions.Models;
using OpenMacroKeyboard.API.V1.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Actions.CommonActions
{
    public class BringToFrontActionFactory : BaseActionFactory
    {
        private readonly ActionMetadata metadata;

        public BringToFrontActionFactory(ILog log)
            : base(log)
        {
            var settings = new List<BaseSettingInfo>
            {
                new StringSettingInfo(Constants.BringToFront.KEY_PROCESS, Resources.Strings.Macro_BringToFront_Label_Process, string.Empty),
                new StringSettingInfo(Constants.BringToFront.KEY_EXECUTABLE, Resources.Strings.Macro_BringToFront_Label_Executable, string.Empty),
                new StringSettingInfo(Constants.BringToFront.KEY_PARAMETERS, Resources.Strings.Macro_BringToFront_Label_Parameters, string.Empty)
            };

            metadata = new ActionMetadata(Resources.Strings.Macro_BringToFront_Name,
                Resources.Strings.Macro_BringToFront_Group,
                Constants.BringToFront.Guid,
                settings);
        }

        public override BaseAction CreateAction(Dictionary<string, object> settings)
        {
            string? process = settings.ContainsKey(Constants.BringToFront.KEY_PROCESS) ? settings[Constants.BringToFront.KEY_PROCESS] as string : null;
            string? executable = settings.ContainsKey(Constants.BringToFront.KEY_EXECUTABLE) ? settings[Constants.BringToFront.KEY_EXECUTABLE] as string : null;
            string? parameters = settings.ContainsKey(Constants.BringToFront.KEY_PARAMETERS) ? settings[Constants.BringToFront.KEY_PARAMETERS] as string : null;

            return new BringToFrontAction(log, process, executable, parameters);
        }

        public override ActionMetadata Metadata => metadata;
    }
}
