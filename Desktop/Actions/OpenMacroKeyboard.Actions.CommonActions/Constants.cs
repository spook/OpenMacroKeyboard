﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Actions.CommonActions
{
    public static class Constants
    {
        public static class KeyPress
        {
            public static readonly Guid Guid = new("77440629-6981-4738-A4B8-C25C8E2FFFF0");

            public const string KEY_KEY = "key";
            public const string KEY_LEFT_SHIFT = "left_shift";
            public const string KEY_RIGHT_SHIFT = "right_shift";
            public const string KEY_LEFT_CTRL = "left_ctrl";
            public const string KEY_RIGHT_CTRL = "right_ctrl";
            public const string KEY_LEFT_ALT = "left_alt";
            public const string KEY_RIGHT_ALT = "right_alt";
        }

        public static class BringToFront
        {
            public static readonly Guid Guid = new("93B9CF21-9A97-45B9-BD72-70F7E03E750A");

            public const string KEY_PROCESS = "process";
            public const string KEY_EXECUTABLE = "executable";
            public const string KEY_PARAMETERS = "parameters";
        }

        public static class Execute
        {
            public static readonly Guid Guid = new("DE9A09F3-015C-479F-B596-250EE3F1607D");

            public const string KEY_PATH = "path";
            public const string KEY_PARAMETERS = "parameters";
            public const string KEY_WORKING_DIRECTORY = "working_directory";
        }
    }
}
