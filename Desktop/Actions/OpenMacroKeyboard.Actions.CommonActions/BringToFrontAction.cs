﻿using OpenMacroKeyboard.API.V1;
using OpenMacroKeyboard.API.V1.Actions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Actions.CommonActions
{
    public class BringToFrontAction : BaseAction
    {
        private IntPtr instance;

        [DllImport("OpenMacroKeyboard.Actions.CommonActions.Native.dll")]
        private static extern IntPtr Create_SwitchTo([MarshalAs(UnmanagedType.LPWStr)] string? process,
            [MarshalAs(UnmanagedType.LPWStr)] string? executable,
            [MarshalAs(UnmanagedType.LPWStr)] string? parameters);

        [DllImport("OpenMacroKeyboard.Actions.CommonActions.Native.dll")]
        private static extern void Delete_SwitchTo(IntPtr instance);

        [DllImport("OpenMacroKeyboard.Actions.CommonActions.Native.dll")]
        private static extern void SwitchTo_Execute(IntPtr instance);

        public BringToFrontAction(ILog log, string? process, string? executable, string? parameters)
            : base(log)
        {
            instance = Create_SwitchTo(process, executable, parameters);
        }

        public override void Execute()
        {
            SwitchTo_Execute(instance);
        }

        public override void Dispose()
        {
            Delete_SwitchTo(instance);
            instance = IntPtr.Zero;
        }
    }
}
