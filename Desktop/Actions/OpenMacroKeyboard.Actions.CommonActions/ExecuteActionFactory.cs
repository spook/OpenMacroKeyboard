﻿using OpenMacroKeyboard.API.V1;
using OpenMacroKeyboard.API.V1.Actions;
using OpenMacroKeyboard.API.V1.Actions.Models;
using OpenMacroKeyboard.API.V1.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Actions.CommonActions
{
    public class ExecuteActionFactory : BaseActionFactory
    {
        private readonly ActionMetadata metadata;

        public ExecuteActionFactory(ILog log) 
            : base(log)
        {
            var settings = new List<BaseSettingInfo>
            {
                new StringSettingInfo(Constants.Execute.KEY_PATH, Resources.Strings.Macro_Execute_Label_Path, string.Empty),
                new StringSettingInfo(Constants.Execute.KEY_PARAMETERS, Resources.Strings.Macro_Execute_Label_Parameters, string.Empty),
                new StringSettingInfo(Constants.Execute.KEY_WORKING_DIRECTORY, Resources.Strings.Macro_Execute_Label_WorkingDirectory, string.Empty)
            };

            metadata = new ActionMetadata(Resources.Strings.Macro_Execute_Name,
                Resources.Strings.Macro_Execute_Group,
                Constants.Execute.Guid,
                settings);
        }

        public override ActionMetadata Metadata => metadata;

        public override BaseAction CreateAction(Dictionary<string, object> settings)
        {
            return new ExecuteAction(log, settings);
        }
    }
}
