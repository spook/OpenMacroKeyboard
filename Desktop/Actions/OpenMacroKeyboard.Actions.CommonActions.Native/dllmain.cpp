﻿// dllmain.cpp : Definiuje punkt wejścia dla aplikacji DLL.
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include "SwitchTo.h"

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

extern "C" __declspec(dllexport) void* __stdcall Create_SwitchTo(wchar_t * process, wchar_t * executable, wchar_t * parameters)
{
    return (void*)new omk_actions::SwitchTo(std::wstring(process), std::wstring(executable), std::wstring(parameters));
}

extern "C" __declspec(dllexport) void _stdcall Delete_SwitchTo(void* instance)
{
    delete ((omk_actions::SwitchTo*)instance);
}

extern "C" __declspec(dllexport) void __stdcall SwitchTo_Execute(void* instance) {
    ((omk_actions::SwitchTo*)instance)->execute();
}