#define WIN32_LEAN_AND_MEAN
#include "SwitchTo.h"

namespace omk_actions
{
	/// <summary>
	/// Compares two wide strings case insensitive
	/// </summary>
	bool SwitchTo::equals_case_insensitive(const std::wstring & a, const std::wstring & b)
	{
		// Different lengths means different strings
		if (a.length() != b.length())
			return false;

		// Compare character by character
		for (int i = 0; i < a.length(); i++)
		{
			if (towlower(a[i]) != towlower(b[i]))
				return false;
		}

		return true;
	}

	/// <summary>
	/// Checks, if given window is main window of the process
	/// </summary>
	bool SwitchTo::is_main_window(HWND handle)
	{
		return (GetWindow(handle, GW_OWNER) == (HWND)0) && (IsWindowVisible(handle));
	}

	/// <summary>
	/// Callback used for enumerating windows per processes
	/// </summary>
	/// <returns>TRUE if enumeration should be continued, else FALSE</returns>
	BOOL CALLBACK SwitchTo::enum_windows_callback(HWND handle, LPARAM lParam)
	{
		// If this is not main window, continue searching
		if (!is_main_window(handle))
			return TRUE;

		// Get process ID owning this window
		DWORD process_id = 0;
		GetWindowThreadProcessId(handle, &process_id);

		// Check if that process is on the list of processes
		std::vector<ProcessWindowsInfo>& data = *(std::vector<ProcessWindowsInfo>*)lParam;

		auto process = std::find_if(data.begin(), data.end(),
			[process_id](ProcessWindowsInfo& data) { return data.process_id == process_id; });

		// If it is, add found window to that process' window list
		if (process != data.end())
			(*process).window_handles.push_back(handle);

		return TRUE;
	}

	bool SwitchTo::has_no_windows(ProcessWindowsInfo& info)
	{
		return info.window_handles.size() == 0;
	}

	/// <summary>
	/// Finds main windows for given processes
	/// </summary>
	/// <param name="process_ids">A vector of process IDs for which windows should be found</param>
	std::vector<ProcessWindowsInfo> SwitchTo::find_windows(std::vector<DWORD> process_ids)
	{
		// Creates process_data entries
		std::vector<ProcessWindowsInfo> result;

		for (DWORD processId : process_ids)
		{
			ProcessWindowsInfo processData;
			processData.process_id = processId;
			result.push_back(processData);
		}

		// Collects main windows for given processes
		EnumWindows(enum_windows_callback, (LPARAM)&result);

		// Remove processes without windows
		result.erase(std::remove_if(result.begin(), result.end(), has_no_windows), result.end());

		// Sorts processes by their IDs (to provide some
		// deterministic way of ordering the processes)
		std::sort(result.begin(),
			result.end(),
			[](ProcessWindowsInfo& first, ProcessWindowsInfo& second) { return first.process_id > second.process_id ? 1 : 0; });

		// For each process
		for (ProcessWindowsInfo& process : result)
		{
			// Sorts windows by their handles (again, to
			// provide some deterministic way of ordering them)
			std::sort(process.window_handles.begin(),
				process.window_handles.end(),
				[](HWND& first, HWND& second) { return first > second ? 1 : 0; });
		}

		return result;
	}

	/// <summary>
	/// Finds all process IDs, which executable name matches
	/// given name (eg. notepad.exe)
	/// </summary>
	std::vector<DWORD> SwitchTo::find_process_ids(const std::wstring& processName)
	{
		// Collects information about running processes
		PROCESSENTRY32 processInfo{};
		processInfo.dwSize = sizeof(processInfo);
		std::vector<DWORD> result;

		HANDLE processesSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
		if (processesSnapshot == INVALID_HANDLE_VALUE)
			return result;

		// Collects all those, which executable name matches
		// the parameter
		bool keepProcessing = Process32First(processesSnapshot, &processInfo);
		while (keepProcessing)
		{
			std::wstring processExeFile = std::wstring(processInfo.szExeFile);
			if (equals_case_insensitive(processName, processExeFile))
				result.push_back(processInfo.th32ProcessID);

			keepProcessing = Process32Next(processesSnapshot, &processInfo);
		}

		CloseHandle(processesSnapshot);

		return result;
	}

	/// <summary>
	/// Brings given window to the front
	/// </summary>
	bool SwitchTo::bring_to_front(HWND hWnd)
	{
		bool result = false;

		// First restore if window is minimized

		WINDOWPLACEMENT placement{};
		placement.length = sizeof(placement);

		if (!GetWindowPlacement(hWnd, &placement))
			return false;

		bool minimized = placement.showCmd == SW_SHOWMINIMIZED;
		if (minimized)
			ShowWindow(hWnd, SW_RESTORE);

		// Then bring it to front using UI automation

		IUIAutomationElement* window = nullptr;
		if (SUCCEEDED(uiAutomation->ElementFromHandle(hWnd, &window)))
		{
			if (SUCCEEDED(window->SetFocus()))
			{
				result = true;
			}

			window->Release();
		}

		return result;
	}

	bool SwitchTo::find_active_window(std::vector<ProcessWindowsInfo>& processes, 
		HWND currentWindow, 
		int& processId, 
		int& windowId)
	{
		processId = 0;
		windowId = 0;

		// Try to locate current active window in the list
		while (processId < processes.size())
		{
			windowId = 0;
			while (windowId < processes[processId].window_handles.size())
			{
				if (processes[processId].window_handles[windowId] == currentWindow)
					return true;

				windowId++;
			}

			processId++;
		}

		return false;
	}

	/// <summary>
	/// Searches for all processes with given executable name and enumerates
	/// their windows. If any of those windows is already activated, tries
	/// to activate next in the list. Otherwise activates first one on the
	/// list.
	/// </summary>
	bool SwitchTo::activate_main_window(const std::wstring& processName)
	{
		// Find all processes matching given process executable name
		std::vector<DWORD> processIDs = find_process_ids(processName);

		// We need at least one
		if (processIDs.size() == 0)
			return false;

		// Find main windows for found processes
		std::vector<ProcessWindowsInfo> processes = find_windows(processIDs);

		// We need at least one process with a window
		if (processes.size() == 0)
			return false;

		// Check, which window is currently in the foreground
		HWND currentWindow = GetForegroundWindow();

		int processId = 0;
		int windowId = 0;
		bool found = find_active_window(processes, currentWindow, processId, windowId);

		if (found)
		{
			// If found, select next one on the list
			windowId++;

			// If we ran out of windows for this process, switch
			// to the next process.
			if (windowId >= processes[processId].window_handles.size())
			{
				processId++;
				windowId = 0;
			}

			// If we ran out of processes, switch to the first
			// process and first window.
			if (processId >= processes.size())
			{
				processId = 0;
				windowId = 0;
			}
		}
		else
		{
			// If not found, select first window on the list
			processId = 0;
			windowId = 0;
		}

		// If current window is the same as chosen one, it means, that
		// there is only one process with one window and it is already
		// active. Otherwise, try to bring window to front.
		if (currentWindow != processes[processId].window_handles[windowId])
		{
			// Try to bring to front given window.
			// Failure in bringing window to front shouldn't cause
			// application to be started, so return true anyway.
			bring_to_front(processes[processId].window_handles[windowId]);
		}

		return true;
	}

	// Starts process from given path
	void SwitchTo::start_process(const std::wstring& processPath, const std::wstring& parameters)
	{
		// Additional information

		STARTUPINFO si;
		PROCESS_INFORMATION pi;

		// Set the size of the structures

		ZeroMemory(&si, sizeof(si));
		si.cb = sizeof(si);
		ZeroMemory(&pi, sizeof(pi));

		std::shared_ptr<wchar_t> cmdLine(nullptr);

		if (parameters.length() > 0)
		{
			std::wstring cmdLineStr = L"\"" + processPath + L"\" " + parameters;

			// A requirement by CreateProcessW
			cmdLine = std::shared_ptr<wchar_t>(_wcsdup(cmdLineStr.c_str()));
		}

		// OutputDebugStringW(processPath.c_str());
		// OutputDebugStringW(cmdLine);

		// Start the program up
		CreateProcess(processPath.c_str(),   // The path
			cmdLine.get(),     // Command line
			NULL,              // Process handle not inheritable
			NULL,              // Thread handle not inheritable
			FALSE,             // Set handle inheritance to FALSE
			0,                 // No creation flags
			NULL,              // Use parent's environment block
			NULL,              // Use parent's starting directory 
			&si,               // Pointer to STARTUPINFO structure
			&pi                // Pointer to PROCESS_INFORMATION structure (removed extra parentheses)
		);

		// Close process and thread handles. 
		CloseHandle(pi.hProcess);
		CloseHandle(pi.hThread);
	}

	SwitchTo::SwitchTo(std::wstring process, std::wstring executable, std::wstring parameters)
	{
		IUIAutomation* uiAutomationPtr = nullptr;

		if (!SUCCEEDED(CoCreateInstance(__uuidof(CUIAutomation), NULL, CLSCTX_INPROC_SERVER, __uuidof(IUIAutomation), (void**)&(uiAutomationPtr))))
		{
			throw new std::exception("Failed to create instance of UI automation!");
		}

		this->uiAutomation = std::shared_ptr<IUIAutomation>(uiAutomationPtr, [](IUIAutomation* obj) { obj->Release(); });

		this->process = process;
		this->executable = executable;
		this->parameters = parameters;
	}

	void SwitchTo::execute()
	{
		if (this->process.length() == 0 || !activate_main_window(this->process))
		{
			if (this->executable.length() > 0)
			{
				start_process(this->executable, this->parameters);
			}
		}
	}

}