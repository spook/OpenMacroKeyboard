#pragma once

#include <Windows.h>
#include <tlhelp32.h>
#include <combaseapi.h>
#include <UIAutomationClient.h>
#include <string>
#include <vector>
#include <memory>
#include <algorithm>


namespace omk_actions
{
	/// <summary>
	/// Contains information about single process and associated windows
	/// </summary>
	struct ProcessWindowsInfo
	{
		unsigned long process_id;
		std::vector<HWND> window_handles;
	};

	class SwitchTo 
	{
	private:
		std::wstring process;
		std::wstring executable;
		std::wstring parameters;
		std::shared_ptr<IUIAutomation> uiAutomation;

		bool equals_case_insensitive(const std::wstring& a, const std::wstring& b);
		static bool is_main_window(HWND handle);
		static BOOL CALLBACK enum_windows_callback(HWND handle, LPARAM lParam);
		static bool has_no_windows(ProcessWindowsInfo& info);
		std::vector<ProcessWindowsInfo> find_windows(std::vector<DWORD> process_ids);
		std::vector<DWORD> find_process_ids(const std::wstring& processName);
		bool bring_to_front(HWND hWnd);
		static bool find_active_window(std::vector<ProcessWindowsInfo>& processes, HWND currentWindow, int& processId, int& windowId);
		bool activate_main_window(const std::wstring& processName);
		void start_process(const std::wstring& processPath, const std::wstring& parameters);

	public:
		SwitchTo(std::wstring process, std::wstring executable, std::wstring parameters);
		void execute();
	};
}