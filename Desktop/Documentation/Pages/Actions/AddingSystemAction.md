# Adding new system action

This is a checklist for adding a new system action:

| Place | Action |
|-------|--------|
| `OpenMacroKeyboard.BusinessLogic.Types` | Add new entry to `MacroType` enumeration |
| `OpenMacroKeyboard.BusinessLogic.Models.` `Configuration.MacroKeyboard.Screens.Macros` | Implement new macro class (derive from `BaseMacro`).<br /><br />The class must be designed as a readonly model.<br /><br /> Make sure to add appropriate attributes for `MessagePack` to work correctly, namely `MessagePackObject` and `Key` |
| `OpenMacroKeyboard.BusinessLogic.Models.` `Configuration.MacroKeyboard.Screens.Macros` | Add new `[XmlIncludeDerived]` attribute to `BaseMacro` class |
| `OpenMacroKeyboard.BusinessLogic.Models.` `Configuration.MacroKeyboard.Screens.Macros` | Add new `MessagePack.Union` attribute to `BaseMacro` class to allow for proper resolution during serialization |
| `OpenMacroKeyboard.BusinessLogic.ViewModels.` `EditMacros.MacroEditors` | Implement new macro editor class (deriving from `BaseMacroEditorViewModel`) <br /><br />The constructor must accept an optional parameter containing the current setting(s) of the macro, defaulted to `null`. |
| `OpenMacroKeyboard.BusinessLogic.ViewModels.` `EditMacros.MacroEditors` | Implement new case in switch in method `BaseMacroEditorViewModel.FromMacro` |
| `OpenMacroKeyboard.Controls.MacroEditors` | Create new control, which will serve as an editor for new macro. Use one of existing controls as a guide. |
| `OpenMacroKeyboard.Windows` | Add a new `DataTemplate` to `Border.Resources` in `EditMacrosWindow.xaml`, so that new editor will be displayed when user adds the new macro |
| `OpenMacroKeyboard.BusinessLogic.ViewModels.` `EditMacros.MacroDirectory` | Add new entry in switch in `MacroDirectoryEntryViewModel.ToNewMacroEditor`, so that macro editor will be properly instantiated when user drags it into a slot |
| `OpenMacroKeyboard.BusinessLogic.Services.` `DriverRepository.MacroInfos` | Implement MacroInfo for new system macro. <br /><br />Fill switch in `BaseMacroInfo` class |
| `OpenMacroKeyboard.BusinessLogic.Services.` `DriverRepository` | Implement switch case in `MacroKeyboard.ExecuteMacro` method |
| `OpenMacroKeyboard.BusinessLogic.ViewModels.` `EditMacros.MacroEditors.MultiMacroEditorViewModel` | Consider adding new system maccro type to the switch statement |
