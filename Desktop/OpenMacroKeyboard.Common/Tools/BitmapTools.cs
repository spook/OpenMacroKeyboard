﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Common.Tools
{
    public static class BitmapTools
    {
        public static Bitmap BitmapFromArgb(byte[] argb, int width, int height)
        {
            if (width <= 0 || 
                height <= 0 ||
                argb == null || 
                argb.Length < width * height * 4)
                throw new InvalidOperationException("Invalid parameters, cannot convert to bitmap!");

            Bitmap bmp = new Bitmap(width, height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            for (int y = 0; y < height; y++)
            {
                var data = bmp.LockBits(new Rectangle(0, y, width, 1), System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                Marshal.Copy(argb, y * width * 4, data.Scan0, width * 4);
                bmp.UnlockBits(data);
            }

            return bmp;
        }

        public static (byte[] argb, int width, int height) ArgbFromBitmap(Bitmap bmp)
        {
            byte[] result = new byte[bmp.Width * bmp.Height * 4];

            for (int y = 0; y < bmp.Height; y++)
            {
                var data = bmp.LockBits(new Rectangle(0, y, bmp.Width, 1), System.Drawing.Imaging.ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                Marshal.Copy(data.Scan0, result, y * bmp.Width * 4, bmp.Width * 4);
                bmp.UnlockBits(data);
            }

            return (result, bmp.Width, bmp.Height);
        }
    }
}
