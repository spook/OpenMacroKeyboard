using Autofac;
using Hardcodet.Wpf.TaskbarNotification;
using OpenMacroKeyboard.BusinessLogic.Services;
using OpenMacroKeyboard.BusinessLogic.Services.Dialogs;
using OpenMacroKeyboard.BusinessLogic.Services.UIThreadAccess;
using OpenMacroKeyboard.BusinessLogic.ViewModels.App;
using OpenMacroKeyboard.Dependencies;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace OpenMacroKeyboard
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application, IAppAccess, IUIThreadAccess
    {
        private Mutex singleInstanceMutex;
        private TaskbarIcon taskbarIcon;
        private AppViewModel viewModel;

        private void ConfigureContainer(ContainerBuilder builder)
        {
            OpenMacroKeyboard.Dependencies.Configuration.Configure(builder);
            builder.RegisterInstance(this).As<IUIThreadAccess>();
        }

        private void HandleUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            var dialogService = Container.Instance.Resolve<IDialogService>() ?? throw new InvalidOperationException("Critical error: failed to resolve DialogService!");
            dialogService.ShowExceptionDialog(e.Exception);
            e.Handled = true;
        }

        private TaskbarIcon InitializeTaskbarIcon(AppViewModel viewModel)
        {
            var taskbarIcon = new TaskbarIcon();
            taskbarIcon.Icon = new Icon(GetResourceStream(new Uri(@"pack://application:,,,/icon.ico")).Stream);
            taskbarIcon.LeftClickCommand = viewModel.ShowMainWindowCommand;

            var menu = new ContextMenu();

            var showMainWindowItem = new MenuItem { Command = viewModel.ShowMainWindowCommand, Header = OpenMacroKeyboard.Resources.App.Strings.Command_OpenConfiguration };
            menu.Items.Add(showMainWindowItem);

            var separator = new Separator();
            menu.Items.Add(separator);

            var closeItem = new MenuItem { Command = viewModel.ExitCommand, Header = OpenMacroKeyboard.Resources.App.Strings.Command_Exit };
            menu.Items.Add(closeItem);

            taskbarIcon.ContextMenu = menu;

            return taskbarIcon;
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            // Check single instance

            this.singleInstanceMutex = new Mutex(true, "Spooksoft.OpenMacroKeyboar", out bool isNewInstance);
            if (!isNewInstance)
            {
                MessageBox.Show(OpenMacroKeyboard.Resources.App.Strings.Message_ApplicationIsAlreadyRunning);
                App.Current.Shutdown();
                return;
            }

            // Resolve viewmodel

            this.viewModel = Container.Instance.Resolve<AppViewModel>(new NamedParameter("access", this)) ?? throw new InvalidOperationException("Critical error: failed to resolve AppViewModel!");
            this.taskbarIcon = InitializeTaskbarIcon(viewModel) ?? throw new InvalidOperationException("Critical error: failed to resolve TaskbarIcon!");

            // Preload main windows

            var dialogService = Container.Instance.Resolve<IDialogService>();
            dialogService.PreloadMainWindows();

        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);

            // Make sure all services are disposed - in particular ControllerService,
            // which holds all driver libraries and drivers
            Container.Instance.Dispose();
        }

        void IAppAccess.Close()
        {
            this.Shutdown();
        }

#pragma warning disable CS8618
        public App()
        {
            // Uncomment for capturing video
            //
            // Thread.CurrentThread.CurrentCulture = CultureInfo.GetCultureInfo("en-US");
            // Thread.CurrentThread.CurrentUICulture = CultureInfo.GetCultureInfo("en-US");

            // Configure exception catching

            DispatcherUnhandledException += HandleUnhandledException;

            // Build container

            Container.BuildContainer(ConfigureContainer);
        }

        public void RunOnUIThread(Action action)
        {
            this.Dispatcher.BeginInvoke(action, DispatcherPriority.Normal);
        }
    }
#pragma warning restore CS8618
}
