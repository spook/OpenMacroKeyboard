﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Services.Dialogs
{
    public interface IResultDialog<out TResult>
        where TResult : class?
    {
        TResult? Result { get; }
    }
}
