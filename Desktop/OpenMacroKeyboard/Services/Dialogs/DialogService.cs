﻿using Microsoft.Win32;
using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.BusinessLogic.Models.AddMacroKeyboard;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens;
using OpenMacroKeyboard.BusinessLogic.Services.Dialogs;
using OpenMacroKeyboard.BusinessLogic.Services.DriverRepository;
using OpenMacroKeyboard.BusinessLogic.ViewModels.Reconnect;
using OpenMacroKeyboard.Resources;
using OpenMacroKeyboard.Windows;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace OpenMacroKeyboard.Services.Dialogs
{
    internal class DialogService : IDialogService
    {
        private readonly Stack<Window> dialogs = new();

        private MacroKeyboardPreviewWindow? previewWindow;
        private MainWindow? mainWindow;

        public DialogService()
        {
            previewWindow = null;
        }

        private (bool result, TResult? data) ShowDialog<TDialog, TResult>(TDialog dialog)
            where TDialog : Window, IResultDialog<TResult>
            where TResult : class?
        {
            try
            {
                dialog.Owner = dialogs.Any() ? dialogs.Peek() : mainWindow;
                dialogs.Push(dialog);

                if (dialog.ShowDialog() == true)
                {
                    return (true, dialog.Result);
                }
                else
                {
                    return (false, null);
                }
            }
            finally
            {
                if (dialogs.Peek() != dialog)
                    throw new InvalidOperationException("Dialog stack got corrupted!");

                dialogs.Pop();
            }
        }

        public (bool result, string? path) ShowOpenDialog(string? filter = null, string? title = null, string? filename = null)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            if (filename != null)
                dialog.FileName = filename;

            if (filter != null)
                dialog.Filter = filter;
            else
                dialog.Filter = Resources.Common.Strings.DefaultFilter;

            if (title != null)
                dialog.Title = title;
            else
                dialog.Title = Resources.Common.Strings.DefaultDialogTitle;

            if (dialog.ShowDialog() == true)
                return (true, dialog.FileName);
            else
                return (false, null);
        }

        public (bool result, string? path) ShowSaveDialog(string? filter = null, string? title = null, string? filename = null)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            if (filename != null)
                dialog.FileName = filename;

            if (filter != null)
                dialog.Filter = filter;
            else
                dialog.Filter = Resources.Common.Strings.DefaultFilter;

            if (title != null)
                dialog.Title = title;
            else
                dialog.Title = Resources.Common.Strings.DefaultDialogTitle;

            if (dialog.ShowDialog() == true)
                return (true, dialog.FileName);
            else
                return (false, null);
        }

        public void ShowExceptionDialog(Exception e)
        {
            var dialog = new ExceptionWindow(e);
            dialog.ShowDialog();
        }

        public (bool result, AddMacroKeyboardResult? model) ShowAddKeyboardDialog()
        {
            var dialog = new AddMacroKeyboardWindow();
            return ShowDialog<AddMacroKeyboardWindow, AddMacroKeyboardResult?>(dialog);            
        }

        public (bool result, IReadOnlyDictionary<string, object>? newSettings) ShowReconnectDialog(DriverMetadata driverMetadata, IReadOnlyDictionary<string, object> currentSettings)
        {
            var dialog = new ReconnectMacroKeyboardWindow(driverMetadata, currentSettings);
            return ShowDialog<ReconnectMacroKeyboardWindow, IReadOnlyDictionary<string, object>?>(dialog);
            
        }

        public (bool result, Screen? edited) ShowMacroEditorDialog(DriverMetadata driverMetadata, Screen? current)
        {
            var dialog = new EditMacrosWindow(driverMetadata, current);
            return ShowDialog<EditMacrosWindow, Screen?>(dialog);
        }

        public void ShowMacroKeyboardPreview(IMacroKeyboard macroKeyboard)
        {
            previewWindow ??= new();

            previewWindow.SetMacroKeyboard(macroKeyboard);
            previewWindow.ShowTimed();
        }

        public void ShowMainWindow()
        {
            mainWindow ??= new();
            mainWindow.Show();
        }

        public void PreloadMainWindows()
        {
            mainWindow ??= new();
            previewWindow ??= new();
        }
    }
}
