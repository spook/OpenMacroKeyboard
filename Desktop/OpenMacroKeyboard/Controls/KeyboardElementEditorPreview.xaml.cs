﻿using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroDirectory;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroKeyboard;
using OpenMacroKeyboard.Common.Wpf.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OpenMacroKeyboard.Controls
{
    /// <summary>
    /// Logika interakcji dla klasy KeyboardElementEditorPreview.xaml
    /// </summary>
    public partial class KeyboardElementEditorPreview : UserControl
    {
        private class ViewModelHolder
        {
            public ViewModelHolder(BaseElementEditorViewModel viewModel)
            {
                ViewModel = viewModel;
            }

            public BaseElementEditorViewModel ViewModel { get; }
        }

        private BaseElementEditorViewModel? viewModel;

        private DragAdorner? dragAdorner;
        private bool inDragDrop;
        private bool lBtnDown;

        private EntryDropAdorner? dropAdorner;

        private void AddDropAdorner(UIElement uiElement)
        {
            dropAdorner = new EntryDropAdorner(uiElement);
            var layer = AdornerLayer.GetAdornerLayer(uiElement);
            layer.Add(dropAdorner);
        }

        private void RemoveDropAdorner(UIElement uiElement)
        {
            var layer = AdornerLayer.GetAdornerLayer(uiElement);
            layer.Remove(dropAdorner);
            dropAdorner = null;
        }

        public KeyboardElementEditorPreview()
        {
            InitializeComponent();
            inDragDrop = false;
            lBtnDown = false;
        }
                
        private void HandleMouseDown(object sender, MouseButtonEventArgs e)
        {
            inDragDrop = false;

            if (e.ChangedButton == MouseButton.Left)
                lBtnDown = true;            
        }

        private void HandleMouseMove(object sender, MouseEventArgs e)
        {
            if (lBtnDown && e.LeftButton == MouseButtonState.Pressed && viewModel != null && !inDragDrop)
            {
                inDragDrop = true;
                try
                {
                    FrameworkElement? parent = sender as FrameworkElement;

                    while (parent is not null && parent is not ListBox)
                        parent = VisualTreeHelper.GetParent(parent) as FrameworkElement;

                    if (parent != null)
                    {
                        var adornerLayer = AdornerLayer.GetAdornerLayer(parent);

                        var localDragAdorner = new DragAdorner(this, e.GetPosition(this));
                        dragAdorner = localDragAdorner;
                        adornerLayer.Add(dragAdorner);

                        try
                        {
                            DragDrop.DoDragDrop(this, new ViewModelHolder(viewModel), DragDropEffects.Move);
                        }
                        finally
                        {
                            adornerLayer.Remove(localDragAdorner);
                            dragAdorner = null;
                        }
                    }
                    else
                    {
                        DragDrop.DoDragDrop(this, new ViewModelHolder(viewModel), DragDropEffects.Move);
                    }
                }
                finally
                {
                    inDragDrop = false;
                }
            }
        }

        private void HandleMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                if (inDragDrop)
                {
                    inDragDrop = false;
                }

                lBtnDown = false;
            }
        }

        private void HandleDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            viewModel = e.NewValue as BaseElementEditorViewModel;
        }

        private void HandleGiveFeedback(object sender, GiveFeedbackEventArgs e)
        {
            if (dragAdorner != null)
            {
                FrameworkElement? element = sender as FrameworkElement;
                if (element != null)
                {
                    var pos = element.PointFromScreen(Win32.AbsoluteMousePosition);
                    dragAdorner.UpdatePosition(pos);
                }
            }
        }

        private void HandleDragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetData(typeof(ViewModelHolder)) is ViewModelHolder draggedVM &&
                viewModel != null &&
                viewModel != draggedVM.ViewModel &&
                viewModel.GetType() == draggedVM.ViewModel.GetType())
            {
                e.Handled = true;
                e.Effects = DragDropEffects.Move;

                FrameworkElement? parent = sender as FrameworkElement;

                while (parent is not null && parent is not UserControl)
                    parent = VisualTreeHelper.GetParent(parent) as FrameworkElement;

                if (parent != null)
                    AddDropAdorner(parent);
            }
            else if (e.Data.GetData(typeof(MacroDirectoryEntryViewModel)) is MacroDirectoryEntryViewModel macroDirectory &&
                viewModel != null)
            {
                e.Handled = true;
                e.Effects = DragDropEffects.Copy;

                FrameworkElement? parent = sender as FrameworkElement;
                while (parent is not null && parent is not UserControl)
                    parent = VisualTreeHelper.GetParent(parent) as FrameworkElement;

                if (parent != null)
                    AddDropAdorner(parent);
            }
        }

        private void HandleDragLeave(object sender, DragEventArgs e)
        {
            if (dropAdorner != null)
            {
                e.Handled = true;

                FrameworkElement? parent = sender as FrameworkElement;

                while (parent is not null && parent is not UserControl)
                    parent = VisualTreeHelper.GetParent(parent) as FrameworkElement;

                if (parent != null)
                    RemoveDropAdorner(parent);
            }
        }

        private void HandleDrop(object sender, DragEventArgs e)
        {
            if (dropAdorner != null)
            {
                FrameworkElement? parent = sender as FrameworkElement;

                while (parent is not null && parent is not UserControl)
                    parent = VisualTreeHelper.GetParent(parent) as FrameworkElement;

                if (parent != null)
                    RemoveDropAdorner(parent);
            }

            if (e.Data.GetData(typeof(ViewModelHolder)) is ViewModelHolder draggedVM &&
                viewModel != null &&
                viewModel != draggedVM.ViewModel &&
                viewModel.GetType() == draggedVM.ViewModel.GetType())
            {
                e.Handled = true;

                viewModel.RequestExchangeContent(draggedVM.ViewModel);
            }
            else if (e.Data.GetData(typeof(MacroDirectoryEntryViewModel)) is MacroDirectoryEntryViewModel macroDirectoryEntry &&
                viewModel != null)
            {
                e.Handled = true;

                viewModel.RequestSetMacroFromPreview(macroDirectoryEntry);
            }
        }

        private void HandleMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            viewModel?.NotifyDoubleClicked();
        }
    }
}
