﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace OpenMacroKeyboard.Controls
{
    public class ExclamationIcon : Control
    {
        #region Errors dependency property

        public object Errors
        {
            get { return (object)GetValue(ErrorsProperty); }
            set { SetValue(ErrorsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Errors.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ErrorsProperty =
            DependencyProperty.Register("Errors", typeof(object), typeof(ExclamationIcon), new PropertyMetadata(null));

        #endregion
    }
}
