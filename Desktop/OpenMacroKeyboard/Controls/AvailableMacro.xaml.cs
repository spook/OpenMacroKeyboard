﻿using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroDirectory;
using OpenMacroKeyboard.Common.Wpf.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OpenMacroKeyboard.Controls
{
    /// <summary>
    /// Logika interakcji dla klasy AvailableMacro.xaml
    /// </summary>
    public partial class AvailableMacro : Border
    {
        private MacroDirectoryEntryViewModel? viewModel;

        private DragAdorner? dragAdorner;
        private bool inDragDrop;
        private bool lBtnDown;

        public AvailableMacro()
        {
            InitializeComponent();
            inDragDrop = false;
            lBtnDown = false;
        }

        private void HandleMouseDown(object sender, MouseButtonEventArgs e)
        {
            inDragDrop = false;

            if (e.ChangedButton == MouseButton.Left)
                lBtnDown = true;
        }

        private void HandleMouseMove(object sender, MouseEventArgs e)
        {
            if (lBtnDown && e.LeftButton == MouseButtonState.Pressed && viewModel != null && !inDragDrop)
            {
                inDragDrop = true;
                try
                {
                    FrameworkElement? parent = sender as FrameworkElement;

                    while (parent is not null && parent is not Window)
                        parent = VisualTreeHelper.GetParent(parent) as FrameworkElement;

                    var window = parent as Window;

                    if (window?.Content is UIElement windowContent)
                    {
                        var adornerLayer = AdornerLayer.GetAdornerLayer(windowContent);

                        var localDragAdorner = new DragAdorner(this, e.GetPosition(this));
                        dragAdorner = localDragAdorner;
                        adornerLayer.Add(dragAdorner);
                        
                        try
                        {
                            DragDrop.DoDragDrop(this, viewModel, DragDropEffects.Copy);
                        }
                        finally
                        {
                            adornerLayer.Remove(localDragAdorner);
                            dragAdorner = null;
                        }
                    }
                    else
                    {
                        DragDrop.DoDragDrop(this, viewModel, DragDropEffects.Copy);
                    }
                }
                finally
                {
                    inDragDrop = false;
                }
            }
        }

        private void HandleMouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                if (inDragDrop)
                {
                    inDragDrop = false;
                }

                lBtnDown = false;
            }
        }

        private void HandleDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            viewModel = e.NewValue as MacroDirectoryEntryViewModel;
        }

        private void HandleGiveFeedback(object sender, GiveFeedbackEventArgs e)
        {
            if (dragAdorner != null)
            {
                FrameworkElement? element = sender as FrameworkElement;
                if (element != null)
                {
                    var pos = element.PointFromScreen(Win32.AbsoluteMousePosition);
                    dragAdorner.UpdatePosition(pos);
                }
            }
        }
    }
}
