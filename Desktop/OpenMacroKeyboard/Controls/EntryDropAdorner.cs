﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows;
using System.Windows.Documents;

namespace OpenMacroKeyboard.Controls
{
    public class EntryDropAdorner : Adorner
    {
        private Brush brush;

        public EntryDropAdorner(UIElement adornedElement)
            : base(adornedElement)
        {
            brush = new SolidColorBrush(Color.FromArgb(128, SystemColors.HighlightColor.R, SystemColors.HighlightColor.G, SystemColors.HighlightColor.B));
            IsHitTestVisible = false;
        }

        public void UpdatePosition(double locationY)
        {
            InvalidateVisual();
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            drawingContext.DrawRectangle(brush, null, new Rect(0, 0, this.RenderSize.Width, this.RenderSize.Height));
        }
    }
}
