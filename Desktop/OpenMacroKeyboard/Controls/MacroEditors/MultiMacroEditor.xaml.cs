﻿using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroDirectory;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroEditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OpenMacroKeyboard.Controls.MacroEditors
{
    /// <summary>
    /// Logika interakcji dla klasy MultiMacroEditor.xaml
    /// </summary>
    public partial class MultiMacroEditor : StackPanel
    {
        // Private fields -----------------------------------------------------

        private MultiMacroEditorViewModel? viewModel;
        private EntryDropAdorner? dropAdorner;

        // Private methods ----------------------------------------------------

        private void AddDropAdorner(UIElement uiElement)
        {
            dropAdorner = new EntryDropAdorner(uiElement);
            var layer = AdornerLayer.GetAdornerLayer(uiElement);
            layer.Add(dropAdorner);
        }

        private void RemoveDropAdorner(UIElement uiElement)
        {
            var layer = AdornerLayer.GetAdornerLayer(uiElement);
            layer.Remove(dropAdorner);
            dropAdorner = null;
        }

        private void HandleListBoxDragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetData(typeof(MacroDirectoryEntryViewModel)) is MacroDirectoryEntryViewModel availableMacroViewModel && 
                viewModel != null && 
                viewModel.CanAddMacro(availableMacroViewModel))
            {
                e.Handled = true;
                e.Effects = DragDropEffects.Copy;

                FrameworkElement? parent = sender as FrameworkElement;

                while (parent is not null && parent is not ListBox)
                    parent = VisualTreeHelper.GetParent(parent) as FrameworkElement;

                if (parent != null)
                    AddDropAdorner(parent);
            }
        }

        private void HandleMultiMacroEditorListDragLeave(object sender, DragEventArgs e)
        {
            if (dropAdorner != null)
            {
                e.Handled = true;

                FrameworkElement? parent = sender as FrameworkElement;

                while (parent is not null && parent is not ListBox)
                    parent = VisualTreeHelper.GetParent(parent) as FrameworkElement;

                if (parent != null)
                    RemoveDropAdorner(parent);
            }
        }

        private void HandleMultiMacroEditorListDragDrop(object sender, DragEventArgs e)
        {
            if (dropAdorner != null)
            {
                FrameworkElement? parent = sender as FrameworkElement;

                while (parent is not null && parent is not ListBox)
                    parent = VisualTreeHelper.GetParent(parent) as FrameworkElement;

                if (parent != null)
                    RemoveDropAdorner(parent);
            }

            if (e.Data.GetData(typeof(MacroDirectoryEntryViewModel)) is MacroDirectoryEntryViewModel availableMacroViewModel &&
                viewModel != null && 
                viewModel.CanAddMacro(availableMacroViewModel))
            {
                e.Handled = true;
                viewModel.RequestAddMacro(availableMacroViewModel);
            }
        }

        private void HandleDataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            viewModel = e.NewValue as MultiMacroEditorViewModel;
        }

        // Public methods -----------------------------------------------------

        public MultiMacroEditor()
        {
            InitializeComponent();
        }
    }
}
