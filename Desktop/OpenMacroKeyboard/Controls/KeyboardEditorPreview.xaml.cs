﻿using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroKeyboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OpenMacroKeyboard.Controls
{
    /// <summary>
    /// Logika interakcji dla klasy KeyboardEditorPreview.xaml
    /// </summary>
    public partial class KeyboardEditorPreview : ListBox
    {
        public KeyboardEditorPreview()
        {
            InitializeComponent();
        }
    }
}
