﻿using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroDirectory;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroKeyboard;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace OpenMacroKeyboard.Controls
{
    /// <summary>
    /// Logika interakcji dla klasy MacroContainer.xaml
    /// </summary>
    public partial class MacroContainer : UserControl
    {
        private EntryDropAdorner? dropAdorner;        

        private MacroEditorSlotViewModel? viewModel;

        private void AddDropAdorner(UIElement uiElement)
        {
            dropAdorner = new EntryDropAdorner(uiElement);
            var layer = AdornerLayer.GetAdornerLayer(uiElement);
            layer.Add(dropAdorner);
        }

        private void RemoveDropAdorner(UIElement uiElement)
        {
            var layer = AdornerLayer.GetAdornerLayer(uiElement);
            layer.Remove(dropAdorner);
            dropAdorner = null;
        }

        private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            viewModel = e.NewValue as MacroEditorSlotViewModel;
        }

        private void HandleDragEnter(object sender, DragEventArgs e)
        {
            var availableMacroViewModel = e.Data.GetData(typeof(MacroDirectoryEntryViewModel));

            if (availableMacroViewModel != null && viewModel != null && viewModel.CanReplaceMacro)
            {
                e.Handled = true;
                e.Effects = DragDropEffects.Copy;

                FrameworkElement? parent = sender as FrameworkElement;

                while (parent is not null && parent is not UserControl)
                    parent = VisualTreeHelper.GetParent(parent) as FrameworkElement;

                if (parent != null)
                    AddDropAdorner(parent);
            }
        }

        private void HandleDragLeave(object sender, DragEventArgs e)
        {
            if (dropAdorner != null)
            {
                e.Handled = true;

                FrameworkElement? parent = sender as FrameworkElement;

                while (parent is not null && parent is not UserControl)
                    parent = VisualTreeHelper.GetParent(parent) as FrameworkElement;

                if (parent != null)
                    RemoveDropAdorner(parent);
            }
        }

        private void HandleDrop(object sender, DragEventArgs e)
        {
            if (dropAdorner != null)
            {
                FrameworkElement? parent = sender as FrameworkElement;

                while (parent is not null && parent is not UserControl)
                    parent = VisualTreeHelper.GetParent(parent) as FrameworkElement;

                if (parent != null)
                    RemoveDropAdorner(parent);
            }

            if (e.Data.GetData(typeof(MacroDirectoryEntryViewModel)) is MacroDirectoryEntryViewModel availableMacroViewModel && 
                viewModel != null && 
                viewModel.CanReplaceMacro)
            {
                e.Handled = true;

                viewModel.RequestReplaceMacro((MacroDirectoryEntryViewModel)availableMacroViewModel);
            }
        }

        public MacroContainer()
        {
            InitializeComponent();
        }
    }
}
