﻿using Autofac;
using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.BusinessLogic.ViewModels.Reconnect;
using OpenMacroKeyboard.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OpenMacroKeyboard.Windows
{
    /// <summary>
    /// Logika interakcji dla klasy ReconnectMacroKeyboardWindow.xaml
    /// </summary>
    public partial class ReconnectMacroKeyboardWindow : Window, IReconnectWindowAccess, IResultDialog<IReadOnlyDictionary<string, object>?>
    {
        private readonly ReconnectWindowViewModel viewModel;

        void IReconnectWindowAccess.Close(bool result)
        {
            DialogResult = result;
            Close();
        }

        public ReconnectMacroKeyboardWindow(DriverMetadata driverMetadata, IReadOnlyDictionary<string, object> currentSettings)
        {
            InitializeComponent();

            viewModel = Dependencies.Container.Instance.Resolve<ReconnectWindowViewModel>(new NamedParameter("access", this), 
                new NamedParameter("driverMetadata", driverMetadata),
                new NamedParameter("currentSettings", currentSettings));
            DataContext = viewModel;
        }

        public IReadOnlyDictionary<string, object>? Result => viewModel.Result;
    }
}
