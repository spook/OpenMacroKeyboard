﻿<Window x:Class="OpenMacroKeyboard.Windows.EditMacrosWindow"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:e="clr-namespace:OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroKeyboard;assembly=OpenMacroKeyboard.BusinessLogic"
        xmlns:local="clr-namespace:OpenMacroKeyboard.Windows" 
        xmlns:vm="clr-namespace:OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros;assembly=OpenMacroKeyboard.BusinessLogic" 
        xmlns:c="clr-namespace:OpenMacroKeyboard.Controls"
        xmlns:ci="clr-namespace:OpenMacroKeyboard.Controls.Icons"
        xmlns:mkvm="clr-namespace:OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroKeyboard;assembly=OpenMacroKeyboard.BusinessLogic"
        xmlns:mevm="clr-namespace:OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroEditors;assembly=OpenMacroKeyboard.BusinessLogic"
        xmlns:dvm="clr-namespace:OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroDirectory;assembly=OpenMacroKeyboard.BusinessLogic"
        xmlns:cme="clr-namespace:OpenMacroKeyboard.Controls.MacroEditors"
        xmlns:res="clr-namespace:OpenMacroKeyboard.Resources.Windows.EditMacros;assembly=OpenMacroKeyboard.Resources"
        xmlns:cres="clr-namespace:OpenMacroKeyboard.Resources.Common;assembly=OpenMacroKeyboard.Resources"
        xmlns:sys="clr-namespace:System;assembly=netstandard"
        d:DataContext="{d:DesignInstance Type=vm:EditMacrosWindowViewModel}"
        mc:Ignorable="d" WindowStartupLocation="CenterOwner"
        Title="{x:Static res:Strings.Title}" SizeToContent="WidthAndHeight" ResizeMode="NoResize">
    <Grid>
        <Grid.ColumnDefinitions>
            <ColumnDefinition Width="Auto" />
            <ColumnDefinition Width="Auto" />
        </Grid.ColumnDefinitions>
        <Grid.RowDefinitions>
            <RowDefinition Height="Auto" />
            <RowDefinition Height="Auto" />
            <RowDefinition Height="Auto" />
        </Grid.RowDefinitions>

        <!-- Keyboard preview -->

        <c:Card x:Name="cKeyboardPreview" Grid.Row="0" Grid.Column="0" Margin="{StaticResource DialogItemsExceptBottomMargin}">
            <StackPanel Orientation="Vertical">
                <c:HeaderSplitter Header="{x:Static res:Strings.Label_MacroKeyboardLayout}" />
                <c:KeyboardEditorPreview x:Name="kpKeyboardPreview" ItemsSource="{Binding Path=CurrentScreen.Elements}" SelectedItem="{Binding Path=CurrentScreen.SelectedElement, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}" />
            </StackPanel>
        </c:Card>

        <!-- Available macro list -->

        <c:Card Grid.Row="0" Grid.Column="1" Margin="{StaticResource DialogItemsExceptBottomLeftMargin}" Width="250"
                MinHeight="200" VerticalAlignment="Stretch">
            <Grid VerticalAlignment="Stretch">
                <Grid.RowDefinitions>
                    <RowDefinition Height="Auto" />
                    <RowDefinition Height="1*" />
                </Grid.RowDefinitions>
                
                <c:HeaderSplitter Grid.Row="0" Header="{x:Static res:Strings.Label_AvailableMacros}" />

                <ScrollViewer Grid.Row="1" HorizontalAlignment="Stretch" VerticalAlignment="Stretch" 
                              MaxHeight="{Binding ElementName=kpKeyboardPreview, Path=ActualHeight}"
                              HorizontalScrollBarVisibility="Disabled" VerticalScrollBarVisibility="Auto">
                    <ItemsControl ItemsSource="{Binding AvailableMacroGroups}">
                        <ItemsControl.ItemTemplate>
                            <DataTemplate DataType="{x:Type dvm:MacroDirectoryGroupViewModel}">
                                <Expander Header="{Binding Display}" IsExpanded="{Binding IsExpanded, Mode=TwoWay}"
                                          Padding="{StaticResource DialogItemsVerticalOnlyMargin}">
                                    <ItemsControl ItemsSource="{Binding Entries}">
                                        <ItemsControl.ItemTemplate>
                                            <DataTemplate DataType="{x:Type dvm:MacroDirectoryEntryViewModel}">
                                                <c:AvailableMacro />
                                            </DataTemplate>
                                        </ItemsControl.ItemTemplate>
                                    </ItemsControl>
                                </Expander>
                            </DataTemplate>
                        </ItemsControl.ItemTemplate>
                    </ItemsControl>
                </ScrollViewer>
            </Grid>
        </c:Card>

        <!-- Configuration -->

        <c:Card Grid.Row="1" Grid.Column="0" Grid.ColumnSpan="2" Margin="{StaticResource DialogItemsMargin}">
            <StackPanel Orientation="Vertical">
                <c:HeaderSplitter Header="{x:Static res:Strings.Label_ConfigureElement}" />

                <Border>
                    <Border.Resources>

                        <!-- Pause -->

                        <DataTemplate DataType="{x:Type mevm:PauseMacroEditorViewModel}">
                            <cme:PauseMacroEditor />
                        </DataTemplate>

                        <!-- Multi macro editor -->

                        <DataTemplate DataType="{x:Type mevm:MultiMacroEditorViewModel}">
                            <cme:MultiMacroEditor />
                        </DataTemplate>

                        <!-- Folder macro editor -->

                        <DataTemplate DataType="{x:Type mevm:FolderMacroEditorViewModel}">
                            <cme:FolderMacroEditor />
                        </DataTemplate>

                        <!-- Back macro editor -->

                        <DataTemplate DataType="{x:Type mevm:BackMacroEditorViewModel}">
                            <cme:BackMacroEditor />
                        </DataTemplate>

                        <!-- Action macro editor -->

                        <DataTemplate DataType="{x:Type mevm:ActionMacroEditorViewModel}">
                            <cme:ActionMacroEditor />
                        </DataTemplate>

                        <!-- Sleep macro editor -->

                        <DataTemplate DataType="{x:Type mevm:SleepMacroEditorViewModel}">
                            <cme:SleepMacroEditor />
                        </DataTemplate>

                        <!-- Missing plugin macro editor -->

                        <DataTemplate DataType="{x:Type mevm:MissingPluginEditorViewModel}">
                            <cme:MissingPluginMacroEditor />
                        </DataTemplate>

                        <!-- Button and encoder editor -->

                        <DataTemplate DataType="{x:Type mkvm:BaseElementEditorViewModel}">
                            <StackPanel Orientation="Horizontal">
                                <Rectangle HorizontalAlignment="Stretch" VerticalAlignment="Stretch"
                                           Width="4" Fill="{x:Static SystemColors.HighlightBrush}" Margin="0,0,10,0"/>

                                <StackPanel Orientation="Vertical" Grid.IsSharedSizeScope="True">
                                    <c:HeaderSplitter Header="{Binding EditorHeader}" />

                                    <Grid Width="{StaticResource PropertiesGridWidth}" HorizontalAlignment="Left" Margin="{StaticResource DialogItemsMargin}">
                                        <Grid.ColumnDefinitions>
                                            <ColumnDefinition Width="Auto" SharedSizeGroup="Label" />
                                            <ColumnDefinition Width="1*" />
                                        </Grid.ColumnDefinitions>

                                        <Grid.RowDefinitions>
                                            <RowDefinition Height="Auto" />
                                            <RowDefinition Height="Auto" />
                                            <RowDefinition Height="Auto" />
                                            <RowDefinition Height="Auto" />
                                        </Grid.RowDefinitions>

                                        <Label Content="{x:Static res:Strings.Label_Description}" Grid.Row="0" Grid.Column="0" 
                                               HorizontalAlignment="Left" VerticalAlignment="Center"                               
                                               Margin="{StaticResource DialogItemsBottomRightMargin}"
                                               Visibility="{Binding Info.CanSetDescription, Converter={StaticResource BooleanToVisibilityConverter}}" />
                                        <TextBox Text="{Binding Content.Description, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}" Grid.Row="0" Grid.Column="1"
                                                 HorizontalAlignment="Stretch" VerticalAlignment="Center"
                                                 Margin="{StaticResource DialogItemsBottomMargin}" 
                                                 Visibility="{Binding Info.CanSetDescription, Converter={StaticResource BooleanToVisibilityConverter}}" />

                                        <Label Content="{x:Static res:Strings.Label_Color}" Grid.Row="1" Grid.Column="0"
                                               HorizontalAlignment="Left" VerticalAlignment="Center"
                                               Margin="{StaticResource DialogItemsBottomRightMargin}"
                                               Visibility="{Binding Info.CanSetColor, Converter={StaticResource BooleanToVisibilityConverter}}" />
                                        <TextBox Text="{Binding Content.Color, Mode=TwoWay, UpdateSourceTrigger=LostFocus, Converter={StaticResource ElementColorToStringConverter}}" Grid.Row="1" Grid.Column="1" 
                                                 HorizontalAlignment="Stretch" VerticalAlignment="Center"
                                                 Margin="{StaticResource DialogItemsBottomMargin}"
                                                 Visibility="{Binding Info.CanSetColor, Converter={StaticResource BooleanToVisibilityConverter}}" />

                                        <Label Content="{x:Static res:Strings.Label_Image}" Grid.Row="2" Grid.Column="0"
                                               HorizontalAlignment="Left" VerticalAlignment="Center"
                                               Margin="{StaticResource DialogItemsBottomRightMargin}"
                                               Visibility="{Binding Info.CanSetImage, Converter={StaticResource BooleanToVisibilityConverter}}" />
                                        <StackPanel Orientation="Horizontal" Grid.Row="2" Grid.Column="1"
                                                    Visibility="{Binding Info.CanSetImage, Converter={StaticResource BooleanToVisibilityConverter}}">
                                            <Button Width="24" Height="24" Command="{Binding ClearImageCommand}" Margin="{StaticResource DialogItemsBottomRightMargin}">
                                                <Image Width="16" Height="16" Source="\Resources\Images\Delete16.png" />
                                            </Button>
                                            <Button Width="24" Height="24" Command="{Binding ChooseImageCommand}" Margin="{StaticResource DialogItemsBottomMargin}">
                                                <Image Width="16" Height="16" Source="\Resources\Images\Open16.png" />
                                            </Button>
                                        </StackPanel>
                                    </Grid>

                                    <ListBox ItemsSource="{Binding Content.MacroSlots}" SelectedItem="{Binding Content.SelectedMacroSlot, Mode=TwoWay, UpdateSourceTrigger=PropertyChanged}"
                                             Padding="{StaticResource DialogItemsMargin}" Width="{StaticResource PropertiesGridWidth}">
                                        <ListBox.ItemContainerStyle>
                                            <Style TargetType="ListBoxItem">
                                                <Setter Property="HorizontalContentAlignment" Value="Stretch" />
                                            </Style>
                                        </ListBox.ItemContainerStyle>
                                        <ListBox.Template>
                                            <ControlTemplate TargetType="ListBox">
                                                <ItemsPresenter />
                                            </ControlTemplate>
                                        </ListBox.Template>
                                        <ListBox.ItemTemplate>
                                            <DataTemplate DataType="{x:Type mkvm:MacroEditorSlotViewModel}">
                                                <Grid HorizontalAlignment="Stretch">
                                                    <Grid.ColumnDefinitions>
                                                        <ColumnDefinition Width="Auto" SharedSizeGroup="Label" />
                                                        <ColumnDefinition Width="1*" />
                                                    </Grid.ColumnDefinitions>

                                                    <Label Margin="{StaticResource DialogItemsRightMargin}" Grid.Column="0" Content="{Binding Label}" 
                                                           VerticalAlignment="Center" />
                                                    <c:MacroContainer Grid.Column="1" Margin="{StaticResource DialogItemsVerticalOnlyMargin}" />
                                                </Grid>
                                            </DataTemplate>
                                        </ListBox.ItemTemplate>
                                    </ListBox>
                                </StackPanel>

                                <ContentControl Content="{Binding Content.SelectedMacroSlot.Editor}" />
                            </StackPanel>
                        </DataTemplate>

                    </Border.Resources>

                    <ContentControl Margin="10" Content="{Binding CurrentScreen.SelectedElement}" MinHeight="150" />
                </Border>

            </StackPanel>
        </c:Card>

        <!-- Buttons -->

        <StackPanel Orientation="Horizontal" Grid.Row="2" Grid.Column="0" Grid.ColumnSpan="2" HorizontalAlignment="Right">
            <Button Style="{StaticResource DialogButton}" Content="{x:Static cres:Strings.Ok}" Command="{Binding OkCommand}" />
            <Button Style="{StaticResource DialogButton}" Content="{x:Static cres:Strings.Cancel}" Command="{Binding CancelCommand}" />
        </StackPanel>

    </Grid>
</Window>
