﻿using Autofac;
using OpenMacroKeyboard.BusinessLogic.Models.AddMacroKeyboard;
using OpenMacroKeyboard.BusinessLogic.ViewModels.AddMacroKeyboard;
using OpenMacroKeyboard.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OpenMacroKeyboard.Windows
{
    /// <summary>
    /// Logika interakcji dla klasy AddKeyboardWindow.xaml
    /// </summary>
    public partial class AddMacroKeyboardWindow : Window, IAddMacroKeyboardWindowAccess, IResultDialog<AddMacroKeyboardResult>
    {
        private readonly AddMacroKeyboardViewModel viewModel;

        void IAddMacroKeyboardWindowAccess.Close(bool result)
        {
            DialogResult = result;
            this.Close();
        }

        public AddMacroKeyboardWindow()
        {
            InitializeComponent();

            viewModel = Dependencies.Container.Instance.Resolve<AddMacroKeyboardViewModel>(new NamedParameter("access", this));
            DataContext = viewModel;
        }

        public AddMacroKeyboardResult Result => viewModel.Result;
    }
}
