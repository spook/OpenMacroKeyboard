﻿using Autofac;
using OpenMacroKeyboard.BusinessLogic.Services.DriverRepository;
using OpenMacroKeyboard.BusinessLogic.ViewModels.MacroKeyboardPreview;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace OpenMacroKeyboard.Windows
{
    /// <summary>
    /// Logika interakcji dla klasy MacroKeyboardPreviewWindow.xaml
    /// </summary>
    public partial class MacroKeyboardPreviewWindow : Window, IMacroKeyboardPreviewWindowAccess
    {
        private readonly TimeSpan SHOW_TIME_MS = TimeSpan.FromSeconds(5);
        private float SCREEN_MARGIN = 16.0f;

        private readonly MacroKeyboardPreviewViewModel viewModel;
        private readonly DispatcherTimer hideTimer;

        private void PositionWindow()
        {
            var mainScreen = System.Windows.Forms.Screen.PrimaryScreen!;
            var dpi = VisualTreeHelper.GetDpi(this);
            this.Left = mainScreen.WorkingArea.Right / dpi.DpiScaleX - ActualWidth - SCREEN_MARGIN;
            this.Top = mainScreen.WorkingArea.Bottom / dpi.DpiScaleY - ActualHeight - SCREEN_MARGIN;
        }

        private void HandleTimerTick(object? sender, EventArgs e)
        {
            hideTimer.Stop();
            Hide();
        }

        void IMacroKeyboardPreviewWindowAccess.NotifyPageChanged()
        {
            if (IsVisible)
            {
                hideTimer.Stop();
                hideTimer.Interval = SHOW_TIME_MS;
                hideTimer.Start();
            }
        }

        public MacroKeyboardPreviewWindow()
        {
            InitializeComponent();

            viewModel = Dependencies.Container.Instance.Resolve<MacroKeyboardPreviewViewModel>(new NamedParameter("access", this))!;
            DataContext = viewModel;
            hideTimer = new DispatcherTimer();
            hideTimer.Tick += HandleTimerTick;
        }

        public void SetMacroKeyboard(IMacroKeyboard macroKeyboard)
        {
            viewModel.SetMacroKeyboard(macroKeyboard);
        }

        public void ShowTimed()
        {
            Show();
            PositionWindow();

            hideTimer.Interval = SHOW_TIME_MS;
            hideTimer.Start();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            PositionWindow();
        }
    }
}
