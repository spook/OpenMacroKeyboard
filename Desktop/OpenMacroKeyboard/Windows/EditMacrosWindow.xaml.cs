﻿using Autofac;
using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroDirectory;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroKeyboard;
using OpenMacroKeyboard.Controls;
using OpenMacroKeyboard.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OpenMacroKeyboard.Windows
{
    /// <summary>
    /// Logika interakcji dla klasy EditMacrosWindow.xaml
    /// </summary>
    public partial class EditMacrosWindow : Window, IEditMacrosWindowAccess, IResultDialog<Screen?>
    {
        private readonly EditMacrosWindowViewModel viewModel;

        void IEditMacrosWindowAccess.Close(bool result)
        {
            DialogResult = result;
            Close();
        }

        public EditMacrosWindow(DriverMetadata driverMetadata, Screen? currentRootScreen)
        {
            InitializeComponent();

            viewModel = Dependencies.Container.Instance.Resolve<EditMacrosWindowViewModel>(new NamedParameter("access", this),
                new NamedParameter("driverMetadata", driverMetadata),
                new NamedParameter("currentRootScreen", currentRootScreen!));
            DataContext = viewModel;            
        }

        public Screen? Result => viewModel.Result;
    }
}
