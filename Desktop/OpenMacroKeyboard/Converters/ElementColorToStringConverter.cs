﻿using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Data;

namespace OpenMacroKeyboard.Converters
{
    public class ElementColorToStringConverter : IValueConverter
    {
        private static readonly Regex colorRegex = new("^#?([0-9a-fA-F]{2})([0-9a-fA-F]{2})([0-9a-fA-F]{2})$");

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is ElementColor color)
            {
                return $"#{color.R:X2}{color.G:X2}{color.B:X2}";
            }

            return Binding.DoNothing;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string str)
            {
                var match = colorRegex.Match(str);
                if (match.Success)
                {
                    byte r = System.Convert.ToByte(match.Groups[1].Value, 16);
                    byte g = System.Convert.ToByte(match.Groups[2].Value, 16);
                    byte b = System.Convert.ToByte(match.Groups[3].Value, 16);

                    return new ElementColor(r, g, b);
                }
                else
                {
                    return ElementColor.Zero;
                }
            }

            return Binding.DoNothing;
        }
    }
}
