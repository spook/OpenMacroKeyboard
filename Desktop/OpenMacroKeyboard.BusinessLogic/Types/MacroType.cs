﻿using OpenMacroKeyboard.BusinessLogic.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Types
{
    public enum MacroType
    {
        [DisplayResourceKey("Macro.Action", "")]
        [DoNotList]
        Action = 1,
        [DisplayResourceKey("Macro.Back", "")]
        [DoNotList]
        Back = 2,
        [DisplayResourceKey("Macro.Folder", "Group.System")]
        Folder = 3,
        [DisplayResourceKey("Macro.Multi", "Group.System")]
        Multi = 4,
        [DisplayResourceKey("Macro.Pause", "Group.System")]
        Pause = 5,
        [DisplayResourceKey("Macro.Sleep", "Group.System")]
        Sleep = 6
    }
}
