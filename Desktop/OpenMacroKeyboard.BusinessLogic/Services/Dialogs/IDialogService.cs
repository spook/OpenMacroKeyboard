﻿using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.BusinessLogic.Models.AddMacroKeyboard;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens;
using OpenMacroKeyboard.BusinessLogic.Services.DriverRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Services.Dialogs
{
    public interface IDialogService
    {
        (bool result, AddMacroKeyboardResult? model) ShowAddKeyboardDialog();
        void ShowExceptionDialog(Exception e);
        (bool result, Screen? edited) ShowMacroEditorDialog(DriverMetadata driverMetadata, Screen? current);
        void ShowMacroKeyboardPreview(IMacroKeyboard macroKeyboard);
        void ShowMainWindow();
        (bool result, string? path) ShowOpenDialog(string? filter = null, string? title = null, string? filename = null);
        (bool result, string? path) ShowSaveDialog(string? filter = null, string? title = null, string? filename = null);
        void PreloadMainWindows();
        (bool result, IReadOnlyDictionary<string, object>? newSettings) ShowReconnectDialog(DriverMetadata driverMetadata, IReadOnlyDictionary<string, object> currentSettings);
    }
}
