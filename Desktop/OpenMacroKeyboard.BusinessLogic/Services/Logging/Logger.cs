﻿using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Services.Logging
{
    internal class Logger : ILogger, OpenMacroKeyboard.API.V1.ILog, IDisposable
    {
        private class DebugOutputSink : Serilog.Core.ILogEventSink
        {
            public void Emit(LogEvent logEvent)
            {
                System.Diagnostics.Debug.WriteLine($"[{logEvent.Timestamp}] {logEvent.MessageTemplate}");
            }
        }

        private Serilog.Core.Logger logger;

        public Logger()
        {
            logger = new Serilog
                .LoggerConfiguration()                
                .CreateLogger();            
        }

        public void Dispose()
        {
            logger.Dispose();
        }

        public void Trace(string source, string message) => logger.Verbose(message, source);

        public void Debug(string source, string message) => logger.Debug(message, source);

        public void Information(string source, string message) => logger.Information(message, source);

        public void Warning(string source, string message) => logger.Warning(message, source);

        public void Error(string source, string message) => logger.Error(message, source);

        public void Fatal(string source, string message) => logger.Fatal(message, source);
    }
}
