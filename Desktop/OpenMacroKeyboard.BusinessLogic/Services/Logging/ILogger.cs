﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Services.Logging
{
    public interface ILogger
    {
        void Debug(string source, string message);
        void Error(string source, string message);
        void Fatal(string source, string message);
        void Information(string source, string message);
        void Trace(string source, string message);
        void Warning(string source, string message);
    }
}
