﻿using OpenMacroKeyboard.BusinessLogic.Models.Configuration.MacroKeyboard;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens;
using OpenMacroKeyboard.BusinessLogic.Services.DriverRepository;
using System;
using System.Collections.Generic;

namespace OpenMacroKeyboard.BusinessLogic.Services.MacroKeyboardManager
{
    public interface IMacroKeyboardManagerService
    {
        IMacroKeyboard AddKeyboard(Guid driverGuid, Dictionary<string, object> settings);
        IMacroKeyboard ImportKeyboard(KeyboardInstance keyboard);
        void DeleteKeyboard(IMacroKeyboard macroKeyboard);
        void UpdateMacroLayout(IMacroKeyboard macroKeyboard, Screen newRootScreen);
        void ReconnectMacroKeyboard(IMacroKeyboard macroKeyboard, IReadOnlyDictionary<string, object>? newSettings);

        IReadOnlyList<IMacroKeyboard> MacroKeyboards { get; }
    }
}