﻿using OpenMacroKeyboard.BusinessLogic.Models.Configuration.MacroKeyboard;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration;
using OpenMacroKeyboard.BusinessLogic.Services.ActionRepository;
using OpenMacroKeyboard.BusinessLogic.Services.Configuration;
using OpenMacroKeyboard.BusinessLogic.Services.DriverRepository;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Services.MacroKeyboardManager
{
    public class MacroKeyboardManagerService : IMacroKeyboardManagerService, IDisposable
    {
        private readonly IDriverRepositoryService driverRepository;
        private readonly IConfigurationService configurationService;
        private readonly ObservableCollection<IMacroKeyboard> macroKeyboards = new();

        private void SaveConfiguration()
        {
            var keyboardInstances = macroKeyboards
                .Select(keyboard => new KeyboardInstance(keyboard.DriverMetadata.Guid, new(keyboard.Settings), keyboard.RootScreen ?? Screen.Empty(keyboard.DriverMetadata)))
                .ToList();

            var configuration = new ConfigurationModel(keyboardInstances);

            configurationService.Save(configuration);
        }

        private void LoadConfiguration()
        {
            macroKeyboards.Clear();

            ConfigurationModel? configuration = configurationService.Load();
            if (configuration != null)
            {
                foreach (var keyboard in configuration.Keyboards)
                {
                    if (driverRepository.DriverFactoryGuids.Contains(keyboard.DriverGuid))
                    {
                        IMacroKeyboard macroKeyboard = driverRepository.CreateMacroKeyboard(keyboard.DriverGuid, keyboard.Settings);
                        macroKeyboards.Add(macroKeyboard);

                        macroKeyboard.RootScreen = keyboard.Screen;
                    }
                }
            }
        }

        public MacroKeyboardManagerService(IDriverRepositoryService driverRepositoryService, 
            IConfigurationService configurationService)
        {
            this.driverRepository = driverRepositoryService;
            this.configurationService = configurationService;

            LoadConfiguration();
        }

        public IMacroKeyboard AddKeyboard(Guid driverGuid, Dictionary<string, object> settings)
        {
            var macroKeyboard = driverRepository.CreateMacroKeyboard(driverGuid, settings);
            macroKeyboards.Add(macroKeyboard);
            SaveConfiguration();

            return macroKeyboard;
        }

        public IMacroKeyboard ImportKeyboard(KeyboardInstance keyboard)
        {
            var macroKeyboard = driverRepository.CreateMacroKeyboard(keyboard.DriverGuid, keyboard.Settings);
            macroKeyboard.RootScreen = keyboard.Screen;
            macroKeyboards.Add(macroKeyboard);
            SaveConfiguration();

            return macroKeyboard;
        }


        public void UpdateMacroLayout(IMacroKeyboard macroKeyboard, Screen newRootScreen)
        {
            macroKeyboard.RootScreen = newRootScreen;
            SaveConfiguration();
        }

        public void ReconnectMacroKeyboard(IMacroKeyboard macroKeyboard, IReadOnlyDictionary<string, object>? newSettings)
        {
            macroKeyboard.Reconnect(newSettings);
            macroKeyboard.RefreshVisuals();
            SaveConfiguration();
        }

        public void DeleteKeyboard(IMacroKeyboard macroKeyboard)
        {
            int index = macroKeyboards.IndexOf(macroKeyboard);
            if (index >= 0 && index < macroKeyboards.Count)
            {
                macroKeyboards.RemoveAt(index);
                macroKeyboard.Dispose();
            }

            SaveConfiguration();
        }

        public void Dispose()
        {
            foreach (var keyboard in macroKeyboards)
            {
                keyboard.Dispose();
            }

            macroKeyboards.Clear();
        }

        public IReadOnlyList<IMacroKeyboard> MacroKeyboards => macroKeyboards;
    }
}
