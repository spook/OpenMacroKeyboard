﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Services.Paths
{
    internal class PathService : IPathService
    {
        private const string PUBLISHER = "Spooksoft";
        private const string APPNAME = "OpenMacroKeyboard";
        private const string CONFIGURATION_FILENAME = "config.dat";

        private readonly string appDataPath;

        public PathService()
        {
            appDataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), PUBLISHER, APPNAME);
            Directory.CreateDirectory(appDataPath);
        }

        public string AppDataPath => appDataPath;

        public string ConfigurationFilePath => Path.Combine(appDataPath, CONFIGURATION_FILENAME);
    }
}
