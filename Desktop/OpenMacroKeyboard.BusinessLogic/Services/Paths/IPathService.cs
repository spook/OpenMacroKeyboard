﻿namespace OpenMacroKeyboard.BusinessLogic.Services.Paths
{
    public interface IPathService
    {
        string AppDataPath { get; }
        string ConfigurationFilePath { get; }
    }
}