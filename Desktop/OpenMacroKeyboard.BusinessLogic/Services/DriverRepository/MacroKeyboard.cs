﻿using OpenMacroKeyboard.API.V1.Drivers;
using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.API.V1.Drivers.Types;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens.Macros;
using OpenMacroKeyboard.BusinessLogic.Services.ActionRepository;
using OpenMacroKeyboard.BusinessLogic.Services.Dialogs;
using OpenMacroKeyboard.BusinessLogic.Services.DriverRepository.MacroInfos;
using OpenMacroKeyboard.BusinessLogic.Services.DriverRepository.Screens;
using OpenMacroKeyboard.BusinessLogic.Services.UIThreadAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Services.DriverRepository
{
    internal class MacroKeyboard : IMacroKeyboard, IDriverHandler, IDisposable
    {
        // Private constants --------------------------------------------------

        private const uint LONG_PRESS_THRESHOLD_MS = 300;

        // Private fields -----------------------------------------------------

        private readonly IActionRepositoryService actionRepository;
        private readonly IDialogService dialogService;
        private readonly BaseDriverFactory driverFactory;
        private readonly IUIThreadAccess uIThreadAccess;
        private IReadOnlyDictionary<string, object> settings;

        private BaseDriver? driver;
        private Screen? rootScreen;
        private readonly Stack<Screen> subScreens = new();

        private Screen? activeScreen;
        private ScreenInfo? activeScreenInfo;
        private uint[] buttonDownTimes;

        // Private methods ----------------------------------------------------

        private void UpdateKeyboardVisuals()
        {
            if (activeScreenInfo != null)
            {
                driver?.UpdateVisuals(activeScreenInfo.Buttons
                    .Select(b => b.ToVisuals())
                    .ToList(),
                    activeScreenInfo.Encoders
                    .Select(e => e.ToVisuals())
                    .ToList());
            }
            else
            {
                driver?.UpdateVisuals(driverFactory.Metadata.ButtonDefinitions
                    .Select(b => new KeyboardElementVisuals(null, null, null, null))
                    .ToList(),
                    driverFactory.Metadata.EncoderDefinitions
                    .Select(e => new KeyboardElementVisuals(null, null, null, null))
                    .ToList());
            }
        }

        private void SetActiveScreen(Screen? newScreen)
        {
            activeScreen = newScreen;

            activeScreenInfo?.Dispose();
            activeScreenInfo = null;

            if (activeScreen != null)
                activeScreenInfo = ScreenInfo.FromScreen(activeScreen, driverFactory.Metadata, actionRepository);
            
            UpdateKeyboardVisuals();

            ActiveScreenChanged?.Invoke(this, EventArgs.Empty);
        }

        private void ExecuteMacro(BaseMacroInfo macro)
        {
            switch (macro)
            {
                case ActionMacroInfo action:
                    {
                        action.Action.Execute();
                        break;
                    }
                case BackMacroInfo back:
                    {
                        if (subScreens.Count > 0)
                        {
                            subScreens.Pop();

                            SetActiveScreen(subScreens.Count > 0 ? subScreens.Peek() : rootScreen);
                        }
                        break;
                    }
                case FolderMacroInfo folder:
                    {
                        subScreens.Push(folder.Screen);

                        SetActiveScreen(subScreens.Peek());

                        break;
                    }
                case MultiMacroInfo multi:
                    {
                        for (int i = 0; i < multi.SubMacros.Count; i++)
                        {
                            var subMacro = multi.SubMacros[i];
                            if (subMacro != null)
                                ExecuteMacro(subMacro);
                        }

                        break;
                    }
                case PauseMacroInfo pause:
                    {
                        Thread.Sleep(pause.DelayMs);

                        break;
                    }
                case SleepMacroInfo sleep:
                    {
                        driver?.Sleep();
                        break;
                    }
                default:
                    throw new InvalidOperationException("Unsupported macro type!");
            }
        }

        private void BuildCustomCacheRecursive(Screen screen)
        {
            if (driver == null)
                throw new InvalidOperationException("Driver cannot be null at this point!");

            var screenInfo = ScreenInfo.FromScreen(screen, DriverMetadata, actionRepository);

            for (int i = 0; i < screen.Buttons.Count; i++)
            {
                var button = screen.Buttons[i];
                var buttonInfo = screenInfo.Buttons[i];
                var buttonDefinition = DriverMetadata.ButtonDefinitions[i];

                button.CustomCache = driver.BuildButtonCustomCache(buttonDefinition, buttonInfo.ToVisuals());

                if (button.Macro is FolderMacro folderMacro)
                    BuildCustomCacheRecursive(folderMacro.Screen);
            }

            for (int i = 0; i < screen.Encoders.Count; i++)
            {
                var encoder = screen.Encoders[i];
                var encoderInfo = screenInfo.Encoders[i];
                var encoderDefinition = DriverMetadata.EncoderDefinitions[i];

                encoder.CustomCache = driver.BuildEncoderCustomCache(encoderDefinition, encoderInfo.ToVisuals());

                if (encoder.PressMacro is FolderMacro folderMacro)
                    BuildCustomCacheRecursive(folderMacro.Screen);

                if (encoder.IncMacro is FolderMacro incFolderMacro)
                    BuildCustomCacheRecursive(incFolderMacro.Screen);

                if (encoder.DecMacro is FolderMacro decFolderMacro)
                    BuildCustomCacheRecursive(decFolderMacro.Screen);
            }
        }

        private void BuildCustomCache()
        {
            if (rootScreen != null)
                BuildCustomCacheRecursive(rootScreen);
        }

        [DllImport("kernel32.dll")]
        public static extern uint GetTickCount();

        // IDriverHandler implementation --------------------------------------

        void IDriverHandler.HandleEvent(MacroKeyboardEventSource source, int index, MacroKeyboardEventType type)
        {
            uIThreadAccess.RunOnUIThread(() => 
            { 
                if (source == MacroKeyboardEventSource.Button && type == MacroKeyboardEventType.Down && index >= 0 && index < buttonDownTimes.Length)
                {
                    buttonDownTimes[index] = GetTickCount();
                }
                else if (source == MacroKeyboardEventSource.Button && type == MacroKeyboardEventType.Up && index >= 0 && index < buttonDownTimes.Length)
                {
                    long now = GetTickCount();

                    long diff;
                    if (now > buttonDownTimes[index])
                        diff = now - buttonDownTimes[index];
                    else
                        diff = uint.MaxValue - buttonDownTimes[index] + now;

                    if (diff > LONG_PRESS_THRESHOLD_MS)
                    {
                        dialogService.ShowMacroKeyboardPreview(this);

                        return;
                    }
                }

                if (activeScreenInfo != null)
                {
                    switch (source)
                    {
                        case MacroKeyboardEventSource.Button:
                            {
                                if (index >= 0 && 
                                    index < activeScreenInfo.Buttons.Count &&
                                    activeScreenInfo.Buttons[index].ActionMap.TryGetValue(type, out BaseMacroInfo? macro) &&
                                    macro != null)
                                    ExecuteMacro(macro);

                                break;
                            }
                        case MacroKeyboardEventSource.Encoder:
                            {
                                if (index >= 0 &&
                                    index < activeScreenInfo.Encoders.Count &&
                                    activeScreenInfo.Encoders[index].ActionMap.TryGetValue(type, out BaseMacroInfo? macro) &&
                                    macro != null)
                                    ExecuteMacro(macro);

                                break;
                            }
                        default:
                            throw new InvalidOperationException("Unsupported macro keyboard event source!");
                    }
                }
            });
        }

        void IDriverHandler.NotifyAwake()
        {
            // Send visuals, so that keyboard can wake up from the sleep
            UpdateKeyboardVisuals();
        }

        // Public methods -----------------------------------------------------

        public MacroKeyboard(IActionRepositoryService actionRepository, 
            IDialogService dialogService,
            BaseDriverFactory driverFactory, 
            IUIThreadAccess uIThreadAccess,
            IReadOnlyDictionary<string, object> settings)
        {
            this.actionRepository = actionRepository;
            this.dialogService = dialogService;
            this.driverFactory = driverFactory;
            this.uIThreadAccess = uIThreadAccess;
            this.settings = new Dictionary<string, object>(settings);

            buttonDownTimes = new uint[driverFactory.Metadata.ButtonDefinitions.Count];

            driver = driverFactory.CreateDriver(new(this.settings), this);
            driver.Init();

            BuildCustomCache();
        }

        public void Dispose()
        {
            Disposing?.Invoke(this, EventArgs.Empty);

            activeScreenInfo?.Dispose();
            activeScreenInfo = null;

            driver?.Dispose();
            driver = null;
        }

        public void Reconnect(IReadOnlyDictionary<string, object>? newSettings = null)
        {
            if (driver != null)
            {
                driver.Dispose();
                driver = null;
            }

            if (newSettings != null)
                settings = new Dictionary<string, object>(newSettings);

            driver = driverFactory.CreateDriver(new(settings), this);
            driver.Init();

            BuildCustomCache();

            SetActiveScreen(rootScreen);
        }

        public void SetRootScreen(Screen? rootScreen)
        {
            this.rootScreen = rootScreen;
            this.subScreens.Clear();
            BuildCustomCache();

            SetActiveScreen(rootScreen);
        }

        public void RefreshVisuals()
        {
            UpdateKeyboardVisuals();
        }

        // Public properties --------------------------------------------------

        public string? Display => driver?.Display;

        public Screen? ActiveScreen => activeScreen;

        public Screen? RootScreen
        {
            get => rootScreen;
            set => SetRootScreen(value);
        }

        public IReadOnlyDictionary<string, object> Settings => settings;

        public DriverMetadata DriverMetadata => driverFactory.Metadata;

        public event EventHandler? Disposing;

        public event EventHandler? ActiveScreenChanged;
    }
}
