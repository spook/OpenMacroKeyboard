﻿using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens;
using System;
using System.Collections.Generic;

namespace OpenMacroKeyboard.BusinessLogic.Services.DriverRepository
{
    public interface IMacroKeyboard : IDisposable
    {
        void Reconnect(IReadOnlyDictionary<string, object>? newSettings = null);
        void RefreshVisuals();

        string? Display { get; }
        Screen? ActiveScreen { get; }
        Screen? RootScreen { get; set; }
        IReadOnlyDictionary<string, object> Settings { get; }
        DriverMetadata DriverMetadata { get; }

        event EventHandler? Disposing;

        event EventHandler? ActiveScreenChanged;
    }
}