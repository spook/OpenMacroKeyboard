﻿using OpenMacroKeyboard.API.V1.Actions.Models;
using OpenMacroKeyboard.API.V1.Actions;
using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.API.V1.Drivers;
using OpenMacroKeyboard.BusinessLogic.Services.ActionRepository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using OpenMacroKeyboard.BusinessLogic.Services.Logging;
using OpenMacroKeyboard.BusinessLogic.Services.Dialogs;
using OpenMacroKeyboard.BusinessLogic.Services.UIThreadAccess;

namespace OpenMacroKeyboard.BusinessLogic.Services.DriverRepository
{
    internal class DriverRepositoryService : IDriverRepositoryService
    {
        private readonly ILogger logger;
        private readonly IActionRepositoryService actionRepository;
        private readonly IDialogService dialogService;
        private readonly IUIThreadAccess uiThreadAccess;

        private readonly Dictionary<Guid, BaseDriverFactory> driverFactories = new();

        public DriverRepositoryService(ILogger logger, 
            IActionRepositoryService actionRepository, 
            IDialogService dialogService,
            IUIThreadAccess uiThreadAccess)
        {
            this.logger = logger;
            this.actionRepository = actionRepository;
            this.dialogService = dialogService;
            this.uiThreadAccess = uiThreadAccess;

            // Loading drivers

            var driversPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)!, "Drivers");

            if (Directory.Exists(driversPath))
            {
                foreach (var driver in Directory.EnumerateFiles(driversPath, "*.dll", SearchOption.AllDirectories))
                {
                    try
                    {
                        Assembly driverAssembly = Assembly.LoadFrom(driver);

                        foreach (var type in driverAssembly.ExportedTypes)
                        {
                            if (type.IsAssignableTo(typeof(BaseDriverFactory)) && !type.IsAbstract)
                            {
                                try
                                {
                                    var factory = (BaseDriverFactory)Activator.CreateInstance(type, logger)!;

                                    // Sanity checks

                                    if (driverFactories.ContainsKey(factory.Metadata.Guid))
                                    {
                                        logger.Debug("OpenMacroKeyboard", $"Cannot add driver factory {type.Name} from {Path.GetFileName(driver)}, because another factory with same GUID is already registered.");
                                        factory.Dispose();
                                        continue;
                                    }

                                    if (factory.Metadata.ButtonDefinitions.Count > 100)
                                    {
                                        logger.Debug("OpenMacroKeyboard", $"Cannot add driver factory {type.Name} from {Path.GetFileName(driver)}. Factory registers more than 100 buttons.");
                                        factory.Dispose();
                                        continue;
                                    }

                                    if (factory.Metadata.EncoderDefinitions.Count > 100)
                                    {
                                        logger.Debug("OpenMacroKeyboard", $"Cannot add driver factory {type.Name} from {Path.GetFileName(driver)}. Factory registers more than 100 encoders.");
                                        factory.Dispose();
                                        continue;
                                    }

                                    driverFactories.Add(factory.Metadata.Guid, factory);
                                }
                                catch
                                {
                                    // Cannot construct this library, skip
                                    logger.Debug("OpenMacroKeyboard", $"Failed to instantiate driver factory {type.Name}, skipping");
                                    continue;
                                }
                            }
                        }
                    }
                    catch
                    {
                        // Most likely this is not a .NET assembly, skip
                        logger.Debug("OpenMacroKeyboard", $"Failed to load library {Path.GetFileName(driver)}, skipping");
                        continue;
                    }
                }
            }
        }

        public void Dispose()
        {
            foreach (var kvp in driverFactories)
            {
                kvp.Value.Dispose();
            }
            driverFactories.Clear();
        }

        public DriverMetadata GetDriverFactoryMetadata(Guid guid)
        {
            return driverFactories[guid].Metadata;
        }

        public IMacroKeyboard CreateMacroKeyboard(Guid libraryGuid, Dictionary<string, object> settings)
        {
            if (!driverFactories.TryGetValue(libraryGuid, out var driverFactory))
                throw new ArgumentOutOfRangeException(nameof(libraryGuid));

            var result = new MacroKeyboard(actionRepository, dialogService, driverFactory, uiThreadAccess, settings);
            return result;
        }

        public IEnumerable<Guid> DriverFactoryGuids => driverFactories.Keys;
    }
}
