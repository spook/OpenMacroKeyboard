﻿using OpenMacroKeyboard.API.V1.Drivers.Models;
using System;
using System.Collections.Generic;

namespace OpenMacroKeyboard.BusinessLogic.Services.DriverRepository
{
    public interface IDriverRepositoryService
    {
        IEnumerable<Guid> DriverFactoryGuids { get; }
        DriverMetadata GetDriverFactoryMetadata(Guid guid);
        IMacroKeyboard CreateMacroKeyboard(Guid libraryGuid, Dictionary<string, object> settings);
    }
}