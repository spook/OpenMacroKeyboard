﻿using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.API.V1.Drivers.Types;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens;
using OpenMacroKeyboard.BusinessLogic.Services.ActionRepository;
using OpenMacroKeyboard.BusinessLogic.Services.DriverRepository.MacroInfos;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Services.DriverRepository.Elements
{
    public class ElementInfo : IDisposable
    {
        private static RawImage? ElementImageToRawImage(ElementImage image)
        {
            return new RawImage(image.Argb, image.Width, image.Height);
        }

        public ElementInfo(string? description,
            bool showDescription,
            ElementColor color,
            bool showColor,
            RawImage? rawImage,
            bool showImage,
            object? customCache,
            Dictionary<MacroKeyboardEventType, BaseMacroInfo?> actionMap)
        {
            Description = description;
            ShowDescription = showDescription;
            Color = color;
            ShowColor = showColor;
            RawImage = rawImage;
            ShowImage = showImage;
            ActionMap = actionMap;
            CustomCache = customCache;
        }

        public static ElementInfo FromButton(ButtonEntry? b, BaseKeyboardElementInfo info, IActionRepositoryService actionRepository)
        {
            Dictionary<MacroKeyboardEventType, BaseMacroInfo?> actionMap = new();
            actionMap[MacroKeyboardEventType.Up] = BaseMacroInfo.FromMacro(b?.Macro, actionRepository);

            return new ElementInfo(b?.Description ?? string.Empty,
                info.CanSetDescription,
                b?.Color ?? ElementColor.Zero,
                info.CanSetColor,
                (b != null && b.Image != null) ? ElementImageToRawImage(b.Image) : null,
                info.CanSetImage,
                b?.CustomCache,
                actionMap);
        }

        public static ElementInfo FromEncoder(EncoderEntry? e, BaseKeyboardElementInfo info, IActionRepositoryService actionRepository)
        {
            Dictionary<MacroKeyboardEventType, BaseMacroInfo?> actionMap = new();
            actionMap[MacroKeyboardEventType.Increase] = BaseMacroInfo.FromMacro(e?.IncMacro, actionRepository);
            actionMap[MacroKeyboardEventType.Decrease] = BaseMacroInfo.FromMacro(e?.DecMacro, actionRepository);
            actionMap[MacroKeyboardEventType.Up] = BaseMacroInfo.FromMacro(e?.PressMacro, actionRepository);

            return new ElementInfo(e?.Description ?? string.Empty,
                info.CanSetDescription,
                e?.Color ?? ElementColor.Zero,
                info.CanSetColor,
                (e != null && e.Image != null) ? ElementImageToRawImage(e.Image) : null,
                info.CanSetImage, 
                e?.CustomCache,
                actionMap);
        }

        public void Dispose()
        {
            foreach (var kvp in ActionMap)
            {
                kvp.Value?.Dispose();
            }
        }

        public KeyboardElementVisuals ToVisuals()
        {
            return new KeyboardElementVisuals(ShowDescription ? Description : null,
                ShowColor ? new KeyboardElementColor(Color.R, Color.G, Color.B) : null,
                ShowImage ? RawImage : null,
                CustomCache);
        }
        public Dictionary<MacroKeyboardEventType, BaseMacroInfo?> ActionMap { get; }
        public ElementColor Color { get; }
        public string? Description { get; }        
        public RawImage? RawImage { get; }
        public bool ShowColor { get; }
        public bool ShowDescription { get; }
        public bool ShowImage { get; } 
        public object? CustomCache { get; }
    }
}
