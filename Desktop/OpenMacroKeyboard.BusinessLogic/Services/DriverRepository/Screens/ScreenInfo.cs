﻿using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens;
using OpenMacroKeyboard.BusinessLogic.Services.ActionRepository;
using OpenMacroKeyboard.BusinessLogic.Services.DriverRepository.Elements;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Services.DriverRepository.Screens
{
    public class ScreenInfo : IDisposable
    {
        public ScreenInfo(List<ElementInfo> buttons, List<ElementInfo> encoders)
        {
            Buttons = buttons;
            Encoders = encoders;
        }

        public void Dispose()
        {
            foreach (var element in Buttons)
                element.Dispose();
            foreach (var element in Encoders)
                element.Dispose();
        }

        public static ScreenInfo FromScreen(Screen screen, DriverMetadata driverMetadata, IActionRepositoryService actionRepository)
        {
            List<ElementInfo> buttons = new();

            for (int i = 0; i < driverMetadata.ButtonDefinitions.Count; i++)
            {
                buttons.Add(ElementInfo.FromButton(i < screen.Buttons.Count ? screen.Buttons[i] : null, driverMetadata.ButtonDefinitions[i], actionRepository));
            }

            List<ElementInfo> encoders = new();

            for (int i = 0; i < driverMetadata.EncoderDefinitions.Count; i++)
            {
                encoders.Add(ElementInfo.FromEncoder(i < screen.Encoders.Count ? screen.Encoders[i] : null, driverMetadata.EncoderDefinitions[i], actionRepository));
            }

            return new ScreenInfo(buttons, encoders);
        }

        public List<ElementInfo> Buttons { get; } 
        public List<ElementInfo> Encoders { get; }
    }
}
