﻿using OpenMacroKeyboard.BusinessLogic.Services.ActionRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Services.DriverRepository.MacroInfos
{
    public class ActionMacroInfo : BaseMacroInfo
    {
        public ActionMacroInfo(IMacroAction action)
        {
            Action = action;
        }

        public override void Dispose()
        {
            Action.Dispose();
        }

        public IMacroAction Action { get; } 
    }
}
