﻿using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Services.DriverRepository.MacroInfos
{
    public class FolderMacroInfo : BaseMacroInfo
    {        
        public FolderMacroInfo(Screen screen)
        {
            Screen = screen;
        }

        public Screen Screen { get; }
    }
}
