﻿using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens.Macros;
using OpenMacroKeyboard.BusinessLogic.Services.ActionRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Services.DriverRepository.MacroInfos
{
    public abstract class BaseMacroInfo : IDisposable
    {
        public virtual void Dispose()
        {

        }

        public static BaseMacroInfo? FromMacro(BaseMacro? macro, IActionRepositoryService actionRepository)
        {
            if (macro == null)
                return null;

            switch (macro)
            {
                case BackMacro back:
                    return new BackMacroInfo();
                case PauseMacro pause:
                    return new PauseMacroInfo(pause.DelayMs);
                case FolderMacro folder:
                    return new FolderMacroInfo(folder.Screen);
                case MultiMacro multi:
                    return new MultiMacroInfo(multi.SubMacros.Select(sm => BaseMacroInfo.FromMacro(sm, actionRepository)).ToList());
                case ActionMacro action:
                    {
                        try
                        {
                            if (!actionRepository.ActionFactoryGuids.Contains(action.ActionGuid))
                                return null;

                            var macroAction = actionRepository.CreateAction(action.ActionGuid, action.Settings);

                            return new ActionMacroInfo(macroAction);
                        }
                        catch
                        {
                            return null;
                        }
                    }
                case SleepMacro sleep:
                    {
                        return new SleepMacroInfo();
                    }
                default:
                    throw new InvalidOperationException("Unsupported macro type!");
            }
        }
    }
}
