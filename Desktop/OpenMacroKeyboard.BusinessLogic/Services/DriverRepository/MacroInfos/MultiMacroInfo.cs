﻿using System.Collections.Generic;

namespace OpenMacroKeyboard.BusinessLogic.Services.DriverRepository.MacroInfos
{
    public class MultiMacroInfo : BaseMacroInfo
    {
        public MultiMacroInfo(List<BaseMacroInfo?> subMacros)
        {
            SubMacros = subMacros;
        }

        public List<BaseMacroInfo?> SubMacros { get; } 
    }
}