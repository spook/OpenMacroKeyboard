﻿namespace OpenMacroKeyboard.BusinessLogic.Services.DriverRepository.MacroInfos
{
    public class PauseMacroInfo : BaseMacroInfo
    {
        public PauseMacroInfo(int delayMs)
        {
            DelayMs = delayMs;
        }

        public int DelayMs { get; } 
    }
}