﻿using OpenMacroKeyboard.BusinessLogic.Models.Configuration;
using OpenMacroKeyboard.BusinessLogic.Services.Paths;
using Spooksoft.Xml.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Services.Configuration
{
    internal class ConfigurationService : IConfigurationService
    {
        private readonly IPathService pathService;

        public ConfigurationService(IPathService pathService)
        {
            this.pathService = pathService;
        }

        public ConfigurationModel? Load()
        {
            if (!File.Exists(pathService.ConfigurationFilePath))
                return null;

            try
            {
                using var fs = new FileStream(pathService.ConfigurationFilePath, FileMode.Open, FileAccess.Read);
                XmlSerializer serializer = new XmlSerializer();
                ConfigurationModel result = serializer.Deserialize<ConfigurationModel>(fs)!;
                return result;
            }
            catch
            {
                return null;
            }
        }

        public void Save(ConfigurationModel model)
        {
            using var fs = new FileStream(pathService.ConfigurationFilePath, FileMode.Create, FileAccess.Write);
            var serializer = new XmlSerializer();
            serializer.Serialize(model, fs);
        }
    }
}
