﻿using OpenMacroKeyboard.BusinessLogic.Models.Configuration;

namespace OpenMacroKeyboard.BusinessLogic.Services.Configuration
{
    public interface IConfigurationService
    {
        ConfigurationModel? Load();
        void Save(ConfigurationModel model);
    }
}