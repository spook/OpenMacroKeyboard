﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Services.UIThreadAccess
{
    public interface IUIThreadAccess
    {
        void RunOnUIThread(Action action);
    }
}
