﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Services.ActionRepository
{
    public interface IMacroAction : IDisposable
    {
        void Execute();
    }
}
