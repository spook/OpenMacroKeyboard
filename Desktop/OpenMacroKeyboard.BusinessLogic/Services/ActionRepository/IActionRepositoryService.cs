﻿using OpenMacroKeyboard.API.V1.Actions.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Services.ActionRepository
{
    public interface IActionRepositoryService
    {
        IEnumerable<Guid> ActionFactoryGuids { get; }

        IMacroAction CreateAction(Guid guid, Dictionary<string, object> settings);
        ActionMetadata GetActionFactoryMetadata(Guid guid);
    }
}
