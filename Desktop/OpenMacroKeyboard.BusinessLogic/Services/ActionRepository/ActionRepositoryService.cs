﻿using OpenMacroKeyboard.API.V1.Actions.Models;
using OpenMacroKeyboard.API.V1.Actions;
using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.API.V1.Drivers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using OpenMacroKeyboard.BusinessLogic.Services.Logging;
using OpenMacroKeyboard.BusinessLogic.Services.DriverRepository;

namespace OpenMacroKeyboard.BusinessLogic.Services.ActionRepository
{
    internal class ActionRepositoryService : IActionRepositoryService, IDisposable
    {
        private readonly ILogger logger;
        private readonly Dictionary<Guid, BaseActionFactory> actionFactories = new();
        private readonly Dictionary<uint, MacroAction> macroActions = new();

        public ActionRepositoryService(ILogger logger)
        {
            this.logger = logger;

            // Loading actions

            var actionsPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)!, "Actions");

            if (Directory.Exists(actionsPath))
            {
                foreach (var action in Directory.EnumerateFiles(actionsPath, "*.dll", SearchOption.AllDirectories))
                {
                    try
                    {
                        Assembly actionAssembly = Assembly.LoadFrom(action);

                        foreach (var type in actionAssembly.ExportedTypes)
                        {
                            if (type.IsAssignableTo(typeof(BaseActionFactory)) && !type.IsAbstract)
                            {
                                try
                                {
                                    var factory = (BaseActionFactory)Activator.CreateInstance(type, logger)!;

                                    // Sanity checks

                                    if (actionFactories.ContainsKey(factory.Metadata.Guid))
                                    {
                                        logger.Debug("OpenMacroKeyboard", $"Cannot add action factory {type.Name} from {Path.GetFileName(action)}, because another factory with same GUID is already registered.");
                                        factory.Dispose();
                                        continue;
                                    }

                                    actionFactories.Add(factory.Metadata.Guid, factory);
                                }
                                catch
                                {
                                    // Cannot construct this library, skip
                                    logger.Debug("OpenMacroKeyboard", $"Failed to instantiate factory {type.Name}, skipping");
                                    continue;
                                }
                            }
                        }
                    }
                    catch
                    {
                        // Most likely this is not a .NET assembly, skip
                        logger.Debug("OpenMacroKeyboard", $"Failed to load library {Path.GetFileName(action)}, skipping");
                        continue;
                    }
                }
            }
        }

        public void Dispose()
        {
            foreach (var kvp in actionFactories)
            {
                kvp.Value.Dispose();
            }
            actionFactories.Clear();
        }

        public ActionMetadata GetActionFactoryMetadata(Guid guid)
        {
            return actionFactories[guid].Metadata;
        }

        public IMacroAction CreateAction(Guid guid, Dictionary<string, object> settings)
        {
            if (!actionFactories.TryGetValue(guid, out BaseActionFactory? actionFactory))
                throw new ArgumentOutOfRangeException(nameof(guid));

            return new MacroAction(actionFactory!, settings);
        }

        public IEnumerable<Guid> ActionFactoryGuids => actionFactories.Keys;
    }
}
