﻿using OpenMacroKeyboard.API.V1.Actions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Services.ActionRepository
{
    internal class MacroAction : IMacroAction, IDisposable
    {
        private BaseAction action;

        public MacroAction(BaseActionFactory actionFactory, Dictionary<string, object> settings)
        {
            this.action = actionFactory.CreateAction(settings);
        }

        public void Execute()
        {
            action.Execute();
        }

        public void Dispose()
        {
            action.Dispose();
        }
    }
}
