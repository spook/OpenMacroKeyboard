﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Models.AddMacroKeyboard
{
    public class AddMacroKeyboardResult
    {
        public AddMacroKeyboardResult(Guid driverFactoryGuid, Dictionary<string, object> settings)
        {
            DriverGuid = driverFactoryGuid;
            Settings = settings;
        }

        public Guid DriverGuid { get; }
        public Dictionary<string, object> Settings { get; }
    }
}
