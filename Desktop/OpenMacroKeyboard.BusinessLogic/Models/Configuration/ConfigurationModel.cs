﻿using OpenMacroKeyboard.BusinessLogic.Models.Configuration.MacroKeyboard;
using Spooksoft.Xml.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Models.Configuration
{
    [SpkXmlRoot("Configuration")]
    public class ConfigurationModel
    {
        public ConfigurationModel(List<KeyboardInstance> keyboards)
        {
            Keyboards = keyboards;
        }

        [SpkXmlArray]
        public List<KeyboardInstance> Keyboards { get; }
    }
}
