﻿using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens.Macros;
using Spooksoft.Xml.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens
{
    [SpkXmlRoot("ScreenInstance")]
    public class Screen
    {
        public Screen(List<ButtonEntry> buttons, List<EncoderEntry> encoders)
        {
            Buttons = buttons ?? throw new ArgumentNullException(nameof(buttons));
            Encoders = encoders ?? throw new ArgumentNullException(nameof(encoders));
        }

        public static Screen Empty(DriverMetadata driverMetadata)
        {
            return new Screen(
                driverMetadata.ButtonDefinitions.Select(bd => ButtonEntry.Empty()).ToList(),
                driverMetadata.EncoderDefinitions.Select(ed => EncoderEntry.Empty()).ToList());
        }
         
        public static Screen EmptySubfolder(DriverMetadata driverMetadata)
        {
            List<ButtonEntry> buttonEntries = new();
            List<EncoderEntry> encoderEntries = new();

            if (driverMetadata.ButtonDefinitions.Count > 0)
            {
                buttonEntries.Add(new ButtonEntry(Resources.Macros.Strings.Description_Back, ElementColor.Zero, null, new BackMacro()));

                for (int i = 1; i < driverMetadata.ButtonDefinitions.Count; i++)
                    buttonEntries.Add(ButtonEntry.Empty());
            }
            else if (driverMetadata.EncoderDefinitions.Count > 0)
            {
                var firstButtonEncoder = driverMetadata.EncoderDefinitions.FirstOrDefault(e => e.HasButton);
                if (firstButtonEncoder != null)
                {
                    // Create empty entries for all encoders preceding one with button
                    int index = driverMetadata.EncoderDefinitions.IndexOf(firstButtonEncoder);
                    for (int i = 0; i < driverMetadata.EncoderDefinitions.Count; i++)
                    {
                        if (i == index)
                            encoderEntries.Add(new EncoderEntry(Resources.Macros.Strings.Description_Back, ElementColor.Zero, null, null, null, new BackMacro()));
                        else
                            encoderEntries.Add(EncoderEntry.Empty());
                    }
                }
                else
                {
                    for (int i = 0; i < driverMetadata.EncoderDefinitions.Count; i++)
                    {
                        if (i == 0)
                            encoderEntries.Add(new EncoderEntry(Resources.Macros.Strings.Description_Back, ElementColor.Zero, null, new BackMacro(), new BackMacro(), null));
                        else
                            encoderEntries.Add(EncoderEntry.Empty());
                    }
                }
            }

            return new Screen(buttonEntries, encoderEntries);
        }

        [SpkXmlArray]
        public List<ButtonEntry> Buttons { get; }
        [SpkXmlArray]
        public List<EncoderEntry> Encoders { get; }
    }
}
