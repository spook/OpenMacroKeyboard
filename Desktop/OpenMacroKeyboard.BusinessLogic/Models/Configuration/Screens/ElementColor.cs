﻿using Spooksoft.Xml.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens
{
    [SpkXmlRoot("Color")]
    public class ElementColor
    {
        public static readonly ElementColor Zero = new(0, 0, 0);

        public ElementColor(byte r, byte g, byte b)
        {
            R = r;
            G = g;
            B = b;
        }

        [SpkXmlAttribute("R")]
        public byte R { get; } 
        [SpkXmlAttribute("G")]
        public byte G { get; } 
        [SpkXmlAttribute("B")]
        public byte B { get; } 


    }
}
