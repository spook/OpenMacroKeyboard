﻿using Spooksoft.Xml.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens

{    
    public class BaseElementEntry
    {
        public BaseElementEntry(string description, ElementColor color, ElementImage? image)
        {
            Description = description;
            Color = color;
            Image = image;
        }

        [SpkXmlAttribute("Description")]
        public string Description { get; }

        [SpkXmlElement("Color")]
        public ElementColor Color { get; }

        [SpkXmlElement("Image")]
        public ElementImage? Image { get; }

        [SpkXmlIgnore]
        public object? CustomCache { get; set; }
    }
}
