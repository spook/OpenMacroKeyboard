﻿using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens.Macros;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens
{
    public class EncoderEntry : BaseElementEntry
    {
        public EncoderEntry(string description, ElementColor color, ElementImage? image, BaseMacro? incMacro, BaseMacro? decMacro, BaseMacro? pressMacro)
            : base(description, color, image)
        {
            IncMacro = incMacro;
            DecMacro = decMacro;
            PressMacro = pressMacro;
        }

        public static EncoderEntry Empty()
        {
            return new EncoderEntry(string.Empty, ElementColor.Zero, null, null, null, null);
        }

        public BaseMacro? IncMacro { get; }
        public BaseMacro? DecMacro { get; }
        public BaseMacro? PressMacro { get; }
    }
}
