﻿using Spooksoft.Xml.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens.Macros
{
    [SpkXmlIncludeDerived("Macro", typeof(BaseMacro))]
    [SpkXmlIncludeDerived("Action", typeof(ActionMacro))]
    [SpkXmlIncludeDerived("Back", typeof(BackMacro))]
    [SpkXmlIncludeDerived("Folder", typeof(FolderMacro))]
    [SpkXmlIncludeDerived("Multi", typeof(MultiMacro))]
    [SpkXmlIncludeDerived("Pause", typeof(PauseMacro))]
    [SpkXmlIncludeDerived("Sleep", typeof(SleepMacro))]
    public abstract class BaseMacro
    {

    }
}
