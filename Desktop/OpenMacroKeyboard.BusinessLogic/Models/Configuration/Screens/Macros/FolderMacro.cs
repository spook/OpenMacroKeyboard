﻿using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens.Macros
{
    public class FolderMacro : BaseMacro
    {
        public FolderMacro(Screen screen)
        {
            Screen = screen;
        }

        public Screen Screen { get; }
    }
}
