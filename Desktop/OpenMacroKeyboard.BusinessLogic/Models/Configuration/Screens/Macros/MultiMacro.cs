﻿using Spooksoft.Xml.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens.Macros
{
    public class MultiMacro : BaseMacro
    {
        public MultiMacro(IReadOnlyList<BaseMacro> subMacros)
        {
            SubMacros = subMacros;
        }

        [SpkXmlArray]
        public IReadOnlyList<BaseMacro> SubMacros { get; }
    }
}
