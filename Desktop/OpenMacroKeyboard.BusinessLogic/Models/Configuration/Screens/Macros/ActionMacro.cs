﻿using Spooksoft.Xml.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens.Macros
{
    public class ActionMacro : BaseMacro
    {
        public ActionMacro(Guid actionGuid, Dictionary<string, object> settings)
        {
            ActionGuid = actionGuid;
            Settings = settings;
        }

        public Guid ActionGuid { get; }

        [SpkXmlMap]
        [SpkXmlMapValue("String", typeof(string))]
        [SpkXmlMapValue("Int32", typeof(int))]
        [SpkXmlMapValue("Boolean", typeof(bool))]
        public Dictionary<string, object> Settings { get; }
    }
}
