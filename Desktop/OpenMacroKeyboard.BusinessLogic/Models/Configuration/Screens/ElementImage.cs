﻿using Spooksoft.Xml.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens
{    
    public class ElementImage
    {
        public ElementImage(byte[] argb, int width, int height)
        {
            Argb = argb;
            Width = width;
            Height = height;
        }

        [SpkXmlBinary]
        [SpkXmlElement("ARGB")]
        public byte[] Argb { get; }

        [SpkXmlElement("Width")]
        public int Width { get; }

        [SpkXmlElement("Height")]
        public int Height { get; }

    }
}
