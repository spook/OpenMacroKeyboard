﻿using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens.Macros;
using Spooksoft.Xml.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens
{
    public class ButtonEntry : BaseElementEntry
    {
        public ButtonEntry(string description, ElementColor color, ElementImage? image, BaseMacro? macro)
            : base(description, color, image)
        {
            Macro = macro;
        }

        public static ButtonEntry Empty()
        {
            return new ButtonEntry(string.Empty, ElementColor.Zero, null, null);
        }

        public BaseMacro? Macro { get; }
    }
}
