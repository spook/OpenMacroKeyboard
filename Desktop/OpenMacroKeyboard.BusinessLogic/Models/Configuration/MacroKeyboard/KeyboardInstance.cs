﻿using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens;
using Spooksoft.Xml.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Models.Configuration.MacroKeyboard
{
    [SpkXmlRoot("Keyboard")]
    public class KeyboardInstance
    {
        public KeyboardInstance(Guid driverGuid, Dictionary<string, object> settings, Screen screen)
        {
            DriverGuid = driverGuid;
            Settings = settings;
            Screen = screen;
        }

        [SpkXmlAttribute("DriverGuid")]
        public Guid DriverGuid { get; }
        [SpkXmlMap]
        [SpkXmlMapValue("String", typeof(string))]
        [SpkXmlMapValue("Int32", typeof(int))]
        [SpkXmlMapValue("Boolean", typeof(bool))]
        public Dictionary<string, object> Settings { get; }
        public Screen Screen { get; }
    }
}
