﻿using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Models.MacroKeyboardDisplay
{
    public class ElementVisuals
    {
        public ElementVisuals(string? description, bool showDescription, ElementColor color, bool showColor, ElementImage? image, bool showImage)
        {
            Description = description;
            ShowDescription = showDescription;
            Color = color;
            ShowColor = showColor;
            Image = image;
            ShowImage = showImage;
        }

        public string? Description { get; }
        public bool ShowDescription { get; }
        public ElementColor Color { get; }
        public bool ShowColor { get; }
        public ElementImage? Image { get; }
        public bool ShowImage { get; }
        public bool IsImagePresent => Image != null;
    }
}
