﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Attributes
{
    public class DisplayResourceKeyAttribute : Attribute
    {
        public DisplayResourceKeyAttribute(string nameKey, string groupKey)
        {
            NameKey = nameKey;
            GroupKey = groupKey;
        }

        public string NameKey { get; }
        public string GroupKey { get; }
    }
}
