﻿using Autofac;
using OpenMacroKeyboard.BusinessLogic.Services.ActionRepository;
using OpenMacroKeyboard.BusinessLogic.Services.Configuration;
using OpenMacroKeyboard.BusinessLogic.Services.DriverRepository;
using OpenMacroKeyboard.BusinessLogic.Services.EventBus;
using OpenMacroKeyboard.BusinessLogic.Services.Logging;
using OpenMacroKeyboard.BusinessLogic.Services.MacroKeyboardManager;
using OpenMacroKeyboard.BusinessLogic.Services.Paths;
using OpenMacroKeyboard.BusinessLogic.ViewModels.Main;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Dependencies
{
    public static class Configuration
    {
        private static bool isConfigured = false;

        public static void Configure(ContainerBuilder builder)
        {
            if (isConfigured)
                return;
            isConfigured = true;

            // Register services
            builder.RegisterType<EventBus>().As<IEventBus>().SingleInstance();
            builder.RegisterType<PathService>().As<IPathService>().SingleInstance();
            builder.RegisterType<DriverRepositoryService>().As<IDriverRepositoryService>().SingleInstance();
            builder.RegisterType<ActionRepositoryService>().As<IActionRepositoryService>().SingleInstance();
            builder.RegisterType<Logger>().As<ILogger>().SingleInstance();
            builder.RegisterType<ConfigurationService>().As<IConfigurationService>().SingleInstance();
            builder.RegisterType<MacroKeyboardManagerService>().As<IMacroKeyboardManagerService>().SingleInstance();
        }
    }
}
