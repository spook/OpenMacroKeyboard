﻿namespace OpenMacroKeyboard.BusinessLogic.Tools
{
    public class KeyboardMetrics
    {
        public KeyboardMetrics(float minX, float minY, float width, float height, float ratioX, float ratioY)
        {
            MinX = minX;
            MinY = minY;
            Width = width;
            Height = height;
            RatioX = ratioX;
            RatioY = ratioY;
        }

        public float MinX { get; }
        public float MinY { get; }
        public float Width { get; }
        public float Height { get; }
        public float RatioX { get; }
        public float RatioY { get; }
    }
}