﻿using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.BusinessLogic.Models.MacroKeyboardDisplay;
using OpenMacroKeyboard.BusinessLogic.ViewModels.AddMacroKeyboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.Tools
{
    public static class KeyboardGeometry
    {
        public static KeyboardMetrics EvalKeyboardMetrics(DriverMetadata metadata, float buttonYSize, float? buttonXSize = null)
        {
            if (buttonXSize == null)
                buttonXSize = buttonYSize;

            float minX;
            float minY;
            float defWidth;
            float defHeight;
            float ratioX;
            float ratioY;

            float minButtonX, maxButtonX, minButtonY, maxButtonY;
            float minEncoderX, maxEncoderX, minEncoderY, maxEncoderY;

            if (metadata.ButtonDefinitions.Any())
            {
                minButtonX = metadata.ButtonDefinitions.Min(b => b.X);
                maxButtonX = metadata.ButtonDefinitions.Max(b => b.X + b.Width);
                minButtonY = metadata.ButtonDefinitions.Min(b => b.Y);
                maxButtonY = metadata.ButtonDefinitions.Max(b => b.Y + b.Height);
            }
            else
            {
                minButtonX = maxButtonX = minButtonY = maxButtonY = 0.0f;
            }

            if (metadata.EncoderDefinitions.Any())
            {
                minEncoderX = metadata.EncoderDefinitions.Min(b => b.X);
                maxEncoderX = metadata.EncoderDefinitions.Max(b => b.X + b.Width);
                minEncoderY = metadata.EncoderDefinitions.Min(b => b.Y);
                maxEncoderY = metadata.EncoderDefinitions.Max(b => b.Y + b.Height);
            }
            else
            {
                minEncoderX = maxEncoderX = minEncoderY = maxEncoderY = 0.0f;
            }

            float maxX, maxY;

            if (metadata.ButtonDefinitions.Count != 0 && metadata.EncoderDefinitions.Count == 0)
            {
                minX = minButtonX;
                minY = minButtonY;
                maxX = maxButtonX;
                maxY = maxButtonY;
            }
            else if (metadata.ButtonDefinitions.Count == 0 && metadata.EncoderDefinitions.Count != 0)
            {
                minX = minEncoderX;
                minY = minEncoderY;
                maxX = maxEncoderX;
                maxY = maxEncoderY;
            }
            else
            {
                minX = Math.Min(minButtonX, minEncoderX);
                maxX = Math.Max(maxButtonX, maxEncoderX);
                minY = Math.Min(minButtonY, minEncoderY);
                maxY = Math.Max(maxButtonY, maxEncoderY);
            }

            // Calculating scaling ratio

            defWidth = Math.Max(maxX - minX, 1.0f);
            defHeight = Math.Max(maxY - minY, 1.0f);

            // Default button size is 48dip x 48dip
            // But if keyboard is too big, we need to scale it down

            ratioX = buttonXSize.Value;
            ratioY = buttonYSize;

            float width = defWidth * ratioX;
            float height = defHeight * ratioY;

            return new KeyboardMetrics(minX, minY, width, height, ratioX, ratioY);
        }

        public static ElementMetrics EvalElementMetrics(KeyboardMetrics keyboardMetrics, BaseKeyboardElementInfo info)
        {
            float x = (info.X - keyboardMetrics.MinX) * keyboardMetrics.RatioX;
            float y = (info.Y - keyboardMetrics.MinY) * keyboardMetrics.RatioY;
            float width = info.Width * keyboardMetrics.RatioX;
            float height = info.Height * keyboardMetrics.RatioY;

            return new ElementMetrics(x, y, width, height);
        }
    }
}
