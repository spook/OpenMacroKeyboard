﻿using OpenMacroKeyboard.BusinessLogic.Models.MacroKeyboardDisplay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.AddMacroKeyboard
{
    public abstract class BaseKeyboardItemViewModel
    {
        public BaseKeyboardItemViewModel(int index,
            ElementMetrics metrics,
            bool canSetColor,
            bool canSetImage,
            bool canSetDescription)
        {
            Index = index;
            Metrics = metrics;
            CanSetColor = canSetColor;
            CanSetImage = canSetImage;
            CanSetDescription = canSetDescription;
        }

        public ElementMetrics Metrics { get; }
        public bool CanSetColor { get; }
        public bool CanSetDescription { get; }
        public bool CanSetImage { get; }
        public int Index { get; }
    }
}
