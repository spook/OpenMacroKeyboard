﻿using OpenMacroKeyboard.BusinessLogic.Models.MacroKeyboardDisplay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.AddMacroKeyboard
{
    public class EncoderViewModel : BaseKeyboardItemViewModel
    {
        public EncoderViewModel(int index,
            ElementMetrics metrics,
            bool hasButton,
            bool canSetColor,
            bool canSetImage,
            bool canSetDescription) : base(index,
                metrics,
                canSetColor,            
                canSetImage,
                canSetDescription)
        {
            HasButton = hasButton;
        }

        public bool HasButton { get; }
    }
}
