﻿using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.BusinessLogic.Tools;
using OpenMacroKeyboard.BusinessLogic.ViewModels.Base;
using OpenMacroKeyboard.BusinessLogic.ViewModels.Settings;
using Spooksoft.VisualStateManager.Conditions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.AddMacroKeyboard
{
    public class DriverFactoryViewModel : BaseViewModel
    {
        private const float DEFAULT_BUTTON_SIZE = 64.0f;
        private readonly List<BaseKeyboardItemViewModel> keyboardItems = new();
        private readonly ObservableCollection<BaseSettingViewModel> settings = new();

        public DriverFactoryViewModel(DriverMetadata metadata, IReadOnlyDictionary<string, object>? currentSettings = null) 
        {
            Name = metadata.DriverName;
            Guid = metadata.Guid;

            if (metadata.ButtonDefinitions.Count > 0 || metadata.EncoderDefinitions.Count > 0)
            {
                // Collecting information about extremes of the keyboard
                
                var keyboardMetrics = KeyboardGeometry.EvalKeyboardMetrics(metadata, DEFAULT_BUTTON_SIZE);

                // Generate button viewmodels

                for (int i = 0; i < metadata.ButtonDefinitions.Count; i++)
                {
                    var definition = metadata.ButtonDefinitions[i];

                    var buttonMetrics = KeyboardGeometry.EvalElementMetrics(keyboardMetrics, definition);

                    var bvm = new ButtonViewModel(i, 
                        buttonMetrics,
                        definition.CanSetColor,
                        definition.CanSetImage,
                        definition.CanSetDescription);

                    keyboardItems.Add(bvm);
                }

                // Generate encoder viewmodels

                for (int i = 0; i < metadata.EncoderDefinitions.Count; i++)
                {
                    var definition = metadata.EncoderDefinitions[i];

                    var encoderMetrics = KeyboardGeometry.EvalElementMetrics(keyboardMetrics, definition);

                    var evm = new EncoderViewModel(i,
                        encoderMetrics,
                        definition.HasButton,
                        definition.CanSetColor,
                        definition.CanSetImage,
                        definition.CanSetDescription);

                    keyboardItems.Add(evm);
                }

                // Evaluate preview width and height

                Width = keyboardMetrics.Width;
                Height = keyboardMetrics.Height;
            }
            else
            {
                // Fallback when there are no keyboard items

                Width = 300.0f;
                Height = 200.0f;
            }

            foreach (var setting in metadata.Settings)
            {
                if (currentSettings != null && 
                    currentSettings.TryGetValue(setting.Key, out object? currentValue))
                {
                    settings.Add(BaseSettingViewModel.FromModel(setting, currentValue));
                }
                else
                {
                    settings.Add(BaseSettingViewModel.FromModel(setting));
                }
            }

            SettingsValid = Condition.All(this.settings, s => s.SettingValidCondition, false);
        }

        public string Name { get; }
        public Guid Guid { get; }
        public float Width { get; }
        public float Height { get; }

        public IReadOnlyList<BaseKeyboardItemViewModel> KeyboardItems => keyboardItems;
        public IReadOnlyList<BaseSettingViewModel> Settings => settings;

        public BaseCondition SettingsValid { get; }
    }
}
