﻿using OpenMacroKeyboard.BusinessLogic.Models.AddMacroKeyboard;
using OpenMacroKeyboard.BusinessLogic.Services.DriverRepository;
using OpenMacroKeyboard.BusinessLogic.ViewModels.Base;
using OpenMacroKeyboard.BusinessLogic.ViewModels.Settings;
using Spooksoft.VisualStateManager.Commands;
using Spooksoft.VisualStateManager.Conditions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.AddMacroKeyboard
{
    public class AddMacroKeyboardViewModel : BaseViewModel
    {
        private readonly IAddMacroKeyboardWindowAccess access;
        private readonly IDriverRepositoryService driverRepository;
        private readonly ObservableCollection<DriverFactoryViewModel> driverFactories;
        
        private DriverFactoryViewModel? selectedDriverFactory;

        private BaseCondition keyboardSelectedCondition;
        private BaseCondition settingsValidatedCondition;

        private AddMacroKeyboardResult BuildResult()
        {
            if (selectedDriverFactory == null)
                throw new InvalidOperationException("No driver factory is selected!");

            Dictionary<string, object> settings = new();

            foreach (var setting in selectedDriverFactory.Settings)
                settings[setting.Key] = setting.ToSettingValue();

            return new AddMacroKeyboardResult(selectedDriverFactory.Guid, settings);
        }

        private void DoCancel()
        {
            access.Close(false);
        }

        private void DoOk()
        {
            access.Close(true);
        }

        public AddMacroKeyboardViewModel(IAddMacroKeyboardWindowAccess access, IDriverRepositoryService driverRepository) 
        {
            this.access = access;
            this.driverRepository = driverRepository;

            driverFactories = new ObservableCollection<DriverFactoryViewModel>();
            foreach (var guid in driverRepository.DriverFactoryGuids)
            {
                var metadata = driverRepository.GetDriverFactoryMetadata(guid);
                var vm = new DriverFactoryViewModel(metadata);
                driverFactories.Add(vm);
            }

            SelectedDriverFactory = driverFactories.FirstOrDefault();

            keyboardSelectedCondition = Condition.Lambda(this, vm => vm.SelectedDriverFactory != null, false);
            settingsValidatedCondition = Condition.Lambda(this, vm => vm.SelectedDriverFactory != null && vm.SelectedDriverFactory.SettingsValid.Value, false);

            OkCommand = new AppCommand(obj => DoOk(), keyboardSelectedCondition & settingsValidatedCondition);
            CancelCommand = new AppCommand(obj => DoCancel());
        }

        public DriverFactoryViewModel? SelectedDriverFactory
        {
            get => selectedDriverFactory;
            set => Set(ref selectedDriverFactory, value);
        }

        public ObservableCollection<DriverFactoryViewModel> DriverLibraries => driverFactories;

        public ICommand OkCommand { get; }
        public ICommand CancelCommand { get; }

        public AddMacroKeyboardResult Result => BuildResult();
    }
}
