﻿using OpenMacroKeyboard.BusinessLogic.Models.MacroKeyboardDisplay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.AddMacroKeyboard
{
    public class ButtonViewModel : BaseKeyboardItemViewModel
    {
        public ButtonViewModel(int index,
            ElementMetrics metrics,
            bool canSetColor,
            bool canSetImage,
            bool canSetDescription) : base(index,
                metrics,
                canSetColor,
                canSetImage,
                canSetDescription)
        {

        }
    }
}
