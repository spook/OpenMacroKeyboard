﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.AddMacroKeyboard
{
    public interface IAddMacroKeyboardWindowAccess
    {
        public void Close(bool result);
    }
}
