﻿using OpenMacroKeyboard.API.V1.Common.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.Settings
{
    public class BoolSettingViewModel : BaseSettingViewModel
    {
        private readonly BoolSettingInfo boolSettingInfo;
        private bool value;

        public BoolSettingViewModel(BoolSettingInfo boolSettingInfo, object? existingValue)
            : base(boolSettingInfo)
        {
            this.boolSettingInfo = boolSettingInfo;

            if (existingValue is bool existingBoolValue)
                value = existingBoolValue;
            else
                value = boolSettingInfo.DefaultValue;
        }

        public override object ToSettingValue() => value;

        public bool Value
        {
            get => value;
            set => Set(ref this.value, value);
        }
    }
}
