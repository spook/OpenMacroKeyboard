﻿using OpenMacroKeyboard.API.V1.Common.Models;
using OpenMacroKeyboard.BusinessLogic.ViewModels.Base;
using Spooksoft.VisualStateManager.Conditions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.Settings
{
    public abstract class BaseSettingViewModel : BaseViewModel
    {
        private readonly BaseSettingInfo baseSettingInfo;
        private IReadOnlyList<string>? errors;

        private void HandleErrorsChanged()
        {
            OnPropertyChanged(nameof(IsError));
        }

        public BaseSettingViewModel(BaseSettingInfo baseSettingInfo)
        {
            this.baseSettingInfo = baseSettingInfo;
            this.errors = null;

            SettingValidCondition = Condition.Lambda(this, vm => !vm.IsError, false);
        }

        public static BaseSettingViewModel FromModel(BaseSettingInfo baseSettingInfo, object? existingValue = null) => baseSettingInfo switch
        {
            IntSettingInfo intSettingInfo => new IntSettingViewModel(intSettingInfo, existingValue),
            StringSettingInfo stringSettingInfo => new StringSettingViewModel(stringSettingInfo, existingValue),
            BoolSettingInfo boolSettingInfo => new BoolSettingViewModel(boolSettingInfo, existingValue),
            SelectSettingInfo selectSettingInfo => new SelectSettingViewModel(selectSettingInfo, existingValue),
            _ => throw new InvalidOperationException("Unsupported setting type!")
        };

        public static List<BaseSettingViewModel> FromModelList(List<BaseSettingInfo> infos) =>
            infos
            .Select(i => BaseSettingViewModel.FromModel(i))
            .ToList();

        public abstract object ToSettingValue();

        public string Key => baseSettingInfo.Key;

        public string Description => baseSettingInfo.Description;

        public IReadOnlyList<string>? Errors
        {
            get => errors;
            set => Set(ref errors, value, changeHandler:HandleErrorsChanged);
        }

        public bool IsError => Errors?.Any() ?? false;

        public BaseCondition SettingValidCondition { get; }
    }
}
