﻿using OpenMacroKeyboard.API.V1.Common.Models;
using OpenMacroKeyboard.BusinessLogic.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.Settings
{
    public class StringSettingViewModel : BaseSettingViewModel
    {
        private readonly StringSettingInfo stringSettingInfo;
        private string value;

        private void Validate()
        {
            List<string> errors = new();

            if (stringSettingInfo.MaxLength.HasValue && (value?.Length ?? 0) >= stringSettingInfo.MaxLength.Value)
                errors.Add(string.Format(Resources.Settings.Strings.Error_TextTooLong, stringSettingInfo.MaxLength));

            if (stringSettingInfo.ValidationRegex != null && !stringSettingInfo.ValidationRegex.IsMatch(value ?? string.Empty))
                errors.Add(Resources.Settings.Strings.Error_TextDoesNotMatchRegex);

            Errors = errors;
        }

        private void HandleValueChanged()
        {
            Validate();
        }

        public StringSettingViewModel(StringSettingInfo stringSettingInfo, object? existingValue)
            : base(stringSettingInfo)
        {
            this.stringSettingInfo = stringSettingInfo;

            if (existingValue is string existingStringValue)
                value = existingStringValue;
            else
                value = stringSettingInfo.DefaultValue;

            Validate();
        }

        public override object ToSettingValue() => value;

        public string Value
        {
            get => value;
            set => Set(ref this.value, value, changeHandler: HandleValueChanged);
        }
    }
}
