﻿using OpenMacroKeyboard.API.V1.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.Settings
{
    public class IntSettingViewModel : BaseSettingViewModel
    {
        private readonly IntSettingInfo intSettingInfo;
        private string value;

        private void Validate()
        {
            List<string> errors = new();

            if (!int.TryParse(value, out int result))
            {
                errors.Add(Resources.Settings.Strings.Error_InvalidIntValue);
                Errors = errors;
                return;
            }

            if (intSettingInfo.MinValue.HasValue && result < intSettingInfo.MinValue.Value)
                errors.Add(string.Format(Resources.Settings.Strings.Error_TooSmallValue, intSettingInfo.MinValue));

            if (intSettingInfo.MaxValue.HasValue && result > intSettingInfo.MaxValue.Value)
                errors.Add(string.Format(Resources.Settings.Strings.Error_TooBigValue, intSettingInfo.MaxValue));

            Errors = errors;
        }

        private void HandleValueChanged()
        {
            Validate();
        }

        public IntSettingViewModel(IntSettingInfo intSettingInfo, object? existingValue = null)
            : base(intSettingInfo)
        {
            this.intSettingInfo = intSettingInfo;

            if (existingValue is int existingIntValue)
                value = existingIntValue.ToString();
            else
                value = intSettingInfo.DefaultValue.ToString();

            Validate();
        }

        public override object ToSettingValue() => int.Parse(value);

        public string Value
        {
            get => value;
            set => Set(ref this.value, value, changeHandler: HandleValueChanged);
        }
    }
}
