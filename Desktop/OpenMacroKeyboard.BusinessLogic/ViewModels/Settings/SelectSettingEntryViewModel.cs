﻿using OpenMacroKeyboard.API.V1.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.Settings
{
    public class SelectSettingEntryViewModel
    {
        private readonly SelectSettingEntryInfo selectSettingEntryInfo;

        public SelectSettingEntryViewModel(SelectSettingEntryInfo selectSettingEntryInfo)
        {
            this.selectSettingEntryInfo = selectSettingEntryInfo;
        }

        public string Description => selectSettingEntryInfo.Description;
        public int Value => selectSettingEntryInfo.Value;
    }
}
