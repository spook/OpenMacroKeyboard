﻿using OpenMacroKeyboard.API.V1.Common.Models;
using OpenMacroKeyboard.BusinessLogic.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.Settings
{
    public class SelectSettingViewModel : BaseSettingViewModel
    {
        private readonly SelectSettingInfo selectSettingInfo;
        private readonly List<SelectSettingEntryViewModel> availableEntries;
        private SelectSettingEntryViewModel? selectedEntry;

        private void Validate()
        {
            List<string> errors = new();

            if (SelectedEntry == null)
                errors.Add(Resources.Settings.Strings.Error_NoOptionChosen);

            Errors = errors;
        }

        private void HandleSelectedEntryChanged()
        {
            Validate();
        }

        public SelectSettingViewModel(SelectSettingInfo selectSettingInfo, object? existingValue)
            : base(selectSettingInfo)
        {
            this.selectSettingInfo = selectSettingInfo;

            availableEntries = selectSettingInfo.AvailableEntries
                .Select(e => new SelectSettingEntryViewModel(e))
                .ToList();

            int selectedValue = existingValue is int existingIntValue ? existingIntValue : selectSettingInfo.DefaultValue;

            selectedEntry = availableEntries.FirstOrDefault(e => e.Value == selectedValue);
            selectedEntry ??= availableEntries.FirstOrDefault();
            Validate();
        }

        public override object ToSettingValue()
        {
            if (selectedEntry == null)
                throw new InvalidOperationException("No value was selected!");
            
            return selectedEntry.Value;
        }

        public IReadOnlyList<SelectSettingEntryViewModel> AvailableEntries => availableEntries;

        public SelectSettingEntryViewModel? SelectedEntry
        {
            get => selectedEntry;
            set => Set(ref selectedEntry, value, changeHandler: HandleSelectedEntryChanged);
        }
    }
}
