﻿using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.MacroKeyboard;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens.Macros;
using OpenMacroKeyboard.BusinessLogic.Services.ActionRepository;
using OpenMacroKeyboard.BusinessLogic.Services.Configuration;
using OpenMacroKeyboard.BusinessLogic.Services.Dialogs;
using OpenMacroKeyboard.BusinessLogic.Services.DriverRepository;
using OpenMacroKeyboard.BusinessLogic.Services.MacroKeyboardManager;
using OpenMacroKeyboard.BusinessLogic.Services.Messaging;
using OpenMacroKeyboard.BusinessLogic.ViewModels.AddMacroKeyboard;
using OpenMacroKeyboard.BusinessLogic.ViewModels.Base;
using OpenMacroKeyboard.BusinessLogic.ViewModels.MacroKeyboard;
using OpenMacroKeyboard.Resources.Windows.Main;
using Spooksoft.VisualStateManager.Commands;
using Spooksoft.VisualStateManager.Conditions;
using Spooksoft.Xml.Serialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.ObjectiveC;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.Main
{
    public class MainWindowViewModel : BaseViewModel
    {
        // Private fields -----------------------------------------------------

        private readonly IMainWindowAccess access;
        private readonly IMacroKeyboardManagerService macroKeyboardManager;
        private readonly IDialogService dialogService;
        private readonly IMessagingService messagingService;
        private readonly IConfigurationService configurationService;
        private IMacroKeyboard? selectedMacroKeyboard;
        private readonly MacroKeyboardViewModel macroKeyboardViewModel;
        private readonly IDriverRepositoryService driverRepositoryService;

        // Private methods ----------------------------------------------------

        private void DoAddMacroKeyboard()
        {
            (bool result, var model) = dialogService.ShowAddKeyboardDialog();

            if (result && model != null)
            {
                IMacroKeyboard macroKeyboard = macroKeyboardManager.AddKeyboard(model.DriverGuid, model.Settings);
                SelectedMacroKeyboard = macroKeyboard;
            }
        }

        private void DoEditMacros()
        {
            (bool result, Screen? editedScreen) = dialogService.ShowMacroEditorDialog(selectedMacroKeyboard!.DriverMetadata, selectedMacroKeyboard!.RootScreen);

            if (result && editedScreen != null)
            {
                macroKeyboardManager.UpdateMacroLayout(selectedMacroKeyboard, editedScreen);
            }
        }

        private void DoDeleteMacroKeyboard()
        {
            if (selectedMacroKeyboard == null)
                return;

            if (messagingService.AskYesNo(OpenMacroKeyboard.Resources.Windows.Main.Strings.Message_DeleteKeyboardQuestion) == true)
            {
                macroKeyboardManager.DeleteKeyboard(selectedMacroKeyboard);

                if (MacroKeyboards.Count > 0)
                    SelectedMacroKeyboard = MacroKeyboards[0];
                else
                    SelectedMacroKeyboard = null;
            }
        }        

        private void DoReconnectMacroKeyboard()
        {
            if (selectedMacroKeyboard == null)
                return;

            (var result, var newSettings) = dialogService.ShowReconnectDialog(selectedMacroKeyboard.DriverMetadata, selectedMacroKeyboard.Settings);

            if (result)
            {
                macroKeyboardManager.ReconnectMacroKeyboard(selectedMacroKeyboard, newSettings);
            }
        }

        private void HandleSelectedMacroKeyboardChanged()
        {
            MacroKeyboard.MacroKeyboard = selectedMacroKeyboard;
        }

        private void DoExportMacroCommand()
        {
            (bool result, string? path) = dialogService.ShowSaveDialog(Strings.Filter_MacroKeyboardProfile);
            if (result && path != null && selectedMacroKeyboard != null)
            {
                var serializer = new XmlSerializer();
                var keyboard = new KeyboardInstance(selectedMacroKeyboard.DriverMetadata.Guid,
                    new Dictionary<string, object>(selectedMacroKeyboard.Settings),
                    selectedMacroKeyboard.RootScreen ?? Screen.Empty(selectedMacroKeyboard.DriverMetadata));

                using var fs = File.OpenWrite(path);
                serializer.Serialize(keyboard, fs);
            }
        }

        private bool ValidateBackMacro(Screen screen, int nestLevel = 0)
        {
            bool hasBack = false;

            foreach (var button in screen.Buttons)
            {
                if (button.Macro is BackMacro)
                    hasBack = true;
                else if (button.Macro is FolderMacro folder && !ValidateBackMacro(folder.Screen, nestLevel + 1))
                    return false;
            }

            foreach (var encoder in screen.Encoders)
            {
                if (encoder.IncMacro is BackMacro)
                    hasBack = true;
                else if (encoder.IncMacro is FolderMacro folder && !ValidateBackMacro(folder.Screen, nestLevel + 1))
                    return false;

                if (encoder.DecMacro is BackMacro)
                    hasBack = true;
                else if (encoder.DecMacro is FolderMacro folder && !ValidateBackMacro(folder.Screen, nestLevel + 1))
                    return false;

                if (encoder.PressMacro is BackMacro)
                    hasBack = true;
                else if (encoder.PressMacro is FolderMacro folder && !ValidateBackMacro(folder.Screen, nestLevel + 1))
                    return false;
            }

            // For 0-th level, back macro is not required
            return (nestLevel == 0 || hasBack);
        }

        private bool ValidateDriverComplianceRecursive(Screen screen, DriverMetadata driver)
        {
            if (screen.Buttons.Count > driver.ButtonDefinitions.Count)
                return false;

            if (screen.Encoders.Count > driver.EncoderDefinitions.Count)
                return false;

            foreach (var button in screen.Buttons) 
            {
                if (button.Macro is FolderMacro folder && !ValidateDriverComplianceRecursive(folder.Screen, driver))
                    return false;
            }
                    
            foreach (var encoder in screen.Encoders)
            {
                if (encoder.IncMacro is FolderMacro incFolder && !ValidateDriverComplianceRecursive(incFolder.Screen, driver))
                    return false;
                if (encoder.DecMacro is FolderMacro decFolder && !ValidateDriverComplianceRecursive(decFolder.Screen, driver))
                    return false;
                if (encoder.PressMacro is FolderMacro pressFolder && !ValidateDriverComplianceRecursive(pressFolder.Screen, driver))
                    return false;
            }

            return true;
        }

        private bool ValidateDriverCompliance(KeyboardInstance keyboard, DriverMetadata driver)
        {
            return ValidateDriverComplianceRecursive(keyboard.Screen, driver);
        }

        private void DoImportMacroCommand()
        {
            (bool result, string? path) = dialogService.ShowOpenDialog(Strings.Filter_MacroKeyboardProfile);
            
            if (result && path != null)
            {
                var serializer = new XmlSerializer();
                KeyboardInstance? keyboard = null;

                try
                {
                    using var fs = File.OpenRead(path);
                    keyboard = serializer.Deserialize<KeyboardInstance>(fs);
                }
                catch
                {
                    messagingService.ShowError(Strings.Message_FailedToLoadProfile);
                    return;
                }

                if (keyboard == null)
                {
                    messagingService.ShowError(Strings.Message_FailedToLoadProfile);
                    return;
                }

                // Check for valid driver

                if (!driverRepositoryService.DriverFactoryGuids.Contains(keyboard.DriverGuid))
                {
                    messagingService.ShowError(Strings.Message_MissingProfileDriver);
                    return;
                }

                var driver = driverRepositoryService.GetDriverFactoryMetadata(keyboard.DriverGuid);

                // Check for driver compliance

                if (!ValidateDriverCompliance(keyboard, driver))
                {
                    messagingService.ShowError(Strings.Message_ImportedKeyboardLayoutMismatch);
                    return;
                }

                // Check for back macro in all screens

                if (!ValidateBackMacro(keyboard.Screen))
                {
                    messagingService.ShowError(Strings.Message_MissingBackMacro);
                    return;
                }

                // Assume that keyboard is valid and perform the import

                var macroKeyboard = macroKeyboardManager.ImportKeyboard(keyboard);
                SelectedMacroKeyboard = macroKeyboard;
            }
        }

        // Public methods -----------------------------------------------------

        public MainWindowViewModel(IMainWindowAccess access,
            IDialogService dialogService,
            IMessagingService messagingService,
            IConfigurationService configurationService,
            IMacroKeyboardManagerService macroKeyboardManager,
            IDriverRepositoryService driverRepositoryService)
        {
            this.access = access;
            this.dialogService = dialogService;
            this.messagingService = messagingService;
            this.configurationService = configurationService;
            this.macroKeyboardManager = macroKeyboardManager;
            this.driverRepositoryService = driverRepositoryService;

            macroKeyboardViewModel = new MacroKeyboardViewModel();

            var macroKeyboardSelectedCondition = Condition.Lambda(this, vm => vm.SelectedMacroKeyboard != null, false);

            AddMacroKeyboardCommand = new AppCommand(obj => DoAddMacroKeyboard());
            DeleteMacroKeyboardCommand = new AppCommand(obj => DoDeleteMacroKeyboard(), macroKeyboardSelectedCondition);
            EditMacrosCommand = new AppCommand(obj => DoEditMacros(), macroKeyboardSelectedCondition);
            ReconnectMacroKeyboardCommand = new AppCommand(obj => DoReconnectMacroKeyboard(), macroKeyboardSelectedCondition);
            ExportMacroKeyboardCommand = new AppCommand(obj => DoExportMacroCommand(), macroKeyboardSelectedCondition);
            ImportMacroKeyboardCommand = new AppCommand(obj => DoImportMacroCommand(), macroKeyboardSelectedCondition);

            SelectedMacroKeyboard = macroKeyboardManager.MacroKeyboards.FirstOrDefault();
        }

        // Public properties --------------------------------------------------

        public ICommand AddMacroKeyboardCommand { get; }

        public ICommand EditMacrosCommand { get; }

        public ICommand DeleteMacroKeyboardCommand { get; }

        public ICommand ReconnectMacroKeyboardCommand { get; }

        public ICommand ExportMacroKeyboardCommand { get; }

        public ICommand ImportMacroKeyboardCommand { get; }

        public IReadOnlyList<IMacroKeyboard> MacroKeyboards => macroKeyboardManager.MacroKeyboards;

        public IMacroKeyboard? SelectedMacroKeyboard
        {
            get => selectedMacroKeyboard;
            set => Set(ref selectedMacroKeyboard, value, changeHandler: HandleSelectedMacroKeyboardChanged);
        }

        public MacroKeyboardViewModel MacroKeyboard => macroKeyboardViewModel;
    }
}
