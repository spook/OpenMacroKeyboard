﻿using OpenMacroKeyboard.BusinessLogic.Models.MacroKeyboardDisplay;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.MacroKeyboard
{
    public class ButtonViewModel : ElementViewModel
    {
        public ButtonViewModel(int index,
            ElementMetrics metrics,
            ElementVisuals visuals)
            : base(index, metrics, visuals)
        {

        }        
    }
}
