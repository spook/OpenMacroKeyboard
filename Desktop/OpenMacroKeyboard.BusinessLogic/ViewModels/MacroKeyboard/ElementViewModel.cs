﻿using OpenMacroKeyboard.BusinessLogic.Models.MacroKeyboardDisplay;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.MacroKeyboard
{
    public abstract class ElementViewModel
    {
        protected ElementViewModel(int index,
            ElementMetrics metrics,
            ElementVisuals visuals)
        {
            Index = index;
            Metrics = metrics;
            Visuals = visuals;
        }

        public int Index { get; }
        public ElementMetrics Metrics { get; }
        public ElementVisuals Visuals { get; }
    }
}
