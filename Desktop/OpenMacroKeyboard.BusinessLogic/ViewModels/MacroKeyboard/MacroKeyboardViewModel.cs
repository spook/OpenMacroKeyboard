﻿using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens;
using OpenMacroKeyboard.BusinessLogic.Models.MacroKeyboardDisplay;
using OpenMacroKeyboard.BusinessLogic.Services.DriverRepository;
using OpenMacroKeyboard.BusinessLogic.Tools;
using OpenMacroKeyboard.BusinessLogic.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.MacroKeyboard
{
    public class MacroKeyboardViewModel : BaseViewModel
    {
        private const float DEFAULT_WIDTH = 300;
        private const float DEFAULT_HEIGHT = 200;

        private const float DEFAULT_BUTTON_SIZE = 96.0f;

        private readonly ObservableCollection<ElementViewModel> elements = new();
        private readonly float buttonXSize;
        private readonly float buttonYSize;
        private IMacroKeyboard? macroKeyboard;
        private float width;
        private float height;

        private static ElementVisuals ExtractVisuals(IReadOnlyList<BaseElementEntry>? elements, BaseKeyboardElementInfo definition, int i)
        {
            string? description;
            ElementColor color;
            ElementImage? image;

            if (elements != null && i < elements.Count)
            {
                var buttonEntry = elements[i];

                description = buttonEntry.Description;
                color = buttonEntry.Color;
                image = buttonEntry.Image;
            }
            else
            {
                description = null;
                color = ElementColor.Zero;
                image = null;
            }

            return new ElementVisuals(description,
                definition.CanSetDescription,
                color,
                definition.CanSetColor,
                image,
                definition.CanSetImage);
        }

        private void SetMacroKeyboard(IMacroKeyboard? value)
        {
            if (macroKeyboard != null)
            {
                macroKeyboard.ActiveScreenChanged -= HandleActiveScreenChanged;
                macroKeyboard.Disposing -= HandleMacroKeyboardDisposing;
                elements.Clear();
                Width = DEFAULT_WIDTH;
                Height = DEFAULT_HEIGHT;
            }

            macroKeyboard = value;

            if (macroKeyboard != null)
            {
                macroKeyboard.ActiveScreenChanged += HandleActiveScreenChanged;
                macroKeyboard.Disposing += HandleMacroKeyboardDisposing;
                BuildElements();
            }

            OnPropertyChanged(nameof(MacroKeyboard));
        }

        private void HandleActiveScreenChanged(object? sender, EventArgs e)
        {
            if (macroKeyboard == null)
                return;

            BuildElements();
        }

        private void BuildElements()
        {
            elements.Clear();
            Width = DEFAULT_WIDTH;
            Height = DEFAULT_HEIGHT;

            if (macroKeyboard == null)
                return;

            var metadata = macroKeyboard.DriverMetadata;
            var screen = macroKeyboard.ActiveScreen;

            if (metadata.ButtonDefinitions.Count > 0 || metadata.EncoderDefinitions.Count > 0)
            {
                // Collecting information about extremes of the keyboard

                var keyboardMetrics = KeyboardGeometry.EvalKeyboardMetrics(metadata, buttonYSize, buttonXSize);

                // Generate button viewmodels

                for (int i = 0; i < metadata.ButtonDefinitions.Count; i++)
                {
                    var definition = metadata.ButtonDefinitions[i];
                    var visuals = ExtractVisuals(screen?.Buttons, definition, i);
                    var metrics = KeyboardGeometry.EvalElementMetrics(keyboardMetrics, definition);

                    elements.Add(new ButtonViewModel(i, metrics, visuals));
                }

                // Generate encoder viewmodels

                for (int i = 0; i < metadata.EncoderDefinitions.Count; i++)
                {
                    var definition = metadata.EncoderDefinitions[i];
                    var visuals = ExtractVisuals(screen?.Encoders, definition, i);
                    var metrics = KeyboardGeometry.EvalElementMetrics(keyboardMetrics, definition);

                    elements.Add(new EncoderViewModel(i, metrics, visuals));
                }

                // Evaluate preview width and height

                Width = keyboardMetrics.Width;
                Height = keyboardMetrics.Height;
            }
            else
            {
                // Fallback when there are no keyboard items

                Width = DEFAULT_WIDTH;
                Height = DEFAULT_HEIGHT;
            }
        }

        private void HandleMacroKeyboardDisposing(object? sender, EventArgs e)
        {
            if (macroKeyboard != null)
            {
                macroKeyboard.ActiveScreenChanged -= HandleActiveScreenChanged;
                macroKeyboard.Disposing -= HandleMacroKeyboardDisposing;
                macroKeyboard = null;
            }

            elements.Clear();
            Width = DEFAULT_WIDTH;
            Height = DEFAULT_HEIGHT;

            OnPropertyChanged(nameof(MacroKeyboard));
        }

        // Public methods -----------------------------------------------------

        public MacroKeyboardViewModel(float buttonYSize = DEFAULT_BUTTON_SIZE, float buttonXSize = DEFAULT_BUTTON_SIZE)
        {
            this.buttonYSize = buttonYSize;
            this.buttonXSize = buttonXSize;
            Width = DEFAULT_WIDTH;
            Height = DEFAULT_HEIGHT;
        }

        // Public properties --------------------------------------------------

        public string? Display => macroKeyboard?.Display;

        public ObservableCollection<ElementViewModel> Elements => elements;

        public float Width
        {
            get => width;
            set => Set(ref width, value);
        }

        public float Height
        {
            get => height;
            set => Set(ref height, value);
        }

        public IMacroKeyboard? MacroKeyboard
        {
            get => macroKeyboard;
            set => SetMacroKeyboard(value);
        }
    }
}
