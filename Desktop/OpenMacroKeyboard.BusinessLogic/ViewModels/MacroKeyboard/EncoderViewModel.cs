﻿using OpenMacroKeyboard.BusinessLogic.Models.MacroKeyboardDisplay;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.MacroKeyboard
{
    public class EncoderViewModel : ElementViewModel         
    {
        public EncoderViewModel(int index,
            ElementMetrics metrics,
            ElementVisuals visuals)
            : base(index, metrics, visuals)
        {

        }        
    }
}
