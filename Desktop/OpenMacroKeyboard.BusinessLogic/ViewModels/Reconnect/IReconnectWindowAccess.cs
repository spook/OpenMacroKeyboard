﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.Reconnect
{
    public interface IReconnectWindowAccess
    {
        void Close(bool result);
    }
}
