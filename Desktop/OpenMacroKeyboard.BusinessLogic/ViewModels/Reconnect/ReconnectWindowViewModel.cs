﻿using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.BusinessLogic.ViewModels.AddMacroKeyboard;
using OpenMacroKeyboard.BusinessLogic.ViewModels.Base;
using Spooksoft.VisualStateManager.Commands;
using Spooksoft.VisualStateManager.Conditions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.Reconnect
{
    public class ReconnectWindowViewModel : BaseViewModel
    {
        private readonly DriverFactoryViewModel driverFactory;
        private readonly IReconnectWindowAccess access;

        private Dictionary<string, object> BuildResult()
        {
            Dictionary<string, object> settings = new();

            foreach (var setting in driverFactory.Settings)
                settings[setting.Key] = setting.ToSettingValue();

            return settings;
        }

        private void DoOk()
        {
            access.Close(true);
        }

        private void DoCancel()
        {
            access.Close(false);
        }

        public ReconnectWindowViewModel(IReconnectWindowAccess access, DriverMetadata driverMetadata, IReadOnlyDictionary<string, object> currentSettings)
        {
            this.access = access;
            driverFactory = new DriverFactoryViewModel(driverMetadata, currentSettings);

            var settingsValidatedCondition = Condition.Lambda(this, vm => vm.DriverFactory.SettingsValid.Value, false);

            OkCommand = new AppCommand(obj => DoOk(), settingsValidatedCondition);
            CancelCommand = new AppCommand(obj => DoCancel());            
        }

        public DriverFactoryViewModel DriverFactory => driverFactory;

        public ICommand OkCommand { get; }
        public ICommand CancelCommand { get; }

        public IReadOnlyDictionary<string, object> Result => BuildResult();
    }
}
