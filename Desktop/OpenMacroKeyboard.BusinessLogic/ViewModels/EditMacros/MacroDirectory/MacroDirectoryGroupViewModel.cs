﻿using OpenMacroKeyboard.BusinessLogic.ViewModels.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroDirectory
{
    public class MacroDirectoryGroupViewModel : BaseViewModel
    {
        private bool isExpanded;

        public MacroDirectoryGroupViewModel(string display, IReadOnlyList<MacroDirectoryEntryViewModel> entries) 
        {
            Display = display;
            Entries = entries.ToList();

            isExpanded = true;
        }

        public string Display { get; }
        public IReadOnlyList<MacroDirectoryEntryViewModel> Entries { get; }

        public bool IsExpanded
        {
            get => isExpanded;
            set => Set(ref isExpanded, value);
        }
    }
}
