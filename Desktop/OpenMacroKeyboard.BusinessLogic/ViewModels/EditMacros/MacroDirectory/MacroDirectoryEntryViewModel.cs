﻿using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.BusinessLogic.Attributes;
using OpenMacroKeyboard.BusinessLogic.Services.ActionRepository;
using OpenMacroKeyboard.BusinessLogic.Types;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroEditors;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroKeyboard;
using OpenMacroKeyboard.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroDirectory
{
    public class MacroDirectoryEntryViewModel
    {
        public MacroDirectoryEntryViewModel(IActionRepositoryService actionRepository, MacroType macroType)
        {
            ActionGuid = null;
            Type = macroType;

            var rm = new ResourceManager(typeof(OpenMacroKeyboard.Resources.Macros.Strings));

            var displayKeyAttribute = macroType.GetAttribute<DisplayResourceKeyAttribute>() ?? 
                throw new InvalidOperationException($"Macro type {macroType} is missing {nameof(DisplayResourceKeyAttribute)} attribute!");

            string? display = rm.GetString(displayKeyAttribute.NameKey) ??
                throw new InvalidOperationException($"Display string resource {displayKeyAttribute.NameKey} for macro type {macroType} is null!");

            string? group = rm.GetString(displayKeyAttribute.GroupKey) ?? 
                throw new InvalidOperationException($"Group string resource {displayKeyAttribute.GroupKey} for macro type {macroType} is null!");

            Display = display;
            Group = group;
        }

        public MacroDirectoryEntryViewModel(IActionRepositoryService actionRepository, Guid actionGuid)
        {
            ActionGuid = actionGuid;
            Type = MacroType.Action;

            var metadata = actionRepository.GetActionFactoryMetadata(actionGuid);
            
            Display = metadata.ActionName;
            Group = metadata.Group;
        }

        public BaseMacroEditorViewModel ToNewMacroEditor(IMacroEditorHandler handler)
        {
            return Type switch
            {
                MacroType.Folder => FolderMacroEditorViewModel.CreateForSubScreen(handler),
                MacroType.Back => throw new InvalidOperationException("Back macro shouldn't be added manually!"),
                MacroType.Multi => new MultiMacroEditorViewModel(handler),
                MacroType.Action => new ActionMacroEditorViewModel(handler, ActionGuid!.Value),
                MacroType.Pause => new PauseMacroEditorViewModel(handler),
                MacroType.Sleep => new SleepMacroEditorViewModel(handler),
                _ => throw new InvalidOperationException("Unsupported macro editor!")
            };
        }

        public Guid? ActionGuid { get; }
        public MacroType Type { get; }
        public string Display { get; }
        public string Group { get; }
    }
}
