﻿using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.BusinessLogic.Services.ActionRepository;
using OpenMacroKeyboard.BusinessLogic.Services.Dialogs;
using OpenMacroKeyboard.BusinessLogic.Services.DriverRepository;
using OpenMacroKeyboard.BusinessLogic.Services.Messaging;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroKeyboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros
{
    public interface IMacroEditorHandler
    {
        void OpenScreen(ScreenEditorViewModel subScreen);
        void ReturnToParentScreen();        

        IDriverRepositoryService DriverRepository { get; }
        IActionRepositoryService ActionRepository { get; }
        IDialogService DialogService { get; }
        IMessagingService MessagingService { get; }
        DriverMetadata DriverMetadata { get; }
    }
}
