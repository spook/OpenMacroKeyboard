﻿using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroEditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroKeyboard
{
    public class EncoderContentViewModel : BaseElementContentViewModel
    {
        private readonly EncoderInfo encoderInfo;

        private readonly List<MacroEditorSlotViewModel> macroSlots;
        private MacroEditorSlotViewModel selectedMacroSlot;

        public EncoderContentViewModel(EncoderEntry? existingEntry, EncoderInfo encoderInfo, IMacroEditorHandler handler)
            : base(existingEntry)
        {
            DecreaseMacro = new MacroEditorSlotViewModel(Resources.Controls.MacroEditors.Strings.Label_OnDecrease, handler, existingEntry?.DecMacro);
            IncreaseMacro = new MacroEditorSlotViewModel(Resources.Controls.MacroEditors.Strings.Label_OnIncrease, handler, existingEntry?.IncMacro);
            PressMacro = new MacroEditorSlotViewModel(Resources.Controls.MacroEditors.Strings.Label_OnPress, handler, existingEntry?.PressMacro);

            if (encoderInfo.HasButton)
                macroSlots = new() { IncreaseMacro, DecreaseMacro, PressMacro };
            else
                macroSlots = new() { IncreaseMacro, DecreaseMacro };

            selectedMacroSlot = DecreaseMacro;
            this.encoderInfo = encoderInfo;
        }

        public MacroEditorSlotViewModel DecreaseMacro { get; }
        public MacroEditorSlotViewModel IncreaseMacro { get; }
        public MacroEditorSlotViewModel PressMacro { get; }

        public override IList<MacroEditorSlotViewModel> MacroSlots => macroSlots;

        public override MacroEditorSlotViewModel SelectedMacroSlot
        {
            get => selectedMacroSlot;
            set => Set(ref selectedMacroSlot, value);
        }
    }
}
