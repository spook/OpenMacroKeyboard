﻿using System;
using System.Collections.Generic;
using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens;
using OpenMacroKeyboard.BusinessLogic.Models.MacroKeyboardDisplay;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroDirectory;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroEditors;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroKeyboard
{
    public class EncoderEditorViewModel : BaseTypedElementEditorViewModel<EncoderContentViewModel>
    {
        private readonly EncoderInfo encoderInfo;

        public EncoderEditorViewModel(int index,
            IMacroEditorHandler handler,            
            IElementHandler elementHandler,
            ElementMetrics metrics,
            EncoderInfo encoderInfo,
            EncoderEntry? existingEntry)
            : base(index, handler, elementHandler, metrics, encoderInfo)
        {
            this.encoderInfo = encoderInfo;

            Content = new EncoderContentViewModel(existingEntry, encoderInfo, handler);
        }

        public override void NotifyDoubleClicked()
        {
            var encoderContent = Content as EncoderContentViewModel;

            if (encoderContent != null)
            {
                if (encoderContent.PressMacro != null)
                {
                    if (encoderContent.PressMacro.Editor is FolderMacroEditorViewModel folderMacro)
                    {
                        handler.OpenScreen(folderMacro.Screen);
                    }
                    else if (encoderContent.PressMacro.Editor is BackMacroEditorViewModel backMacro)
                    {
                        handler.ReturnToParentScreen();
                    }
                }
            }
        }

        public override void RequestSetMacroFromPreview(MacroDirectoryEntryViewModel macroDirectoryEntry)
        {
            base.RequestSetMacroFromPreview(macroDirectoryEntry);

            var encoderContent = Content as EncoderContentViewModel;
            encoderContent?.PressMacro.RequestReplaceMacro(macroDirectoryEntry);
        }

        public override string EditorHeader => String.Format(OpenMacroKeyboard.Resources.Macros.Strings.Header_Encoder, Index);
    }
}