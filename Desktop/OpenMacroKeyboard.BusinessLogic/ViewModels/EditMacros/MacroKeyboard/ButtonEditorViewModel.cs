﻿using System;
using System.Collections.Generic;
using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens;
using OpenMacroKeyboard.BusinessLogic.Models.MacroKeyboardDisplay;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroDirectory;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroEditors;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroKeyboard
{
    public class ButtonEditorViewModel : BaseTypedElementEditorViewModel<ButtonContentViewModel>
    {
        private readonly ButtonInfo buttonInfo;        

        public ButtonEditorViewModel(int index,
            IMacroEditorHandler handler,            
            IElementHandler elementHandler,
            ElementMetrics metrics,
            ButtonInfo buttonInfo,
            ButtonEntry? existingEntry)
            : base(index, handler, elementHandler, metrics, buttonInfo)
        {
            this.buttonInfo = buttonInfo;

            Content = new ButtonContentViewModel(existingEntry, buttonInfo, handler);            
        }

        public override void NotifyDoubleClicked()
        {
            var buttonContent = Content as ButtonContentViewModel;

            if (buttonContent != null)
            {
                if (buttonContent.PressMacro != null)
                {
                    if (buttonContent.PressMacro.Editor is FolderMacroEditorViewModel folderMacro)
                    {
                        handler.OpenScreen(folderMacro.Screen);
                    }
                    else if (buttonContent.PressMacro.Editor is BackMacroEditorViewModel backMacro)
                    {
                        handler.ReturnToParentScreen();
                    }
                }
            }
        }

        public override void RequestSetMacroFromPreview(MacroDirectoryEntryViewModel macroDirectoryEntry)
        {
            base.RequestSetMacroFromPreview(macroDirectoryEntry);

            var buttonContent = Content as ButtonContentViewModel;
            buttonContent?.PressMacro.RequestReplaceMacro(macroDirectoryEntry);
        }

        public override string EditorHeader => String.Format(OpenMacroKeyboard.Resources.Macros.Strings.Header_Button, Index);
    }
}