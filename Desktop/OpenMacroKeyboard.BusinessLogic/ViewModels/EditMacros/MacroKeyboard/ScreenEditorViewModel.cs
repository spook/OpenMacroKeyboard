﻿using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens;
using OpenMacroKeyboard.BusinessLogic.Tools;
using OpenMacroKeyboard.BusinessLogic.ViewModels.Base;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroEditors;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroKeyboard
{
    public class ScreenEditorViewModel : BaseViewModel, IElementHandler
    {
        private readonly List<BaseElementEditorViewModel> elements;
        private readonly IMacroEditorHandler handler;        
        private BaseElementEditorViewModel? selectedElement;

        void IElementHandler.SelectElement(BaseElementEditorViewModel element)
        {
            SelectedElement = element;
        }

        public ScreenEditorViewModel(IMacroEditorHandler handler, Screen? screen = null)
        {
            this.handler = handler;            

            elements = new();

            // Create buttons and encoders

            var keyboardMetrics = KeyboardGeometry.EvalKeyboardMetrics(handler.DriverMetadata, 100.0f);

            for (int i = 0; i < handler.DriverMetadata.ButtonDefinitions.Count; i++)
            {
                var metrics = KeyboardGeometry.EvalElementMetrics(keyboardMetrics, handler.DriverMetadata.ButtonDefinitions[i]);

                ButtonEntry? existingEntry = null;
                if (screen != null && i < screen.Buttons.Count)
                    existingEntry = screen.Buttons[i];

                var buttonEditor = new ButtonEditorViewModel(i, handler, this, metrics, handler.DriverMetadata.ButtonDefinitions[i], existingEntry);
                elements.Add(buttonEditor);
            }

            for (int i = 0; i < handler.DriverMetadata.EncoderDefinitions.Count; i++)
            {
                var metrics = KeyboardGeometry.EvalElementMetrics(keyboardMetrics, handler.DriverMetadata.EncoderDefinitions[i]);

                EncoderEntry? existingEntry = null;
                if (screen != null && i < screen.Encoders.Count)
                    existingEntry = screen.Encoders[i];

                var encoderEditor = new EncoderEditorViewModel(i, handler, this, metrics, handler.DriverMetadata.EncoderDefinitions[i], existingEntry);
                elements.Add(encoderEditor);
            }

            Width = keyboardMetrics.Width;
            Height = keyboardMetrics.Height;
        }

        public IReadOnlyList<BaseElementEditorViewModel> Elements => elements;
        
        public Screen ToScreen()
        {
            List<ButtonEntry> buttonEntries = new();

            for (int i = 0; i < handler.DriverMetadata.ButtonDefinitions.Count; i++)
            {
                var buttonEditor = elements.OfType<ButtonEditorViewModel>().FirstOrDefault(be => be.Index == i);
                var content = buttonEditor?.Content as ButtonContentViewModel;

                var entry = new ButtonEntry(content?.Description ?? string.Empty,
                    content?.Color ?? ElementColor.Zero,
                    content?.Image,
                    content?.PressMacro?.ToMacro());

                buttonEntries.Add(entry);
            }

            List<EncoderEntry> encoderEntries = new();
            for (int i = 0; i < handler.DriverMetadata.EncoderDefinitions.Count; i++)
            {
                var encoderEditor = elements.OfType<EncoderEditorViewModel>().FirstOrDefault(ee => ee.Index == i);
                var content = encoderEditor?.Content as EncoderContentViewModel;

                var entry = new EncoderEntry(content?.Description ?? string.Empty,
                    content?.Color ?? ElementColor.Zero,
                    content?.Image,
                    content?.IncreaseMacro?.ToMacro(),
                    content?.DecreaseMacro?.ToMacro(),
                    content?.PressMacro?.ToMacro());

                encoderEntries.Add(entry);
            }

            return new Screen(buttonEntries, encoderEntries);
        }

        public BaseElementEditorViewModel? SelectedElement
        {
            get => selectedElement;
            set => Set(ref selectedElement, value);
        }

        public float Width { get; }
        public float Height { get; }
    }
}
