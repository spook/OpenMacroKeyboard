﻿using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroEditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroKeyboard
{
    public class ButtonContentViewModel : BaseElementContentViewModel
    {
        private readonly ButtonInfo buttonInfo;

        private readonly List<MacroEditorSlotViewModel> macroSlots;
        private MacroEditorSlotViewModel selectedMacroSlot;

        public ButtonContentViewModel(ButtonEntry? existingEntry, ButtonInfo buttonInfo, IMacroEditorHandler editorHandler)
            : base(existingEntry)
        {
            this.buttonInfo = buttonInfo;

            PressMacro = new MacroEditorSlotViewModel(OpenMacroKeyboard.Resources.Controls.MacroEditors.Strings.Label_OnPress, editorHandler, existingEntry?.Macro);
            macroSlots = new() { PressMacro };
            selectedMacroSlot = PressMacro;
        }        

        public MacroEditorSlotViewModel PressMacro { get; }

        public override IList<MacroEditorSlotViewModel> MacroSlots => macroSlots;

        public override MacroEditorSlotViewModel SelectedMacroSlot
        {
            get => selectedMacroSlot;
            set => Set(ref selectedMacroSlot, value);
        }

    }
}
