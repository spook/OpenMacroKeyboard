﻿using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens;
using OpenMacroKeyboard.BusinessLogic.Models.MacroKeyboardDisplay;
using OpenMacroKeyboard.BusinessLogic.Services.Dialogs;
using OpenMacroKeyboard.BusinessLogic.Services.Messaging;
using OpenMacroKeyboard.BusinessLogic.ViewModels.Base;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroDirectory;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroEditors;
using OpenMacroKeyboard.Common.Tools;
using OpenMacroKeyboard.Resources;
using Spooksoft.VisualStateManager.Commands;
using Spooksoft.VisualStateManager.Conditions;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Input;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroKeyboard
{
    public abstract class BaseElementEditorViewModel : BaseViewModel
    {
        private readonly ElementMetrics metrics;
        private readonly BaseKeyboardElementInfo info;

        private void DoClearImage()
        {
            if (Content == null)
                throw new InvalidOperationException("Cannot change image - content is null!");

            Content.Image = null;
        }

        private void DoChooseImage()
        {
            if (Content == null)
                throw new InvalidOperationException("Cannot change image - content is null!");

            (bool result, string? path) = handler.DialogService.ShowOpenDialog(OpenMacroKeyboard.Resources.Common.Strings.ImageFilter);

            if (result && path != null)
            {
                try
                {
                    using Bitmap image = new(path);
                    (byte[] argb, int width, int height) = BitmapTools.ArgbFromBitmap(image);
                    Content.Image = new ElementImage(argb, width, height);
                }
                catch
                {
                    handler.MessagingService.ShowError(OpenMacroKeyboard.Resources.Windows.ElementEditor.Strings.Error_CannotOpenImageFile);
                }
            }
        }

        protected readonly IMacroEditorHandler handler;
        protected readonly IElementHandler elementHandler;

        public void RequestExchangeContent(BaseElementEditorViewModel draggedVM)
        {
            ArgumentNullException.ThrowIfNull(draggedVM);

            if (this.GetType() != draggedVM.GetType())
                throw new InvalidOperationException("You can exchange content only between elements of the same type!");

            var temp = this.Content;
            this.Content = draggedVM.Content;
            draggedVM.Content = temp;
        }

        public virtual void NotifyDoubleClicked()
        {
            
        }

        public virtual void RequestSetMacroFromPreview(MacroDirectoryEntryViewModel macroDirectoryEntry)
        {
            elementHandler.SelectElement(this);
        }

        public BaseElementEditorViewModel(int index,
            IMacroEditorHandler handler,            
            IElementHandler elementHandler,
            ElementMetrics metrics,
            BaseKeyboardElementInfo info)
        {
            this.Index = index;
            this.metrics = metrics;
            this.info = info;
            this.handler = handler;
            this.elementHandler = elementHandler;

            var canSetImageCondition = Condition.Simple(Info.CanSetImage);
            var contentIsPresentCondition = Condition.Lambda(this, vm => vm.Content != null, false);
            var canChangeImageCondition = canSetImageCondition & contentIsPresentCondition;

            ClearImageCommand = new AppCommand(obj => DoClearImage(), canChangeImageCondition);
            ChooseImageCommand = new AppCommand(obj => DoChooseImage(), canChangeImageCondition);
        }

        public ElementMetrics Metrics => metrics;

        public BaseKeyboardElementInfo Info => info;

        public abstract BaseElementContentViewModel? Content { get; set; }

        public int Index { get; }

        public abstract string EditorHeader { get; }        

        public ICommand ClearImageCommand { get; }

        public ICommand ChooseImageCommand { get; }
    }
}