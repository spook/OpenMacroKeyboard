﻿using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens.Macros;
using OpenMacroKeyboard.BusinessLogic.Types;
using OpenMacroKeyboard.BusinessLogic.ViewModels.Base;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroDirectory;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroEditors;
using Spooksoft.VisualStateManager.Commands;
using Spooksoft.VisualStateManager.Conditions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroKeyboard
{
    public class MacroEditorSlotViewModel : BaseViewModel
    {
        private BaseMacroEditorViewModel? editor;
        private IMacroEditorHandler handler;

        private void DoClearMacro()
        {
            if (editor is BackMacroEditorViewModel)
            {
                handler.MessagingService.Warn(Resources.Controls.Strings.Message_CannotDeleteBackMacro);
                return;
            }
            
            if (editor is FolderMacroEditorViewModel &&
                handler.MessagingService.AskYesNo(Resources.Controls.Strings.Message_ReplacingFolderQuestion) == false)
                return;

            Editor = null;
        }

        public MacroEditorSlotViewModel(string label, IMacroEditorHandler handler, BaseMacro? existingMacro = null)
        {
            this.handler = handler;
            Label = label;

            var canClearMacro = Condition.Lambda(this, vm => vm.Editor != null && !(editor is BackMacroEditorViewModel), false);
            ClearMacroCommand = new AppCommand(obj => DoClearMacro());

            // Load existing macro

            if (existingMacro != null)
            {
                Editor = BaseMacroEditorViewModel.FromMacro(existingMacro, handler);
            }
        }

        public void RequestReplaceMacro(MacroDirectoryEntryViewModel availableMacroViewModel)
        {
            // Sanity check
            if (editor is FolderMacroEditorViewModel && 
                handler.MessagingService.AskYesNo(Resources.Controls.Strings.Message_ReplacingFolderQuestion) == false)
                return;

            if (editor is BackMacroEditorViewModel)
                throw new InvalidOperationException("Replacing a back macro should not be possible!");

            Editor = availableMacroViewModel.ToNewMacroEditor(handler);
        }

        public BaseMacro? ToMacro()
        {
            return Editor?.ToMacro();
        }

        public bool CanReplaceMacro => editor == null || editor is not BackMacroEditorViewModel;

        public string Label { get; }

        public ICommand ClearMacroCommand { get; }

        public BaseMacroEditorViewModel? Editor
        {
            get => editor;
            private set => Set(ref editor, value);
        }
    }
}
