﻿using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens;
using OpenMacroKeyboard.BusinessLogic.Models.MacroKeyboardDisplay;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroEditors;
using Spooksoft.VisualStateManager.Commands;
using Spooksoft.VisualStateManager.Conditions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroKeyboard
{
    public abstract class BaseTypedElementEditorViewModel<TContent> : BaseElementEditorViewModel
        where TContent : BaseElementContentViewModel
    {
        private TContent? content;

        private void SetContent(TContent? value)
        {
            this.content = value;                        

            if (content != null)
            {
                if (!Info.CanSetImage)
                    content.Image = null;
                if (!Info.CanSetColor)
                    content.Color = ElementColor.Zero;
                if (!Info.CanSetDescription)
                    content.Description = null;
            }

            OnPropertyChanged(nameof(Content));
        }

        public BaseTypedElementEditorViewModel(int index,
            IMacroEditorHandler handler,        
            IElementHandler elementHandler,
            ElementMetrics metrics,
            BaseKeyboardElementInfo info)
            : base(index, handler, elementHandler, metrics, info)
        {

        }

        public override BaseElementContentViewModel? Content
        {
            get => content;
            set => SetContent((TContent?)value);
        }
    }
}
