﻿using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens;
using OpenMacroKeyboard.BusinessLogic.ViewModels.Base;
using System.Collections.Generic;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroKeyboard
{
    public abstract class BaseElementContentViewModel : BaseViewModel
    {
        private string? description;
        private ElementColor color;
        private ElementImage? image;

        private void HandleImageChanged()
        {
            OnPropertyChanged(nameof(IsImagePresent));
        }

        public BaseElementContentViewModel(BaseElementEntry? existingEntry)
        {
            description = existingEntry?.Description;
            color = existingEntry?.Color ?? ElementColor.Zero;
            image = existingEntry?.Image;
        }

        public string? Description
        {
            get => description;
            set => Set(ref description, value);
        }

        public ElementColor Color
        {
            get => color;
            set => Set(ref color, value);
        }

        public ElementImage? Image
        {
            get => image;
            set => Set(ref image, value, changeHandler: HandleImageChanged);
        }

        public bool IsImagePresent => image != null;

        public abstract IList<MacroEditorSlotViewModel> MacroSlots { get; }

        public abstract MacroEditorSlotViewModel SelectedMacroSlot { get; set; }
    }
}
