﻿using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens.Macros;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroDirectory;
using Spooksoft.VisualStateManager.Commands;
using Spooksoft.VisualStateManager.Conditions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroEditors
{
    public class MultiMacroEditorViewModel : BaseMacroEditorViewModel
    {
        // Private fields -----------------------------------------------------

        private readonly ObservableCollection<BaseMacroEditorViewModel> editors = new();
        private BaseMacroEditorViewModel? selectedEditor;
        private int selectedEditorIndex;

        // Private methods ----------------------------------------------------

        private void DoRemoveEditor()
        {
            if (selectedEditor != null)
                editors.Remove(selectedEditor);

            SelectedEditor = null;
        }

        private void DoMoveEditorUp()
        {
            if (selectedEditor != null)
            {
                int index = editors.IndexOf(selectedEditor);
                if (index > 0 && index < editors.Count)
                {
                    editors.Move(index, index - 1);
                }

                selectedEditorIndex = index - 1;
            }
        }        

        private void DoMoveEditorDown()
        {
            if (selectedEditor != null)
            {
                int index = editors.IndexOf(selectedEditor);
                if (index >= 0 && index < editors.Count - 1)
                {
                    editors.Move(index, index + 1);
                }

                selectedEditorIndex = index + 1;
            }
        }

        // Public methods -----------------------------------------------------

        public MultiMacroEditorViewModel(IMacroEditorHandler handler, IReadOnlyList<BaseMacro>? subMacros = null) 
            : base(handler)
        {
            selectedEditorIndex = -1;

            var editorSelectedCondition = Condition.Lambda(this, vm => vm.SelectedEditor != null, false);
            var firstEditorSelectedCondition = Condition.Lambda(this, vm => vm.SelectedEditorIndex == 0, false);
            var lastEditorSelectedCondition = Condition.Lambda(this, vm => vm.SelectedEditorIndex == Editors.Count - 1, false);

            RemoveEditorCommand = new AppCommand(obj => DoRemoveEditor(), editorSelectedCondition);
            MoveEditorUpCommand = new AppCommand(obj => DoMoveEditorUp(), editorSelectedCondition & !firstEditorSelectedCondition);
            MoveEditorDownCommand = new AppCommand(obj => DoMoveEditorDown(), editorSelectedCondition & !lastEditorSelectedCondition);

            if (subMacros != null)
            {
                foreach (var subMacro in subMacros)
                {
                    Editors.Add(FromMacro(subMacro, handler));
                }
            }
        }

        public void RequestAddMacro(MacroDirectoryEntryViewModel availableMacroViewModel)
        {
            var editor = availableMacroViewModel.Type switch
            {
                Types.MacroType.Back => throw new InvalidOperationException("Adding a back macro to multi macro list should not be possible!"),
                Types.MacroType.Folder => throw new InvalidOperationException("Adding a folder macro to multi macro list should not be possible!"),
                Types.MacroType.Multi => throw new InvalidOperationException("Adding a multi macro to multi macro list should not be possible!"),
                Types.MacroType.Pause or
                Types.MacroType.Sleep or
                Types.MacroType.Action => availableMacroViewModel.ToNewMacroEditor(handler),
                _ => throw new InvalidOperationException("Unsupported macro type!")
            };

            editors.Add(editor);
            SelectedEditor = editor;
        }

        public bool CanAddMacro(MacroDirectoryEntryViewModel availableMacroViewModel)
        {
            return availableMacroViewModel.Type is Types.MacroType.Action or Types.MacroType.Pause or Types.MacroType.Sleep;
        }

        public override BaseMacro ToMacro()
        {
            List<BaseMacro> subMacros = new();

            for (int i = 0; i < Editors.Count; i++)
                subMacros.Add(Editors[i].ToMacro());

            return new MultiMacro(subMacros);
        }

        // Public properties --------------------------------------------------

        public override string Display => Resources.Macros.Strings.Macro_Multi;

        public ObservableCollection<BaseMacroEditorViewModel> Editors => editors;

        public BaseMacroEditorViewModel? SelectedEditor
        {
            get => selectedEditor;
            set => Set(ref selectedEditor, value);
        }

        public ICommand MoveEditorUpCommand { get; }

        public ICommand MoveEditorDownCommand { get; }

        public ICommand RemoveEditorCommand { get; }
        
        public int SelectedEditorIndex
        {
            get => selectedEditorIndex;
            set => Set(ref selectedEditorIndex, value);
        }
    }
}
