﻿using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens.Macros;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroEditors
{
    public class SleepMacroEditorViewModel : BaseMacroEditorViewModel
    {
        public SleepMacroEditorViewModel(IMacroEditorHandler handler)
            : base(handler)
        {

        }

        public override BaseMacro ToMacro()
        {
            return new SleepMacro();
        }

        public override string Display => Resources.Macros.Strings.Macro_Sleep;
    }
}
