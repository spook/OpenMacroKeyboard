﻿using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens.Macros;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroEditors
{
    public class BackMacroEditorViewModel : BaseMacroEditorViewModel
    {
        public BackMacroEditorViewModel(IMacroEditorHandler handler)
            : base(handler)
        {

        }

        public override BaseMacro ToMacro()
        {
            return new BackMacro();
        }

        public override string Display => Resources.Macros.Strings.Macro_Back;
    }
}
