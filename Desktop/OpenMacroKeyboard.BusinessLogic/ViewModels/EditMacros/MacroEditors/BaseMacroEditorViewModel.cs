﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens.Macros;
using OpenMacroKeyboard.BusinessLogic.ViewModels.Base;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroEditors;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroEditors
{
    public abstract class BaseMacroEditorViewModel : BaseViewModel
    {
        protected readonly IMacroEditorHandler handler;

        public BaseMacroEditorViewModel(IMacroEditorHandler handler)
        {
            this.handler = handler;
        }

        public static BaseMacroEditorViewModel FromMacro(BaseMacro macro, IMacroEditorHandler handler)
        {
            switch (macro)
            {
                case BackMacro:
                    {
                        return new BackMacroEditorViewModel(handler);
                    }
                case FolderMacro folderMacro:
                    {
                        return new FolderMacroEditorViewModel(handler, folderMacro.Screen);
                    }
                case MultiMacro multiMacro:
                    {
                        return new MultiMacroEditorViewModel(handler, multiMacro.SubMacros);
                    }
                case ActionMacro actionMacro:
                    {
                        if (handler.ActionRepository.ActionFactoryGuids.Contains(actionMacro.ActionGuid))
                            return new ActionMacroEditorViewModel(handler, actionMacro.ActionGuid, actionMacro.Settings);
                        else
                            return new MissingPluginEditorViewModel(handler, actionMacro.ActionGuid, actionMacro.Settings);
                    }
                case PauseMacro pauseMacro:
                    {
                        return new PauseMacroEditorViewModel(handler, pauseMacro.DelayMs);
                    }
                case SleepMacro:
                    {
                        return new SleepMacroEditorViewModel(handler);
                    }
                default:
                    throw new InvalidOperationException("Unsupported macro type!");
            }
        }

        public abstract BaseMacro ToMacro();

        public abstract string Display { get; }
    }
}
