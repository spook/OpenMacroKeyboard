﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroEditors
{
    public class SettingEntryViewModel
    {
        public SettingEntryViewModel(string key, object value)
        {
            Key = key;
            Value = value;
        }

        public string Key { get; } 
        public object Value { get; }
    }
}
