﻿using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens.Macros;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroEditors
{
    /// <remarks>
    /// This editor exists to limit damage caused by (e.g.) forgetting
    /// to install specific plugin. Instead of blindly cleaning up the
    /// entry, guid and settings are preserved, so that user have a
    /// chance to re-install plugin. That's why <see cref="ToMacro"/>
    /// returns instance of <see cref="ActionMacro"/>.
    /// </remarks>
    public class MissingPluginEditorViewModel : BaseMacroEditorViewModel
    {
        private readonly Guid actionGuid;
        private readonly Dictionary<string, object> settings;

        public MissingPluginEditorViewModel(IMacroEditorHandler handler, Guid guid, Dictionary<string, object> settings) 
            : base(handler)
        {
            actionGuid = guid;
            this.settings = settings;
        }

        public override BaseMacro ToMacro()
        {
            return new ActionMacro(actionGuid, settings);
        }

        public override string Display => OpenMacroKeyboard.Resources.Macros.Strings.Macro_MissingPlugin;

        public IReadOnlyList<SettingEntryViewModel> Settings => settings.Select(kvp => new SettingEntryViewModel(kvp.Key, kvp.Value)).ToList();
    }
}
