﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenMacroKeyboard.API.V1.Actions.Models;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens.Macros;
using OpenMacroKeyboard.BusinessLogic.ViewModels.Settings;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroEditors
{
    public class ActionMacroEditorViewModel : BaseMacroEditorViewModel
    {
        private readonly ActionMetadata actionMetadata;
        private readonly List<BaseSettingViewModel> settings;

        public ActionMacroEditorViewModel(IMacroEditorHandler handler, Guid actionGuid, Dictionary<string, object>? settings = null)
            : base(handler)
        {
            if (!handler.ActionRepository.ActionFactoryGuids.Contains(actionGuid))
                throw new InvalidOperationException("Action GUID is not present within loaded plugins! MissingPluginEditorViewModel should have been constructed instead.");

            actionMetadata = handler.ActionRepository.GetActionFactoryMetadata(actionGuid);

            this.settings = new List<BaseSettingViewModel>();

            foreach (var setting in actionMetadata.Settings)
            {
                object? value = null;
                settings?.TryGetValue(setting.Key, out value);

                var settingViewModel = BaseSettingViewModel.FromModel(setting, value);
                this.settings.Add(settingViewModel);
            }
        }

        public override BaseMacro ToMacro()
        {
            Dictionary<string, object> settings = new();
            foreach (var setting in this.settings)
            {
                settings[setting.Key] = setting.ToSettingValue();
            }

            return new ActionMacro(actionMetadata.Guid, settings);
        }

        public override string Display => actionMetadata.ActionName;

        public IReadOnlyList<BaseSettingViewModel> Settings => settings;
    }
}
