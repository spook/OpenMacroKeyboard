﻿using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens.Macros;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroKeyboard;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroEditors
{
    public class FolderMacroEditorViewModel : BaseMacroEditorViewModel
    {
        public FolderMacroEditorViewModel(IMacroEditorHandler handler, Screen? screen = null)
            : base(handler)
        {
            Screen = new ScreenEditorViewModel(handler, screen);
        }

        public static FolderMacroEditorViewModel CreateForSubScreen(IMacroEditorHandler handler) 
        {
            return new FolderMacroEditorViewModel(handler, Models.Configuration.Screens.Screen.EmptySubfolder(handler.DriverMetadata));
        }

        public override BaseMacro ToMacro()
        {
            var screen = Screen.ToScreen();
            return new FolderMacro(screen);
        }

        public override string Display => OpenMacroKeyboard.Resources.Macros.Strings.Macro_Folder;

        public ScreenEditorViewModel Screen { get; }
    }
}
