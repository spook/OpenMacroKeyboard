﻿using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens.Macros;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroEditors
{
    public class PauseMacroEditorViewModel : BaseMacroEditorViewModel
    {
        private string delay;

        private void SetDelay(string value)
        {
            if (int.TryParse(value, out int result))
            {
                this.delay = (Math.Max(0, result)).ToString();
            }
            else
            {
                this.delay = 0.ToString();
            }
        }

        public PauseMacroEditorViewModel(IMacroEditorHandler handler, int? currentSetting = null) 
            : base(handler)
        {
            if (currentSetting != null)
                delay = (Math.Max(0, currentSetting.Value)).ToString();
            else
                delay = 0.ToString();
        }

        public override string Display => OpenMacroKeyboard.Resources.Macros.Strings.Macro_Pause;

        public override BaseMacro ToMacro()
        {
            return new PauseMacro(int.Parse(this.delay));
        }

        public string Delay
        {
            get => this.delay;
            set => SetDelay(value);
        }
    }
}
