﻿using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.BusinessLogic.Attributes;
using OpenMacroKeyboard.BusinessLogic.Models.Configuration.Screens;
using OpenMacroKeyboard.BusinessLogic.Services.ActionRepository;
using OpenMacroKeyboard.BusinessLogic.Services.Dialogs;
using OpenMacroKeyboard.BusinessLogic.Services.DriverRepository;
using OpenMacroKeyboard.BusinessLogic.Services.Messaging;
using OpenMacroKeyboard.BusinessLogic.Types;
using OpenMacroKeyboard.BusinessLogic.ViewModels.Base;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroDirectory;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroEditors;
using OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros.MacroKeyboard;
using OpenMacroKeyboard.Common.Helpers;
using Spooksoft.VisualStateManager.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.EditMacros
{
    public class EditMacrosWindowViewModel : BaseViewModel, IMacroEditorHandler
    {
        private readonly IEditMacrosWindowAccess access;
        private readonly IDriverRepositoryService driverRepository;
        private readonly IActionRepositoryService actionRepository;        
        private readonly IDialogService dialogService;
        private readonly IMessagingService messagingService;
        private readonly DriverMetadata driverMetadata;

        private readonly ScreenEditorViewModel rootScreen;
        private readonly Stack<ScreenEditorViewModel> openedScreens = new();

        private readonly IReadOnlyList<MacroDirectoryGroupViewModel> availableMacroGroups;

        // Private methods ----------------------------------------------------

        private Screen? BuildRootScreen()
        {
            return this.rootScreen.ToScreen();
        }

        private void DoOk()
        {
            access.Close(true);
        }

        private void DoCancel()
        {
            access.Close(false);
        }

        // IMacroEditorHandler implementation ---------------------------------

        void IMacroEditorHandler.OpenScreen(ScreenEditorViewModel subScreen)
        {
            openedScreens.Push(subScreen);
            OnPropertyChanged(nameof(CurrentScreen));
        }

        void IMacroEditorHandler.ReturnToParentScreen()
        {
            if (openedScreens.Count > 0)
            {
                openedScreens.Pop();
                OnPropertyChanged(nameof(CurrentScreen));
            }
        }

        IDialogService IMacroEditorHandler.DialogService => dialogService;

        IMessagingService IMacroEditorHandler.MessagingService => messagingService;

        IDriverRepositoryService IMacroEditorHandler.DriverRepository => driverRepository;

        IActionRepositoryService IMacroEditorHandler.ActionRepository => actionRepository;

        DriverMetadata IMacroEditorHandler.DriverMetadata => driverMetadata;

        // Public methods -----------------------------------------------------

        public EditMacrosWindowViewModel(IEditMacrosWindowAccess access, 
            IDriverRepositoryService controllerService,
            IActionRepositoryService actionRepository,
            IDialogService dialogService,
            IMessagingService messagingService,
            DriverMetadata driverMetadata, 
            Screen currentRootScreen)
        {
            this.access = access;
            this.driverRepository = controllerService;
            this.actionRepository = actionRepository;
            this.dialogService = dialogService;
            this.messagingService = messagingService;
            this.driverMetadata = driverMetadata;

            this.rootScreen = new ScreenEditorViewModel(this, currentRootScreen);

            availableMacroGroups = BuildAvailableMacros();

            OkCommand = new AppCommand(obj => DoOk());
            CancelCommand = new AppCommand(obj => DoCancel());
        }

        private List<MacroDirectoryGroupViewModel> BuildAvailableMacros()
        {
            List<MacroDirectoryEntryViewModel> macros = new();

            // Add system macros
            foreach (var type in (MacroType[])Enum.GetValues(typeof(MacroType)))
            {
                // Some types such as Back and Action should not be added on their own
                if (type.GetAttribute<DoNotListAttribute>() != null)
                    continue;

                macros.Add(new MacroDirectoryEntryViewModel(actionRepository, type));
            }

            // Add plugin macros
            foreach (var guid in actionRepository.ActionFactoryGuids)
                macros.Add(new MacroDirectoryEntryViewModel(actionRepository, guid));

            // Group
            return macros.GroupBy(m => m.Group)
                .Select(g => new MacroDirectoryGroupViewModel(g.Key, g.ToList()))
                .ToList();
        }

        public ScreenEditorViewModel CurrentScreen => openedScreens.Any() ? openedScreens.Peek() : rootScreen;

        public Screen? Result => BuildRootScreen();

        public IReadOnlyList<MacroDirectoryGroupViewModel> AvailableMacroGroups => availableMacroGroups;

        public ICommand OkCommand { get; }
        public ICommand CancelCommand { get; }
    }
}
