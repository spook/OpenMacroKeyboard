﻿using OpenMacroKeyboard.BusinessLogic.Services.ActionRepository;
using OpenMacroKeyboard.BusinessLogic.Services.Dialogs;
using OpenMacroKeyboard.BusinessLogic.Services.DriverRepository;
using OpenMacroKeyboard.BusinessLogic.Services.MacroKeyboardManager;
using OpenMacroKeyboard.BusinessLogic.ViewModels.Base;
using Spooksoft.VisualStateManager.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.App
{
    public class AppViewModel : BaseViewModel
    {
        private readonly IDialogService dialogService;
        private readonly IMacroKeyboardManagerService macroKeyboardManager;
        private readonly IAppAccess access;

        private void DoExit()
        {
            access.Close();
        }

        private void DoShowMainWindow()
        {
            dialogService.ShowMainWindow();
        }

        public AppViewModel(IDialogService dialogService, IMacroKeyboardManagerService macroKeyboardManager, IAppAccess access)
        {
            this.dialogService = dialogService;
            this.macroKeyboardManager = macroKeyboardManager;
            this.access = access;

            ShowMainWindowCommand = new AppCommand(obj => DoShowMainWindow());
            ExitCommand = new AppCommand(obj => DoExit());
        }

        public ICommand ShowMainWindowCommand { get; }
        public ICommand ExitCommand { get; }
    }
}
