﻿using OpenMacroKeyboard.BusinessLogic.Services.DriverRepository;
using OpenMacroKeyboard.BusinessLogic.ViewModels.Base;
using OpenMacroKeyboard.BusinessLogic.ViewModels.MacroKeyboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.BusinessLogic.ViewModels.MacroKeyboardPreview
{
    public class MacroKeyboardPreviewViewModel : BaseViewModel
    {
        private readonly IMacroKeyboardPreviewWindowAccess access;
        private IMacroKeyboard? macroKeyboard;
        private MacroKeyboardViewModel macroKeyboardViewModel;

        private void HandleActiveScreenChanged(object? sender, EventArgs e)
        {
            access.NotifyPageChanged();
        }

        private void HandleMacroKeyboardDisposing(object? sender, EventArgs e)
        {
            ClearMacroKeyboard();
        }

        private void ClearMacroKeyboard()
        {
            if (macroKeyboard != null)
            {
                macroKeyboard.Disposing -= HandleMacroKeyboardDisposing;
                macroKeyboard.ActiveScreenChanged -= HandleActiveScreenChanged;
            }

            macroKeyboard = null;
        }

        public MacroKeyboardPreviewViewModel(IMacroKeyboardPreviewWindowAccess access)
        {
            this.access = access;
            macroKeyboardViewModel = new MacroKeyboardViewModel(64.0f, 92.0f);
        }

        public void SetMacroKeyboard(IMacroKeyboard? value)
        {
            // Detach previous keyboard (if any)

            ClearMacroKeyboard();

            // Set new keyboard

            macroKeyboard = value;

            // Attach new keyboard (if any)

            if (macroKeyboard != null)
            {
                macroKeyboard.Disposing += HandleMacroKeyboardDisposing;
                macroKeyboard.ActiveScreenChanged += HandleActiveScreenChanged;
                macroKeyboardViewModel.MacroKeyboard = macroKeyboard;
            }            
        }

        public MacroKeyboardViewModel? MacroKeyboard => macroKeyboardViewModel;        
    }
}
