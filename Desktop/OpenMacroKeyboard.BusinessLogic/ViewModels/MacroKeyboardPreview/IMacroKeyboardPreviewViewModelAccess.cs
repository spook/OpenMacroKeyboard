﻿namespace OpenMacroKeyboard.BusinessLogic.ViewModels.MacroKeyboardPreview
{
    public interface IMacroKeyboardPreviewWindowAccess
    {
        void NotifyPageChanged();
    }
}