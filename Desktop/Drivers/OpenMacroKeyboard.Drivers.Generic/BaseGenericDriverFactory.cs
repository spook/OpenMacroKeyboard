﻿using OpenMacroKeyboard.API.V1;
using OpenMacroKeyboard.API.V1.Common.Models;
using OpenMacroKeyboard.API.V1.Drivers;
using OpenMacroKeyboard.API.V1.Drivers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Drivers.Generic
{
    public abstract class BaseGenericDriverFactory : BaseDriverFactory
    {
        private readonly int width;
        private readonly int height;
        private readonly bool supportsSleep;
        private readonly DriverMetadata metadata;

        protected DriverMetadata BuildGenericMetadata(int width, int height, Guid guid)
        {
            var metadata = new DriverMetadata($"Generic {width} x {height}", guid);

            for (int y = 0; y < height; y++)
                for (int x = 0; x < width; x++)
                {
                    metadata.ButtonDefinitions.Add(new ButtonInfo(false, true, true, 0.9f, 0.9f, 0.05f + x, 0.05f + y));
                }

            metadata.Settings.Add(new IntSettingInfo(Constants.COM_SETTING_KEY, "COM port number", 1, 1, 255));

            return metadata;
        }

        protected BaseGenericDriverFactory(ILog log, int width, int height, Guid guid, bool supportsSleep) 
            : base(log)
        {
            metadata = BuildGenericMetadata(width, height, guid);
            this.width = width;
            this.height = height;
            this.supportsSleep = supportsSleep;
        }

        public override BaseDriver CreateDriver(Dictionary<string, object> settings, IDriverHandler handler)
        {
            return new GenericDriver(width, height, settings, handler, log, supportsSleep);
        }

        public override DriverMetadata Metadata => metadata;
    }
}
