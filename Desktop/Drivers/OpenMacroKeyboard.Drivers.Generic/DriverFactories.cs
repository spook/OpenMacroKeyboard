﻿using OpenMacroKeyboard.API.V1;
using OpenMacroKeyboard.API.V1.Common.Models;
using OpenMacroKeyboard.API.V1.Drivers;
using OpenMacroKeyboard.API.V1.Drivers.Models;

namespace OpenMacroKeyboard.Drivers.Generic
{
    public class DriverFactory3x2 : BaseGenericDriverFactory
    {
        public DriverFactory3x2(ILog log)
            : base(log, 3, 2, Constants.GUID_3X2, false)
        {

        }
    }

    public class DriverFactory3x3 : BaseGenericDriverFactory
    {
        public DriverFactory3x3(ILog log)
            : base(log, 3, 3, Constants.GUID_3X3, false)
        {

        }
    }

    public class DriverFactory4x3 : BaseGenericDriverFactory
    {
        public DriverFactory4x3(ILog log)
            : base(log, 4, 3, Constants.GUID_4X3, false)
        {

        }
    }

    public class DriverFactory4x4 : BaseGenericDriverFactory
    {
        public DriverFactory4x4(ILog log)
            : base(log, 4, 4, Constants.GUID_4X4, false)
        {

        }
    }

    public class DriverFactory5x4 : BaseGenericDriverFactory
    {
        public DriverFactory5x4(ILog log)
            : base(log, 5, 4, Constants.GUID_5X4, false)
        {

        }
    }

    public class DriverFactory5x5 : BaseGenericDriverFactory
    {
        public DriverFactory5x5(ILog log)
            : base(log, 5, 5, Constants.GUID_5X5, false)
        {

        }
    }
}
