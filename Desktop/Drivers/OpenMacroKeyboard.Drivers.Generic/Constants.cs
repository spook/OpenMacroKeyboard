﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Drivers.Generic
{
    internal class Constants
    {

        public const string COM_SETTING_KEY = "com";

        public static readonly Guid GUID_3X2 = new("5ACAB3DA-DC2E-4DE4-BFC9-8D165E5194F1");
        public static readonly Guid GUID_3X3 = new("5FEC6093-EAC3-4B7A-ACA0-590C8644CF42");
        public static readonly Guid GUID_4X3 = new("8C48C5A7-7985-43B0-B9D0-DFAB4F8B0A23");
        public static readonly Guid GUID_4X4 = new("63F3E927-E3B1-4C07-BE6B-67E5C70B0E4C");
        public static readonly Guid GUID_5X4 = new("B0DB750A-681A-41E7-8B88-7A3DAAB7AC8A");
        public static readonly Guid GUID_5X5 = new("589B5B7E-2D1A-47B3-AF21-50D33AAD2CAC");
    }
}
