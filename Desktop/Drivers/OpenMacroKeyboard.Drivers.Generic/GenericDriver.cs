﻿using OpenMacroKeyboard.API.V1;
using OpenMacroKeyboard.API.V1.Drivers;
using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.API.V1.Drivers.Types;
using OpenMacroKeyboard.Drivers.Common.SerialPortDriver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Drivers.Generic
{
    public class GenericDriver : BaseSerialPortDriver
    {
        private readonly Dictionary<string, object> settings;
        private readonly int comPort;
        private readonly string display;

        public GenericDriver(int width, int height, Dictionary<string, object> settings, IDriverHandler handler, ILog log, bool supportsSleep)
            : base(handler, log, supportsSleep)
        {
            this.settings = settings;
            if (settings.ContainsKey(Constants.COM_SETTING_KEY) && settings[Constants.COM_SETTING_KEY] is int settingsComPort)
            {
                comPort = settingsComPort;
            }
            else
            {
                comPort = 1;
            }

            base.SetPort(comPort);
            display = $"Generic keyboard {width} x {height} on COM{comPort}";
        }

        public override string Display => display;
    }
}
