using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Drivers.MacroKeyboardV3
{
    internal class Constants
    {
        public static readonly Guid GUID = new("614E5BC2-E2A2-4E3C-BC54-F891A3DA9ED3");

        public const string COM_SETTING_KEY = "com";
    }
}
