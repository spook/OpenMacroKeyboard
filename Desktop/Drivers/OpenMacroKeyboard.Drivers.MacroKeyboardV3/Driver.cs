using OpenMacroKeyboard.API.V1;
using OpenMacroKeyboard.API.V1.Drivers;
using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.API.V1.Drivers.Types;
using OpenMacroKeyboard.Drivers.Common.Models;
using OpenMacroKeyboard.Drivers.Common.SerialPortDriver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Drivers.MacroKeyboardV3
{
    public class Driver : BaseSerialPortDriver
    {
        private readonly int comPort;

        public Driver(Dictionary<string, object> settings, IDriverHandler handler, ILog log)
            : base(handler, log, true)
        {
            if (settings.ContainsKey(Constants.COM_SETTING_KEY) && settings[Constants.COM_SETTING_KEY] is int settingsComPort)
            {
                comPort = settingsComPort;
            }
            else
            {
                comPort = 1;
            }

            base.SetPort(comPort);
        }

        public override void UpdateVisuals(List<KeyboardElementVisuals> buttons, List<KeyboardElementVisuals> encoders)
        {
            List<ColorChangeRequest> request = new();

            for (int i = 0; i < Math.Min(15, buttons.Count); i++)
                request.Add(new ColorChangeRequest(i, buttons[i].Color ?? KeyboardElementColor.Zero));

            SendChangeColors(request);
        }        

        public override string Display => String.Format(Resources.Strings.DriverDisplay, comPort);
    }
}
