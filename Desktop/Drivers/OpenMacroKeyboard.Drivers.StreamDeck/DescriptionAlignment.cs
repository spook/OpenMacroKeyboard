﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Drivers.StreamDeck
{
    internal enum DescriptionAlignment
    {
        Top = 0,
        Center = 1,
        Bottom = 2
    }
}
