﻿using OpenMacroKeyboard.API.V1;
using OpenMacroKeyboard.API.V1.Common.Models;
using OpenMacroKeyboard.API.V1.Drivers;
using OpenMacroKeyboard.API.V1.Drivers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Drivers.StreamDeck
{
    public class DriverFactory : BaseDriverFactory
    {               
        private readonly DriverMetadata metadata;

        public DriverFactory(ILog log)
            : base(log)
        {
            metadata = new DriverMetadata(Resources.Strings.StreamDeckV2_KeyboardName, Constants.GUID);

            for (int i = 0; i < 15; i++)
                metadata.ButtonDefinitions.Add(new ButtonInfo(false, true, true, (i / 5), (i % 5)));

            metadata.Settings.Add(new IntSettingInfo(Constants.BRIGHTNESS_KEY, Resources.Strings.Brightness, 50, 10, 100));
            metadata.Settings.Add(new StringSettingInfo(Constants.FONT_NAME_KEY, Resources.Strings.FontName, "Arial"));
            metadata.Settings.Add(new IntSettingInfo(Constants.FONT_SIZE_KEY, Resources.Strings.FontSize, 8, 1, 100));
            metadata.Settings.Add(new IntSettingInfo(Constants.LINE_HEIGHT_PERCENT_KEY, Resources.Strings.LineHeightPercent, 100, 1, 1000));
            metadata.Settings.Add(new IntSettingInfo(Constants.DESCRIPTION_WIDTH_PERCENT_KEY, Resources.Strings.DescriptionWidthPercent, 100, 1, 100));
            metadata.Settings.Add(new SelectSettingInfo(Constants.ALIGNMENT_KEY, Resources.Strings.DescriptionAlignment, 0, new List<SelectSettingEntryInfo>
            {
                new(Resources.Strings.DescriptionAlignment_Top, 0),
                new(Resources.Strings.DescriptionAlignment_Center, 1),
                new(Resources.Strings.DescriptionAlignment_Bottom, 2)
            }));
        }

        public override BaseDriver CreateDriver(Dictionary<string, object> settings, IDriverHandler handler)
        {
            return new Driver(settings, handler, log);
        }

        public override DriverMetadata Metadata => metadata;
    }
}
