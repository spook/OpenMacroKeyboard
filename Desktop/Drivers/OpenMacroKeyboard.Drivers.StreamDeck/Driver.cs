﻿using OpenMacroBoard.SDK;
using OpenMacroKeyboard.API.V1;
using OpenMacroKeyboard.API.V1.Drivers;
using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.API.V1.Drivers.Types;
using StreamDeckSharp;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Drivers.StreamDeck
{
    public class Driver : BaseDriver
    {
        private const int IMAGE_WIDTH = 72;
        private const int IMAGE_HEIGHT = 72;
        private const int MARGIN = 10;

        private IMacroBoard? deck;
        private bool sleep = false;
        private readonly byte brightness;
        private readonly string fontName;
        private readonly int fontSize;
        private readonly int lineHeightPercent;
        private readonly int descriptionWidthPercent;
        private readonly DescriptionAlignment descriptionAlignment;

        private object? BuildElementCustomCache(BaseKeyboardElementInfo definition, KeyboardElementVisuals visuals)
        {
            static List<string> BuildLinesWithBreaks(string[] nameParts, List<int> breakPositions)
            {
                int currentBreak = 0;

                List<string> line = new();
                List<string> lines = new();
                for (int i = 0; i < nameParts.Length; i++)
                {
                    line.Add(nameParts[i]);
                    if (currentBreak < breakPositions.Count && breakPositions[currentBreak] == i)
                    {
                        lines.Add(string.Join(' ', line));
                        line.Clear();

                        currentBreak++;
                    }
                }
                lines.Add(string.Join(' ', line));

                return lines;
            }

            static void RenderOutlined(Graphics g, string text, int x, int y, Font font)
            {
                for (int x1 = x - 1; x1 <= x + 1; x1++)
                    for (int y1 = y - 1; y1 <= y + 1; y1++)
                    {
                        g.DrawString(text, font, Brushes.Black, x1, y1);
                    }
                g.DrawString(text, font, Brushes.White, x, y);
            }

            if (!definition.CanSetImage)
                return null;

            Bitmap source;
            if (visuals.Image != null)
                source = BitmapTools.BitmapFromArgb(visuals.Image.Argb, visuals.Image.Width, visuals.Image.Height);
            else
            {
                source = new Bitmap(IMAGE_WIDTH, IMAGE_HEIGHT, PixelFormat.Format32bppArgb);
                using Graphics g = Graphics.FromImage(source);
                g.FillRectangle(Brushes.Black, new Rectangle(0, 0, source.Width, source.Height));
            }

            if (source.Width != IMAGE_WIDTH || source.Height != IMAGE_HEIGHT)
            {
                var resized = BitmapTools.Resize(source,  IMAGE_WIDTH, IMAGE_HEIGHT);
                source.Dispose();
                source = resized;
            }

            // Write description on image (if any)

            if (!string.IsNullOrEmpty(visuals.Description))
            {
                // Optimizing line breaks

                using Graphics g = Graphics.FromImage(source);

                string[] nameParts = visuals.Description.Split(new[] { ' ', '\r', '\n', '\t' }, StringSplitOptions.RemoveEmptyEntries);

                Font font = new(fontName, fontSize);
                int lineHeight = (int)g.MeasureString("Wy", font).Height * lineHeightPercent / 100;
                List<int> breaks = new();

                if (nameParts.Length > 1)
                {
                    int startItem = 0;

                    while (startItem < nameParts.Length - 1)
                    {
                        int itemCount = 1;
                        while (startItem + itemCount < nameParts.Length)
                        {
                            // Measure line with one additional segment added
                            string line = string.Join(" ", nameParts[startItem..(startItem + itemCount + 1)]);

                            var width = g.MeasureString(line, font).Width;
                            if (width > IMAGE_WIDTH * descriptionWidthPercent / 100)
                            {
                                breaks.Add(startItem + itemCount - 1);
                                break;
                            }
                            else
                                itemCount++;

                        }
                        
                        startItem = startItem + itemCount;
                        itemCount = 1;
                    }
                }
                
                // Now rendering the description
                
                List<string> finalLines = BuildLinesWithBreaks(nameParts, breaks);

                int y = descriptionAlignment switch
                {
                    DescriptionAlignment.Top => Math.Min(MARGIN, IMAGE_HEIGHT - finalLines.Count * lineHeight),
                    DescriptionAlignment.Center => (IMAGE_HEIGHT - finalLines.Count * lineHeight) / 2,
                    DescriptionAlignment.Bottom => IMAGE_HEIGHT - (Math.Max(0, Math.Min(MARGIN, IMAGE_HEIGHT - finalLines.Count * lineHeight))) - finalLines.Count * lineHeight,
                    _ => throw new InvalidOperationException("Unsupported description alignment!")
                };

                for (int i = 0; i < finalLines.Count; i++)
                {
                    int width = (int)g.MeasureString(finalLines[i], font).Width;

                    int x = (IMAGE_WIDTH - width) / 2;

                    RenderOutlined(g, finalLines[i], x, y, font);

                    y += lineHeight;
                }
            }

            var rawImage = BitmapTools.ArgbFromBitmap(source);
            return KeyBitmap.Create.FromBgra32Array(rawImage.width, rawImage.height, rawImage.argb);

            // return new RawImage(rawImage.argb, rawImage.width, rawImage.height);
        }


        private void HandleDeckKeyStateChanged(object? sender, KeyEventArgs e)
        {
            if (sleep)
            {
                if (!e.IsDown)
                    handler.NotifyAwake();                
            }
            else
            {
                handler.HandleEvent(MacroKeyboardEventSource.Button, e.Key, e.IsDown ? MacroKeyboardEventType.Down : MacroKeyboardEventType.Up);
            }
        }

        public Driver(Dictionary<string, object> settings, IDriverHandler handler, ILog log)
                    : base(handler, log)
        {
            deck = null;

            this.brightness = (byte)Math.Max(10, Math.Min(100, TryGetSetting(settings, Constants.BRIGHTNESS_KEY, 50)));
            this.fontName = TryGetSetting(settings, Constants.FONT_NAME_KEY, "Arial");
            this.fontSize = TryGetSetting(settings, Constants.FONT_SIZE_KEY, 8);
            this.lineHeightPercent = TryGetSetting(settings, Constants.LINE_HEIGHT_PERCENT_KEY, 100);
            this.descriptionWidthPercent = TryGetSetting(settings, Constants.DESCRIPTION_WIDTH_PERCENT_KEY, 100);
            this.descriptionAlignment = (DescriptionAlignment)Math.Max(0, Math.Min(2, TryGetSetting(settings, Constants.ALIGNMENT_KEY, 1)));
        }

        public override void Dispose()
        {
            deck?.Dispose();
            deck = null;
        }

        public override bool Init()
        {
            try
            {
                deck = StreamDeckSharp.StreamDeck.OpenDevice(Hardware.StreamDeckMK2);
                deck.SetBrightness(brightness);
                deck.KeyStateChanged += HandleDeckKeyStateChanged;
                return true;
            }
            catch
            {
                return false;
            }
        }
        public override void Sleep()
        {
            if (deck == null)
                return;

            deck.SetBrightness(0);
            sleep = true;
        }

        public override object? BuildButtonCustomCache(ButtonInfo definition, KeyboardElementVisuals visuals)
        {
            return BuildElementCustomCache(definition, visuals);
        }

        public override object? BuildEncoderCustomCache(EncoderInfo encoderDefinition, KeyboardElementVisuals visuals)
        {
            return BuildElementCustomCache(encoderDefinition, visuals);
        }


        public override void UpdateVisuals(List<KeyboardElementVisuals> buttons, List<KeyboardElementVisuals> encoders)
        {
            if (deck == null)
                return;

            if (sleep)
            {
                deck.SetBrightness(brightness);
                sleep = false;
            }

            // Prepare images to display            

            for (int i = 0; i < buttons.Count; i++)
            {
                KeyBitmap? bitmap = buttons[i].CustomCache as KeyBitmap;
                if (bitmap != null)
                    deck.SetKeyBitmap(i, bitmap);
                else
                    deck.SetKeyBitmap(i, KeyBitmap.Black);
            }
        }

        public override string Display => Resources.Strings.StreamDeckV2_DriverDisplay;
    }
}
