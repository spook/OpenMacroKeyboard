﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Drivers.StreamDeck
{
    internal class Constants
    {
        public static readonly Guid GUID = new("50A4FC3B-595B-44AB-9CBA-60C01A07D8B2");

        public static readonly string BRIGHTNESS_KEY = "brightness";
        public static readonly string FONT_NAME_KEY = "fontName";
        public static readonly string FONT_SIZE_KEY = "fontSize";
        public static readonly string LINE_HEIGHT_PERCENT_KEY = "lineHeightPercent";
        public static readonly string DESCRIPTION_WIDTH_PERCENT_KEY = "lineWidthPercent";
        public static readonly string ALIGNMENT_KEY = "alignment";
    }
}
