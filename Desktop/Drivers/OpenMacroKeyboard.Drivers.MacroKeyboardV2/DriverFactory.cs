﻿using OpenMacroKeyboard.API.V1;
using OpenMacroKeyboard.API.V1.Common.Models;
using OpenMacroKeyboard.API.V1.Drivers;
using OpenMacroKeyboard.API.V1.Drivers.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Drivers.MacroKeyboardV2
{
    public class DriverFactory : BaseDriverFactory
    {               
        private readonly DriverMetadata metadata;

        public DriverFactory(ILog log)
            : base(log)
        {
            metadata = new DriverMetadata(Resources.Strings.KeyboardName, Constants.GUID);

            for (int i = 0; i < 15; i++)
                metadata.ButtonDefinitions.Add(new ButtonInfo(i < 5, true, true, (i / 5), (i % 5)));

            for (int i = 0; i < 2; i++)
                metadata.EncoderDefinitions.Add(new EncoderInfo(true, true, false, true, (i * 2), 6));

            metadata.Settings.Add(new IntSettingInfo(Constants.COM_SETTING_KEY, Resources.Strings.Label_ComPortNumber, 1, 1, 255));
        }

        public override BaseDriver CreateDriver(Dictionary<string, object> settings, IDriverHandler handler)
        {
            return new Driver(settings, handler, log);
        }

        public override DriverMetadata Metadata => metadata;
    }
}
