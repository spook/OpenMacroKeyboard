﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Drivers.MacroKeyboardV2
{
    internal class Constants
    {
        public static readonly Guid GUID = new("0377D360-8414-46F1-933B-3098D15DAFD6");

        public const string COM_SETTING_KEY = "com";
    }
}
