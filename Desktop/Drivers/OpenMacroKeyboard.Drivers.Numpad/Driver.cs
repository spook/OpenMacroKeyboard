﻿using OpenMacroKeyboard.API.V1;
using OpenMacroKeyboard.API.V1.Drivers;
using OpenMacroKeyboard.API.V1.Drivers.Models;
using OpenMacroKeyboard.API.V1.Drivers.Types;
using OpenMacroKeyboard.Drivers.Common.SerialPortDriver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Drivers.Numpad
{
    public class Driver : BaseSerialPortDriver
    {
        private int comPort;

        public Driver(Dictionary<string, object> settings, IDriverHandler handler, ILog log)
            : base(handler, log, false)
        {
            if (settings.ContainsKey(Constants.COM_SETTING_KEY) && settings[Constants.COM_SETTING_KEY] is int settingsComPort)
            {
                comPort = settingsComPort;
            }
            else
            {
                comPort = 1;
            }

            SetPort(comPort);
        }

        public override string Display => $"Numpad on COM{comPort}";
    }
}
