﻿using OpenMacroKeyboard.API.V1;
using OpenMacroKeyboard.API.V1.Common.Models;
using OpenMacroKeyboard.API.V1.Drivers;
using OpenMacroKeyboard.API.V1.Drivers.Models;

namespace OpenMacroKeyboard.Drivers.Numpad
{
    public class DriverFactory : BaseDriverFactory
    {
        private readonly DriverMetadata metadata;

        public DriverFactory(ILog log)
            : base(log)
        {
            metadata = new DriverMetadata("Numpad", Constants.GUID);

            metadata.ButtonDefinitions.Add(new ButtonInfo(true, true, true, 0.9f, 0.9f, 0.05f + 0 * 1, 0.05f + 0 * 1));
            metadata.ButtonDefinitions.Add(new ButtonInfo(true, true, true, 0.9f, 0.9f, 0.05f + 1 * 1, 0.05f + 0 * 1));
            metadata.ButtonDefinitions.Add(new ButtonInfo(true, true, true, 0.9f, 0.9f, 0.05f + 2 * 1, 0.05f + 0 * 1));
            metadata.ButtonDefinitions.Add(new ButtonInfo(true, true, true, 0.9f, 0.9f, 0.05f + 3 * 1, 0.05f + 0 * 1));

            metadata.ButtonDefinitions.Add(new ButtonInfo(true, true, true, 0.9f, 0.9f, 0.05f + 0 * 1, 0.05f + 1 * 1));
            metadata.ButtonDefinitions.Add(new ButtonInfo(true, true, true, 0.9f, 0.9f, 0.05f + 1 * 1, 0.05f + 1 * 1));
            metadata.ButtonDefinitions.Add(new ButtonInfo(true, true, true, 0.9f, 0.9f, 0.05f + 2 * 1, 0.05f + 1 * 1));
            metadata.ButtonDefinitions.Add(new ButtonInfo(true, true, true, 0.9f, 1.9f, 0.05f + 3 * 1, 0.05f + 1 * 1));

            metadata.ButtonDefinitions.Add(new ButtonInfo(true, true, true, 0.9f, 0.9f, 0.05f + 0 * 1, 0.05f + 2 * 1));
            metadata.ButtonDefinitions.Add(new ButtonInfo(true, true, true, 0.9f, 0.9f, 0.05f + 1 * 1, 0.05f + 2 * 1));
            metadata.ButtonDefinitions.Add(new ButtonInfo(true, true, true, 0.9f, 0.9f, 0.05f + 2 * 1, 0.05f + 2 * 1));

            metadata.ButtonDefinitions.Add(new ButtonInfo(true, true, true, 0.9f, 0.9f, 0.05f + 0 * 1, 0.05f + 3 * 1));
            metadata.ButtonDefinitions.Add(new ButtonInfo(true, true, true, 0.9f, 0.9f, 0.05f + 1 * 1, 0.05f + 3 * 1));
            metadata.ButtonDefinitions.Add(new ButtonInfo(true, true, true, 0.9f, 0.9f, 0.05f + 2 * 1, 0.05f + 3 * 1));
            metadata.ButtonDefinitions.Add(new ButtonInfo(true, true, true, 0.9f, 1.9f, 0.05f + 3 * 1, 0.05f + 3 * 1));

            metadata.ButtonDefinitions.Add(new ButtonInfo(true, true, true, 1.9f, 0.9f, 0.05f + 0 * 1, 0.05f + 4 * 1));
            metadata.ButtonDefinitions.Add(new ButtonInfo(true, true, true, 0.9f, 0.9f, 0.05f + 2 * 1, 0.05f + 4 * 1));

            metadata.Settings.Add(new IntSettingInfo(Constants.COM_SETTING_KEY, "COM port number", 1, 1, 255));
        }

        public override BaseDriver CreateDriver(Dictionary<string, object> settings, IDriverHandler handler)
        {
            return new Driver(settings, handler, log);
        }

        public override DriverMetadata Metadata => metadata;
    }
}
