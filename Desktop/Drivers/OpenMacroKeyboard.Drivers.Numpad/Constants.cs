﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenMacroKeyboard.Drivers.Numpad
{
    internal class Constants
    {
        public static readonly Guid GUID = new("C90A1494-487F-4D16-98BC-F6F22E8D7969");

        public const string COM_SETTING_KEY = "com";
    }
}
